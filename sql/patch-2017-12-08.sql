ALTER TABLE `qishu_isv`.`office_resources` ADD COLUMN `contact` VARCHAR(100) NULL COMMENT '联系人' AFTER `other_tags`;

CREATE TABLE `office_resources_rent` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `count` int(50) DEFAULT '1',
  `staiton_price` int(20) DEFAULT NULL,
  `station_allnum` int(20) DEFAULT NULL COMMENT '工位总数',
  `standard_num` int(20) DEFAULT NULL COMMENT '标准入住人数',
  `surplus_num` int(20) DEFAULT NULL COMMENT '剩余工位数',
  `station_type` int(10) DEFAULT NULL,
  `station_attr` int(10) DEFAULT NULL,
  `billing_mode` int(10) DEFAULT NULL,
  `min_time` int(10) DEFAULT NULL,
  `unit_price` double(50,2) DEFAULT NULL,
  `area` double(50,2) DEFAULT NULL,
  `area_price` double(50,2) DEFAULT NULL,
  `usage_rate` double(50,2) DEFAULT NULL,
  `rent_type` int(10) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;