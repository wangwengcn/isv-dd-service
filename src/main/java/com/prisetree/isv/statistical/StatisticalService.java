package com.prisetree.isv.statistical;

import org.springframework.stereotype.Service;

import com.prisetree.isv.concurrent.InterfaceStatisticalTask;
import com.prisetree.isv.concurrent.IsvExecutor;
import com.prisetree.isv.domain.InterfaceStatistical;

@Service
public class StatisticalService {

	public void statistica(Interfaces i, Long rt) {
		try {
			InterfaceStatistical interfaceStatistical = new InterfaceStatistical();
			interfaceStatistical.setInterfaceId(i.toString());
			interfaceStatistical.setCost(rt);
			InterfaceStatisticalTask task = new InterfaceStatisticalTask(interfaceStatistical);
			IsvExecutor.run(task);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
