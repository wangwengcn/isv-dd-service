package com.prisetree.isv.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IsvExecutor {

	private static ExecutorService fixedThreadPool = Executors.newFixedThreadPool(10);

	public static void run(Runnable task)
	{
		fixedThreadPool.execute(task);
	}
}
