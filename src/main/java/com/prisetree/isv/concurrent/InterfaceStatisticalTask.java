package com.prisetree.isv.concurrent;

import java.util.Calendar;

import com.dingtalk.isv.access.biz.prisetree.dao.InterfaceStatisticalDao;
import com.prisetree.isv.common.util.SpringContextHolder;
import com.prisetree.isv.domain.InterfaceStatistical;

public class InterfaceStatisticalTask implements Runnable {

	InterfaceStatistical interfaceStatistical;
	
	public InterfaceStatisticalTask(InterfaceStatistical interfaceStatistical)
	{
		this.interfaceStatistical = interfaceStatistical;
	}
	
	@Override
	public void run() {
		InterfaceStatisticalDao dao = SpringContextHolder.getBean(InterfaceStatisticalDao.class);
		if(dao != null && interfaceStatistical != null)
		{
			Calendar now = Calendar.getInstance();
			// 设置秒和毫秒数为0，保证一分钟内取到的时间是一致的
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			interfaceStatistical.setDateMinute(now.getTime());
			dao.saveOrUpdateInterfaceStatistical(interfaceStatistical);
		}
	}

}
