package com.prisetree.isv.domain;

import java.util.Date;

import com.prisetree.isv.common.entity.BaseEntity;

public class SysDict extends BaseEntity<SysDict>{

	private String value;	//数据值
	private String label;	//标签名
	private String type;	//类型
	private String description;	//描述
	private Double sort;	//排序（升序）
	private Integer parentId;	//父级编号
	private Date createDate;	//创建时间
	private Date updateDate;	//更新时间
	private String remarks;	//备注信息

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getSort() {
		return sort;
	}

	public void setSort(Double sort) {
		this.sort = sort;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}