package com.prisetree.isv.domain;

import java.util.Date;

public class OfficeResourcesRent {
    /**
     * 工位id
     */
    private String id;

    /**
     * 工位名称
     */
    private String name;
    private String name1;
    private String name2;

    /**
     * 房源数
     */
    private Integer count;

    /**
     * 每工位每人每月/周/日单价
     */
    private Integer staitonPrice;

    /**
     * 工位总数
     */
    private Integer stationAllnum;

    /**
     * 标准入住人数
     */
    private Integer standardNum;

    /**
     * 剩余工位数
     */
    private Integer surplusNum;

    /**
     * 工位类型(0独立,1开放)
     */
    private Integer stationType;

    /**
     * 工位属性(0固定,1流动)
     */
    private Integer stationAttr;

    /**
     * 计费方式(0按天,1按周,2按月,3按年)
     */
    private Integer billingMode;

    /**
     * 最低出租时间
     */
    private Integer minTime;

    /**
     * 每平米单价
     */
    private Double unitPrice;

    /**
     * 面积
     */
    private Double area;

    /**
     * 按面积计算的月租
     */
    private Double areaPrice;

    /**
     * 得房率(使用率)
     */
    private Double usageRate;

    /**
     * 出租方式(0工位人数,1面积)
     */
    private Integer rentType;

    /**
     * 房源id
     */
    private String parentId;

    /**
     * 创建时间
     */
    private Date createDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getStaitonPrice() {
        return staitonPrice;
    }

    public void setStaitonPrice(Integer staitonPrice) {
        this.staitonPrice = staitonPrice;
    }

    public Integer getStationAllnum() {
        return stationAllnum;
    }

    public void setStationAllnum(Integer stationAllnum) {
        this.stationAllnum = stationAllnum;
    }

    public Integer getStandardNum() {
        return standardNum;
    }

    public void setStandardNum(Integer standardNum) {
        this.standardNum = standardNum;
    }

    public Integer getSurplusNum() {
        return surplusNum;
    }

    public void setSurplusNum(Integer surplusNum) {
        this.surplusNum = surplusNum;
    }

    public Integer getStationType() {
        return stationType;
    }

    public void setStationType(Integer stationType) {
        this.stationType = stationType;
    }

    public Integer getStationAttr() {
        return stationAttr;
    }

    public void setStationAttr(Integer stationAttr) {
        this.stationAttr = stationAttr;
    }

    public Integer getBillingMode() {
        return billingMode;
    }

    public void setBillingMode(Integer billingMode) {
        this.billingMode = billingMode;
    }

    public Integer getMinTime() {
        return minTime;
    }

    public void setMinTime(Integer minTime) {
        this.minTime = minTime;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getAreaPrice() {
        return areaPrice;
    }

    public void setAreaPrice(Double areaPrice) {
        this.areaPrice = areaPrice;
    }

    public Double getUsageRate() {
        return usageRate;
    }

    public void setUsageRate(Double usageRate) {
        this.usageRate = usageRate;
    }

    public Integer getRentType() {
        return rentType;
    }

    public void setRentType(Integer rentType) {
        this.rentType = rentType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId == null ? null : parentId.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}
}