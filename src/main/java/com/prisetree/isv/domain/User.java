package com.prisetree.isv.domain;

import com.prisetree.isv.common.entity.BaseEntity;

/**
 * appUserEntity
 * 
 * @author y
 * @version 2017-07-06
 */
public class User extends BaseEntity<User> {
	
	public static final String USER_TYPE_ADMIN = "0";

	private String mobile; // 手机号
	private String password; // 密码
	private String token; //登陆token 
	private String realName; // 真实姓名
	private String userType; // 用户身份
	private String sex; // 性别
	private String email;//邮件
	private String company;//公司名称

	public User() {
		super();
	}

	public User(String id) {
		super(id);
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}