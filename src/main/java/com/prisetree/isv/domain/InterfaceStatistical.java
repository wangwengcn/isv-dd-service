package com.prisetree.isv.domain;

import java.util.Date;

public class InterfaceStatistical {
	
	private Long id;
	private String interfaceId;
	private Date dateMinute;
	private Long count;
	private Long cost;
	
	public Long getRt() {
		return cost/count;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInterfaceId() {
		return interfaceId;
	}
	public void setInterfaceId(String interfaceId) {
		this.interfaceId = interfaceId;
	}
	public Date getDateMinute() {
		return dateMinute;
	}
	public void setDateMinute(Date dateMinute) {
		this.dateMinute = dateMinute;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	

}