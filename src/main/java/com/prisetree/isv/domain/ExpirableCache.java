package com.prisetree.isv.domain;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

public class ExpirableCache<T> {

	private List<T> list;
	/**
     * 过期时间
     */
    private Date expiredTime;
    
    public ExpirableCache(List<T> list, Date expiredTime)
    {
    	this.expiredTime = expiredTime;
    	this.list = list;
    }
    
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}

	public Date getExpiredTime() {
		return expiredTime;
	}

	public void setExpiredTime(Date expiredTime) {
		this.expiredTime = expiredTime;
	}

	public boolean isExpired()
	{
		return CollectionUtils.isEmpty(list) || expiredTime == null || expiredTime.getTime() < System.currentTimeMillis(); 
	}
}
