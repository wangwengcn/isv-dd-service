package com.prisetree.isv.domain;

import java.util.Date;

public class OfficeResources {
    /**
     * ID
     */
    private String id;

    /**
     * 房源名
     */
    private String name;

    /**
     * 备注
     */
    private String profile;

    /**
     *
     * 经度
     */
    private String lon;

    /**
     *
     * 纬度
     */
    private String lat;
    
    /**
     * 服务及设施-基础设施
     */
    private String baseFacility;
    
    /**
     * 服务及设施-基础服务
     */
    private String baseService;
    
    /**
     * 服务及设施-特色服务
     */
    private String speService;

    /**
     *
     * 地理周边-交通
     */
    private String traffic;

    /**
     * 地理周边-周边
     */
    private String nearby;

    /**
     * 省
     */
    private String province;

    /**
     *
     * 城市
     */
    private String city;
    
    /**
    *
    * 区县
    */
    private String area;
    
    /**
    *
    * 详细地址
    */
    private String detailAddr;
    
    /**
    *
    * 联系人
    */
    private String contact;
    
    /**
    *
    * 联系人电话
    */
    private String contactMobile;
    
    /**
    *
    * 空间特点
    */
    private String feature;
    
    /**
    *
    * 工位类型（联合办公 0，园区 1，写字楼 2）
    */
   private Integer officeType;

    /**
     *
     * 是否有固定工位（否 0，是 1）
     */
    private Integer isStable;
    
    /**
    *
    * 固定开放工位价格
    */
   private Integer stableOpenPrice;
   
   /**
   *
   * 固定独立工位价格
   */
   private Integer stableSpecializedPrice;
    
    /**
    *
    * 是否有流动工位（否 0，是 1）
    */
   private Integer isFlexible;
   
   /**
   *
   * 流动开放工位价格
   */
  private Integer flexibleOpenPrice;
  
  /**
  *
  * 流动独立工位价格
  */
  private Integer flexibleSpecializedPrice;

    /**
     *
     * 剩余工位数
     */
    private Integer stationNum;
    
    /**
    *
    * 星级
    */
   private Integer starNum;

    /**
     * 至少预定几个月（固定工位的属性）
     */
    private Integer minTime;

    /**
     * 创建时间
     */
    private Date createDate;
    
    /**
     * 更新时间
     */
    private Date updateDate;

    /**
     * 更新人
     */
    private String updateBy;
    
    /**
     * 标签
     */
    private String otherTags;
    
    // 距离
    private String distance;
    // 标签交互对象
    private String[] tagsvo;

	/**
	 * 关联属性
	 */
	private String imgUrl;
	private String minRentPrice;
	private String minRentTime;
	private String minRentTimeType;
	private String rentType;
	private String unitPrice;
    private Double officeArea;
    private Double usageRate;

	public Double getOfficeArea() {
		return officeArea;
	}

	public void setOfficeArea(Double officeArea) {
		this.officeArea = officeArea;
	}

	public Double getUsageRate() {
		return usageRate;
	}

	public void setUsageRate(Double usageRate) {
		this.usageRate = usageRate;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getRentType() {
		return rentType;
	}

	public void setRentType(String rentType) {
		this.rentType = rentType;
	}

	public String getMinRentPrice() {
		return minRentPrice;
	}

	public void setMinRentPrice(String minRentPrice) {
		this.minRentPrice = minRentPrice;
	}

	public void setMinRentTime(String minRentTime) {
		this.minRentTime = minRentTime;
	}

	public void setMinRentTimeType(String minRentTimeType) {
		this.minRentTimeType = minRentTimeType;
	}

	public String getMinRentTime() {
		return minRentTime;
	}

	public String getMinRentTimeType() {
		return minRentTimeType;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String[] getTagsvo() {
		return tagsvo;
	}

	public void setTagsvo(String[] tagsvo) {
		this.tagsvo = tagsvo;
	}

	public String getOtherTags() {
		return otherTags;
	}

	public void setOtherTags(String otherTags) {
		this.otherTags = otherTags;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDetailAddr() {
		return detailAddr;
	}

	public void setDetailAddr(String detailAddr) {
		this.detailAddr = detailAddr;
	}

	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getBaseFacility() {
		return baseFacility;
	}

	public void setBaseFacility(String baseFacility) {
		this.baseFacility = baseFacility;
	}

	public String getBaseService() {
		return baseService;
	}

	public void setBaseService(String baseService) {
		this.baseService = baseService;
	}

	public String getSpeService() {
		return speService;
	}

	public void setSpeService(String speService) {
		this.speService = speService;
	}

	public String getTraffic() {
		return traffic;
	}

	public void setTraffic(String traffic) {
		this.traffic = traffic;
	}

	public String getNearby() {
		return nearby;
	}

	public void setNearby(String nearby) {
		this.nearby = nearby;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getOfficeType() {
		return officeType;
	}

	public void setOfficeType(Integer officeType) {
		this.officeType = officeType;
	}

	public Integer getIsStable() {
		return isStable;
	}

	public void setIsStable(Integer isStable) {
		this.isStable = isStable;
	}

	public Integer getIsFlexible() {
		return isFlexible;
	}

	public void setIsFlexible(Integer isFlexible) {
		this.isFlexible = isFlexible;
	}

	public Integer getStableOpenPrice() {
		return stableOpenPrice;
	}

	public void setStableOpenPrice(Integer stableOpenPrice) {
		this.stableOpenPrice = stableOpenPrice;
	}

	public Integer getStableSpecializedPrice() {
		return stableSpecializedPrice;
	}

	public void setStableSpecializedPrice(Integer stableSpecializedPrice) {
		this.stableSpecializedPrice = stableSpecializedPrice;
	}

	public Integer getFlexibleOpenPrice() {
		return flexibleOpenPrice;
	}

	public void setFlexibleOpenPrice(Integer flexibleOpenPrice) {
		this.flexibleOpenPrice = flexibleOpenPrice;
	}

	public Integer getFlexibleSpecializedPrice() {
		return flexibleSpecializedPrice;
	}

	public void setFlexibleSpecializedPrice(Integer flexibleSpecializedPrice) {
		this.flexibleSpecializedPrice = flexibleSpecializedPrice;
	}

	public Integer getStationNum() {
		return stationNum;
	}

	public void setStationNum(Integer stationNum) {
		this.stationNum = stationNum;
	}

	public Integer getStarNum() {
		return starNum;
	}

	public void setStarNum(Integer starNum) {
		this.starNum = starNum;
	}

	public Integer getMinTime() {
		return minTime;
	}

	public void setMinTime(Integer minTime) {
		this.minTime = minTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}
    
}