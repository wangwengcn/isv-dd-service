package com.prisetree.isv.domain;

import java.util.Date;

public class OfficeResourcesImg {
	
	public static final String UPLOAD_TYPE_URL = "1";
	public static final String UPLOAD_TYPE_FILE = "2";

	/**
	 * ID
	 */
	private String id;

	/**
	 * 房源ID
	 */
	private String officeResourcesId;

	/**
	 * 图片地址
	 */
	private String imgUrl;

	/**
	 *
	 * 显示顺序
	 */
	private Integer orderNum;

	/**
	 *
	 * 图片类型（1.轮播，2.列表头像,只有一张，3.地理图，只有一张）
	 */
	private Integer isHeader;

	/**
	 *
	 * 上传类型 1:url 2:图片文件
	 */
	private String uploadType;

	private Date createDate;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfficeResourcesId() {
		return officeResourcesId;
	}

	public void setOfficeResourcesId(String officeResourcesId) {
		this.officeResourcesId = officeResourcesId;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getIsHeader() {
		return isHeader;
	}

	public void setIsHeader(Integer isHeader) {
		this.isHeader = isHeader;
	}

	public String getUploadType() {
		return uploadType;
	}

	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}

}
