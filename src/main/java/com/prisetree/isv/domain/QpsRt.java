package com.prisetree.isv.domain;

import java.util.concurrent.atomic.AtomicLong;

public class QpsRt {

	private AtomicLong qps;
	private AtomicLong rt;

	public QpsRt(){
		qps = new AtomicLong(0);
		rt = new AtomicLong(0);
	}
	
	public AtomicLong getQps() {
		return qps;
	}

	public void setQps(AtomicLong qps) {
		this.qps = qps;
	}

	public AtomicLong getRt() {
		return rt;
	}

	public void setRt(AtomicLong rt) {
		this.rt = rt;
	}

}
