package com.prisetree.isv.domain;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.prisetree.isv.common.entity.BaseEntity;

public class UserStatistical extends BaseEntity<UserStatistical> {
	
	private String userId;
	private Date date;
	private String time;
	private Integer count;
	
	public UserStatistical() {
		super();
	}

	public UserStatistical(String id) {
		super(id);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}