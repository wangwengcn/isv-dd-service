package com.prisetree.isv.domain.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrdersExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrdersExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNull() {
            addCriterion("order_number is null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNotNull() {
            addCriterion("order_number is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberEqualTo(String value) {
            addCriterion("order_number =", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotEqualTo(String value) {
            addCriterion("order_number <>", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThan(String value) {
            addCriterion("order_number >", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThanOrEqualTo(String value) {
            addCriterion("order_number >=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThan(String value) {
            addCriterion("order_number <", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThanOrEqualTo(String value) {
            addCriterion("order_number <=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLike(String value) {
            addCriterion("order_number like", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotLike(String value) {
            addCriterion("order_number not like", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIn(List<String> values) {
            addCriterion("order_number in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotIn(List<String> values) {
            addCriterion("order_number not in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberBetween(String value1, String value2) {
            addCriterion("order_number between", value1, value2, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotBetween(String value1, String value2) {
            addCriterion("order_number not between", value1, value2, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdIsNull() {
            addCriterion("office_resources_id is null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdIsNotNull() {
            addCriterion("office_resources_id is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdEqualTo(String value) {
            addCriterion("office_resources_id =", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdNotEqualTo(String value) {
            addCriterion("office_resources_id <>", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdGreaterThan(String value) {
            addCriterion("office_resources_id >", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdGreaterThanOrEqualTo(String value) {
            addCriterion("office_resources_id >=", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdLessThan(String value) {
            addCriterion("office_resources_id <", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdLessThanOrEqualTo(String value) {
            addCriterion("office_resources_id <=", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdLike(String value) {
            addCriterion("office_resources_id like", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdNotLike(String value) {
            addCriterion("office_resources_id not like", value, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdIn(List<String> values) {
            addCriterion("office_resources_id in", values, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdNotIn(List<String> values) {
            addCriterion("office_resources_id not in", values, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdBetween(String value1, String value2) {
            addCriterion("office_resources_id between", value1, value2, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesIdNotBetween(String value1, String value2) {
            addCriterion("office_resources_id not between", value1, value2, "officeResourcesId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameIsNull() {
            addCriterion("office_resources_name is null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameIsNotNull() {
            addCriterion("office_resources_name is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameEqualTo(String value) {
            addCriterion("office_resources_name =", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameNotEqualTo(String value) {
            addCriterion("office_resources_name <>", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameGreaterThan(String value) {
            addCriterion("office_resources_name >", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameGreaterThanOrEqualTo(String value) {
            addCriterion("office_resources_name >=", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameLessThan(String value) {
            addCriterion("office_resources_name <", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameLessThanOrEqualTo(String value) {
            addCriterion("office_resources_name <=", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameLike(String value) {
            addCriterion("office_resources_name like", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameNotLike(String value) {
            addCriterion("office_resources_name not like", value, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameIn(List<String> values) {
            addCriterion("office_resources_name in", values, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameNotIn(List<String> values) {
            addCriterion("office_resources_name not in", values, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameBetween(String value1, String value2) {
            addCriterion("office_resources_name between", value1, value2, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesNameNotBetween(String value1, String value2) {
            addCriterion("office_resources_name not between", value1, value2, "officeResourcesName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdIsNull() {
            addCriterion("office_resources_rent_id is null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdIsNotNull() {
            addCriterion("office_resources_rent_id is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdEqualTo(String value) {
            addCriterion("office_resources_rent_id =", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdNotEqualTo(String value) {
            addCriterion("office_resources_rent_id <>", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdGreaterThan(String value) {
            addCriterion("office_resources_rent_id >", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdGreaterThanOrEqualTo(String value) {
            addCriterion("office_resources_rent_id >=", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdLessThan(String value) {
            addCriterion("office_resources_rent_id <", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdLessThanOrEqualTo(String value) {
            addCriterion("office_resources_rent_id <=", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdLike(String value) {
            addCriterion("office_resources_rent_id like", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdNotLike(String value) {
            addCriterion("office_resources_rent_id not like", value, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdIn(List<String> values) {
            addCriterion("office_resources_rent_id in", values, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdNotIn(List<String> values) {
            addCriterion("office_resources_rent_id not in", values, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdBetween(String value1, String value2) {
            addCriterion("office_resources_rent_id between", value1, value2, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentIdNotBetween(String value1, String value2) {
            addCriterion("office_resources_rent_id not between", value1, value2, "officeResourcesRentId");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameIsNull() {
            addCriterion("office_resources_rent_name is null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameIsNotNull() {
            addCriterion("office_resources_rent_name is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameEqualTo(String value) {
            addCriterion("office_resources_rent_name =", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameNotEqualTo(String value) {
            addCriterion("office_resources_rent_name <>", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameGreaterThan(String value) {
            addCriterion("office_resources_rent_name >", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameGreaterThanOrEqualTo(String value) {
            addCriterion("office_resources_rent_name >=", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameLessThan(String value) {
            addCriterion("office_resources_rent_name <", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameLessThanOrEqualTo(String value) {
            addCriterion("office_resources_rent_name <=", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameLike(String value) {
            addCriterion("office_resources_rent_name like", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameNotLike(String value) {
            addCriterion("office_resources_rent_name not like", value, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameIn(List<String> values) {
            addCriterion("office_resources_rent_name in", values, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameNotIn(List<String> values) {
            addCriterion("office_resources_rent_name not in", values, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameBetween(String value1, String value2) {
            addCriterion("office_resources_rent_name between", value1, value2, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesRentNameNotBetween(String value1, String value2) {
            addCriterion("office_resources_rent_name not between", value1, value2, "officeResourcesRentName");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrIsNull() {
            addCriterion("office_resources_addr is null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrIsNotNull() {
            addCriterion("office_resources_addr is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrEqualTo(String value) {
            addCriterion("office_resources_addr =", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrNotEqualTo(String value) {
            addCriterion("office_resources_addr <>", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrGreaterThan(String value) {
            addCriterion("office_resources_addr >", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrGreaterThanOrEqualTo(String value) {
            addCriterion("office_resources_addr >=", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrLessThan(String value) {
            addCriterion("office_resources_addr <", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrLessThanOrEqualTo(String value) {
            addCriterion("office_resources_addr <=", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrLike(String value) {
            addCriterion("office_resources_addr like", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrNotLike(String value) {
            addCriterion("office_resources_addr not like", value, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrIn(List<String> values) {
            addCriterion("office_resources_addr in", values, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrNotIn(List<String> values) {
            addCriterion("office_resources_addr not in", values, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrBetween(String value1, String value2) {
            addCriterion("office_resources_addr between", value1, value2, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesAddrNotBetween(String value1, String value2) {
            addCriterion("office_resources_addr not between", value1, value2, "officeResourcesAddr");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileIsNull() {
            addCriterion("office_resources_mobile is null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileIsNotNull() {
            addCriterion("office_resources_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileEqualTo(String value) {
            addCriterion("office_resources_mobile =", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileNotEqualTo(String value) {
            addCriterion("office_resources_mobile <>", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileGreaterThan(String value) {
            addCriterion("office_resources_mobile >", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileGreaterThanOrEqualTo(String value) {
            addCriterion("office_resources_mobile >=", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileLessThan(String value) {
            addCriterion("office_resources_mobile <", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileLessThanOrEqualTo(String value) {
            addCriterion("office_resources_mobile <=", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileLike(String value) {
            addCriterion("office_resources_mobile like", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileNotLike(String value) {
            addCriterion("office_resources_mobile not like", value, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileIn(List<String> values) {
            addCriterion("office_resources_mobile in", values, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileNotIn(List<String> values) {
            addCriterion("office_resources_mobile not in", values, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileBetween(String value1, String value2) {
            addCriterion("office_resources_mobile between", value1, value2, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andOfficeResourcesMobileNotBetween(String value1, String value2) {
            addCriterion("office_resources_mobile not between", value1, value2, "officeResourcesMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNull() {
            addCriterion("company_id is null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIsNotNull() {
            addCriterion("company_id is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyIdEqualTo(String value) {
            addCriterion("company_id =", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotEqualTo(String value) {
            addCriterion("company_id <>", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThan(String value) {
            addCriterion("company_id >", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdGreaterThanOrEqualTo(String value) {
            addCriterion("company_id >=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThan(String value) {
            addCriterion("company_id <", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLessThanOrEqualTo(String value) {
            addCriterion("company_id <=", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdLike(String value) {
            addCriterion("company_id like", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotLike(String value) {
            addCriterion("company_id not like", value, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdIn(List<String> values) {
            addCriterion("company_id in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotIn(List<String> values) {
            addCriterion("company_id not in", values, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdBetween(String value1, String value2) {
            addCriterion("company_id between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyIdNotBetween(String value1, String value2) {
            addCriterion("company_id not between", value1, value2, "companyId");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIsNull() {
            addCriterion("company_name is null");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIsNotNull() {
            addCriterion("company_name is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyNameEqualTo(String value) {
            addCriterion("company_name =", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotEqualTo(String value) {
            addCriterion("company_name <>", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameGreaterThan(String value) {
            addCriterion("company_name >", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameGreaterThanOrEqualTo(String value) {
            addCriterion("company_name >=", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLessThan(String value) {
            addCriterion("company_name <", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLessThanOrEqualTo(String value) {
            addCriterion("company_name <=", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLike(String value) {
            addCriterion("company_name like", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotLike(String value) {
            addCriterion("company_name not like", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIn(List<String> values) {
            addCriterion("company_name in", values, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotIn(List<String> values) {
            addCriterion("company_name not in", values, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameBetween(String value1, String value2) {
            addCriterion("company_name between", value1, value2, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotBetween(String value1, String value2) {
            addCriterion("company_name not between", value1, value2, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileIsNull() {
            addCriterion("company_mobile is null");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileIsNotNull() {
            addCriterion("company_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileEqualTo(String value) {
            addCriterion("company_mobile =", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileNotEqualTo(String value) {
            addCriterion("company_mobile <>", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileGreaterThan(String value) {
            addCriterion("company_mobile >", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileGreaterThanOrEqualTo(String value) {
            addCriterion("company_mobile >=", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileLessThan(String value) {
            addCriterion("company_mobile <", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileLessThanOrEqualTo(String value) {
            addCriterion("company_mobile <=", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileLike(String value) {
            addCriterion("company_mobile like", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileNotLike(String value) {
            addCriterion("company_mobile not like", value, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileIn(List<String> values) {
            addCriterion("company_mobile in", values, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileNotIn(List<String> values) {
            addCriterion("company_mobile not in", values, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileBetween(String value1, String value2) {
            addCriterion("company_mobile between", value1, value2, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andCompanyMobileNotBetween(String value1, String value2) {
            addCriterion("company_mobile not between", value1, value2, "companyMobile");
            return (Criteria) this;
        }

        public Criteria andUnitIsNull() {
            addCriterion("unit is null");
            return (Criteria) this;
        }

        public Criteria andUnitIsNotNull() {
            addCriterion("unit is not null");
            return (Criteria) this;
        }

        public Criteria andUnitEqualTo(Integer value) {
            addCriterion("unit =", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotEqualTo(Integer value) {
            addCriterion("unit <>", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThan(Integer value) {
            addCriterion("unit >", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitGreaterThanOrEqualTo(Integer value) {
            addCriterion("unit >=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThan(Integer value) {
            addCriterion("unit <", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitLessThanOrEqualTo(Integer value) {
            addCriterion("unit <=", value, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitIn(List<Integer> values) {
            addCriterion("unit in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotIn(List<Integer> values) {
            addCriterion("unit not in", values, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitBetween(Integer value1, Integer value2) {
            addCriterion("unit between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andUnitNotBetween(Integer value1, Integer value2) {
            addCriterion("unit not between", value1, value2, "unit");
            return (Criteria) this;
        }

        public Criteria andBillingModeIsNull() {
            addCriterion("billing_mode is null");
            return (Criteria) this;
        }

        public Criteria andBillingModeIsNotNull() {
            addCriterion("billing_mode is not null");
            return (Criteria) this;
        }

        public Criteria andBillingModeEqualTo(String value) {
            addCriterion("billing_mode =", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeNotEqualTo(String value) {
            addCriterion("billing_mode <>", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeGreaterThan(String value) {
            addCriterion("billing_mode >", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeGreaterThanOrEqualTo(String value) {
            addCriterion("billing_mode >=", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeLessThan(String value) {
            addCriterion("billing_mode <", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeLessThanOrEqualTo(String value) {
            addCriterion("billing_mode <=", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeLike(String value) {
            addCriterion("billing_mode like", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeNotLike(String value) {
            addCriterion("billing_mode not like", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeIn(List<String> values) {
            addCriterion("billing_mode in", values, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeNotIn(List<String> values) {
            addCriterion("billing_mode not in", values, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeBetween(String value1, String value2) {
            addCriterion("billing_mode between", value1, value2, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeNotBetween(String value1, String value2) {
            addCriterion("billing_mode not between", value1, value2, "billingMode");
            return (Criteria) this;
        }

        public Criteria andEnterTimeIsNull() {
            addCriterion("enter_time is null");
            return (Criteria) this;
        }

        public Criteria andEnterTimeIsNotNull() {
            addCriterion("enter_time is not null");
            return (Criteria) this;
        }

        public Criteria andEnterTimeEqualTo(Date value) {
            addCriterion("enter_time =", value, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeNotEqualTo(Date value) {
            addCriterion("enter_time <>", value, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeGreaterThan(Date value) {
            addCriterion("enter_time >", value, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("enter_time >=", value, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeLessThan(Date value) {
            addCriterion("enter_time <", value, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeLessThanOrEqualTo(Date value) {
            addCriterion("enter_time <=", value, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeIn(List<Date> values) {
            addCriterion("enter_time in", values, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeNotIn(List<Date> values) {
            addCriterion("enter_time not in", values, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeBetween(Date value1, Date value2) {
            addCriterion("enter_time between", value1, value2, "enterTime");
            return (Criteria) this;
        }

        public Criteria andEnterTimeNotBetween(Date value1, Date value2) {
            addCriterion("enter_time not between", value1, value2, "enterTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeIsNull() {
            addCriterion("leave_time is null");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeIsNotNull() {
            addCriterion("leave_time is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeEqualTo(Date value) {
            addCriterion("leave_time =", value, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeNotEqualTo(Date value) {
            addCriterion("leave_time <>", value, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeGreaterThan(Date value) {
            addCriterion("leave_time >", value, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("leave_time >=", value, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeLessThan(Date value) {
            addCriterion("leave_time <", value, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeLessThanOrEqualTo(Date value) {
            addCriterion("leave_time <=", value, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeIn(List<Date> values) {
            addCriterion("leave_time in", values, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeNotIn(List<Date> values) {
            addCriterion("leave_time not in", values, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeBetween(Date value1, Date value2) {
            addCriterion("leave_time between", value1, value2, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andLeaveTimeNotBetween(Date value1, Date value2) {
            addCriterion("leave_time not between", value1, value2, "leaveTime");
            return (Criteria) this;
        }

        public Criteria andStationNumIsNull() {
            addCriterion("station_num is null");
            return (Criteria) this;
        }

        public Criteria andStationNumIsNotNull() {
            addCriterion("station_num is not null");
            return (Criteria) this;
        }

        public Criteria andStationNumEqualTo(Integer value) {
            addCriterion("station_num =", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumNotEqualTo(Integer value) {
            addCriterion("station_num <>", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumGreaterThan(Integer value) {
            addCriterion("station_num >", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("station_num >=", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumLessThan(Integer value) {
            addCriterion("station_num <", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumLessThanOrEqualTo(Integer value) {
            addCriterion("station_num <=", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumIn(List<Integer> values) {
            addCriterion("station_num in", values, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumNotIn(List<Integer> values) {
            addCriterion("station_num not in", values, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumBetween(Integer value1, Integer value2) {
            addCriterion("station_num between", value1, value2, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumNotBetween(Integer value1, Integer value2) {
            addCriterion("station_num not between", value1, value2, "stationNum");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(Double value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(Double value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(Double value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(Double value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(Double value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(Double value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<Double> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<Double> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(Double value1, Double value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(Double value1, Double value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andUsageRateIsNull() {
            addCriterion("usage_rate is null");
            return (Criteria) this;
        }

        public Criteria andUsageRateIsNotNull() {
            addCriterion("usage_rate is not null");
            return (Criteria) this;
        }

        public Criteria andUsageRateEqualTo(Double value) {
            addCriterion("usage_rate =", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateNotEqualTo(Double value) {
            addCriterion("usage_rate <>", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateGreaterThan(Double value) {
            addCriterion("usage_rate >", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateGreaterThanOrEqualTo(Double value) {
            addCriterion("usage_rate >=", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateLessThan(Double value) {
            addCriterion("usage_rate <", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateLessThanOrEqualTo(Double value) {
            addCriterion("usage_rate <=", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateIn(List<Double> values) {
            addCriterion("usage_rate in", values, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateNotIn(List<Double> values) {
            addCriterion("usage_rate not in", values, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateBetween(Double value1, Double value2) {
            addCriterion("usage_rate between", value1, value2, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateNotBetween(Double value1, Double value2) {
            addCriterion("usage_rate not between", value1, value2, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUnitPriceIsNull() {
            addCriterion("unit_price is null");
            return (Criteria) this;
        }

        public Criteria andUnitPriceIsNotNull() {
            addCriterion("unit_price is not null");
            return (Criteria) this;
        }

        public Criteria andUnitPriceEqualTo(Double value) {
            addCriterion("unit_price =", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceNotEqualTo(Double value) {
            addCriterion("unit_price <>", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceGreaterThan(Double value) {
            addCriterion("unit_price >", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("unit_price >=", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceLessThan(Double value) {
            addCriterion("unit_price <", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceLessThanOrEqualTo(Double value) {
            addCriterion("unit_price <=", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceIn(List<Double> values) {
            addCriterion("unit_price in", values, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceNotIn(List<Double> values) {
            addCriterion("unit_price not in", values, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceBetween(Double value1, Double value2) {
            addCriterion("unit_price between", value1, value2, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceNotBetween(Double value1, Double value2) {
            addCriterion("unit_price not between", value1, value2, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceIsNull() {
            addCriterion("total_price is null");
            return (Criteria) this;
        }

        public Criteria andTotalPriceIsNotNull() {
            addCriterion("total_price is not null");
            return (Criteria) this;
        }

        public Criteria andTotalPriceEqualTo(Double value) {
            addCriterion("total_price =", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceNotEqualTo(Double value) {
            addCriterion("total_price <>", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceGreaterThan(Double value) {
            addCriterion("total_price >", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("total_price >=", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceLessThan(Double value) {
            addCriterion("total_price <", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceLessThanOrEqualTo(Double value) {
            addCriterion("total_price <=", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceIn(List<Double> values) {
            addCriterion("total_price in", values, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceNotIn(List<Double> values) {
            addCriterion("total_price not in", values, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceBetween(Double value1, Double value2) {
            addCriterion("total_price between", value1, value2, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceNotBetween(Double value1, Double value2) {
            addCriterion("total_price not between", value1, value2, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("state like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("state not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andStationTypeIsNull() {
            addCriterion("station_type is null");
            return (Criteria) this;
        }

        public Criteria andStationTypeIsNotNull() {
            addCriterion("station_type is not null");
            return (Criteria) this;
        }

        public Criteria andStationTypeEqualTo(String value) {
            addCriterion("station_type =", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeNotEqualTo(String value) {
            addCriterion("station_type <>", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeGreaterThan(String value) {
            addCriterion("station_type >", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeGreaterThanOrEqualTo(String value) {
            addCriterion("station_type >=", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeLessThan(String value) {
            addCriterion("station_type <", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeLessThanOrEqualTo(String value) {
            addCriterion("station_type <=", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeLike(String value) {
            addCriterion("station_type like", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeNotLike(String value) {
            addCriterion("station_type not like", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeIn(List<String> values) {
            addCriterion("station_type in", values, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeNotIn(List<String> values) {
            addCriterion("station_type not in", values, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeBetween(String value1, String value2) {
            addCriterion("station_type between", value1, value2, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeNotBetween(String value1, String value2) {
            addCriterion("station_type not between", value1, value2, "stationType");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}