package com.prisetree.isv.domain.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OfficeResourcesExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OfficeResourcesExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andProfileIsNull() {
            addCriterion("profile is null");
            return (Criteria) this;
        }

        public Criteria andProfileIsNotNull() {
            addCriterion("profile is not null");
            return (Criteria) this;
        }

        public Criteria andProfileEqualTo(String value) {
            addCriterion("profile =", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileNotEqualTo(String value) {
            addCriterion("profile <>", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileGreaterThan(String value) {
            addCriterion("profile >", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileGreaterThanOrEqualTo(String value) {
            addCriterion("profile >=", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileLessThan(String value) {
            addCriterion("profile <", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileLessThanOrEqualTo(String value) {
            addCriterion("profile <=", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileLike(String value) {
            addCriterion("profile like", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileNotLike(String value) {
            addCriterion("profile not like", value, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileIn(List<String> values) {
            addCriterion("profile in", values, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileNotIn(List<String> values) {
            addCriterion("profile not in", values, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileBetween(String value1, String value2) {
            addCriterion("profile between", value1, value2, "profile");
            return (Criteria) this;
        }

        public Criteria andProfileNotBetween(String value1, String value2) {
            addCriterion("profile not between", value1, value2, "profile");
            return (Criteria) this;
        }

        public Criteria andLonIsNull() {
            addCriterion("lon is null");
            return (Criteria) this;
        }

        public Criteria andLonIsNotNull() {
            addCriterion("lon is not null");
            return (Criteria) this;
        }

        public Criteria andLonEqualTo(String value) {
            addCriterion("lon =", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonNotEqualTo(String value) {
            addCriterion("lon <>", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonGreaterThan(String value) {
            addCriterion("lon >", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonGreaterThanOrEqualTo(String value) {
            addCriterion("lon >=", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonLessThan(String value) {
            addCriterion("lon <", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonLessThanOrEqualTo(String value) {
            addCriterion("lon <=", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonLike(String value) {
            addCriterion("lon like", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonNotLike(String value) {
            addCriterion("lon not like", value, "lon");
            return (Criteria) this;
        }

        public Criteria andLonIn(List<String> values) {
            addCriterion("lon in", values, "lon");
            return (Criteria) this;
        }

        public Criteria andLonNotIn(List<String> values) {
            addCriterion("lon not in", values, "lon");
            return (Criteria) this;
        }

        public Criteria andLonBetween(String value1, String value2) {
            addCriterion("lon between", value1, value2, "lon");
            return (Criteria) this;
        }

        public Criteria andLonNotBetween(String value1, String value2) {
            addCriterion("lon not between", value1, value2, "lon");
            return (Criteria) this;
        }

        public Criteria andLatIsNull() {
            addCriterion("lat is null");
            return (Criteria) this;
        }

        public Criteria andLatIsNotNull() {
            addCriterion("lat is not null");
            return (Criteria) this;
        }

        public Criteria andLatEqualTo(String value) {
            addCriterion("lat =", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotEqualTo(String value) {
            addCriterion("lat <>", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThan(String value) {
            addCriterion("lat >", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatGreaterThanOrEqualTo(String value) {
            addCriterion("lat >=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThan(String value) {
            addCriterion("lat <", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLessThanOrEqualTo(String value) {
            addCriterion("lat <=", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatLike(String value) {
            addCriterion("lat like", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotLike(String value) {
            addCriterion("lat not like", value, "lat");
            return (Criteria) this;
        }

        public Criteria andLatIn(List<String> values) {
            addCriterion("lat in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotIn(List<String> values) {
            addCriterion("lat not in", values, "lat");
            return (Criteria) this;
        }

        public Criteria andLatBetween(String value1, String value2) {
            addCriterion("lat between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andLatNotBetween(String value1, String value2) {
            addCriterion("lat not between", value1, value2, "lat");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityIsNull() {
            addCriterion("base_facility is null");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityIsNotNull() {
            addCriterion("base_facility is not null");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityEqualTo(String value) {
            addCriterion("base_facility =", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityNotEqualTo(String value) {
            addCriterion("base_facility <>", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityGreaterThan(String value) {
            addCriterion("base_facility >", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityGreaterThanOrEqualTo(String value) {
            addCriterion("base_facility >=", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityLessThan(String value) {
            addCriterion("base_facility <", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityLessThanOrEqualTo(String value) {
            addCriterion("base_facility <=", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityLike(String value) {
            addCriterion("base_facility like", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityNotLike(String value) {
            addCriterion("base_facility not like", value, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityIn(List<String> values) {
            addCriterion("base_facility in", values, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityNotIn(List<String> values) {
            addCriterion("base_facility not in", values, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityBetween(String value1, String value2) {
            addCriterion("base_facility between", value1, value2, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseFacilityNotBetween(String value1, String value2) {
            addCriterion("base_facility not between", value1, value2, "baseFacility");
            return (Criteria) this;
        }

        public Criteria andBaseServiceIsNull() {
            addCriterion("base_service is null");
            return (Criteria) this;
        }

        public Criteria andBaseServiceIsNotNull() {
            addCriterion("base_service is not null");
            return (Criteria) this;
        }

        public Criteria andBaseServiceEqualTo(String value) {
            addCriterion("base_service =", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceNotEqualTo(String value) {
            addCriterion("base_service <>", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceGreaterThan(String value) {
            addCriterion("base_service >", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceGreaterThanOrEqualTo(String value) {
            addCriterion("base_service >=", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceLessThan(String value) {
            addCriterion("base_service <", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceLessThanOrEqualTo(String value) {
            addCriterion("base_service <=", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceLike(String value) {
            addCriterion("base_service like", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceNotLike(String value) {
            addCriterion("base_service not like", value, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceIn(List<String> values) {
            addCriterion("base_service in", values, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceNotIn(List<String> values) {
            addCriterion("base_service not in", values, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceBetween(String value1, String value2) {
            addCriterion("base_service between", value1, value2, "baseService");
            return (Criteria) this;
        }

        public Criteria andBaseServiceNotBetween(String value1, String value2) {
            addCriterion("base_service not between", value1, value2, "baseService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceIsNull() {
            addCriterion("spe_service is null");
            return (Criteria) this;
        }

        public Criteria andSpeServiceIsNotNull() {
            addCriterion("spe_service is not null");
            return (Criteria) this;
        }

        public Criteria andSpeServiceEqualTo(String value) {
            addCriterion("spe_service =", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceNotEqualTo(String value) {
            addCriterion("spe_service <>", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceGreaterThan(String value) {
            addCriterion("spe_service >", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceGreaterThanOrEqualTo(String value) {
            addCriterion("spe_service >=", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceLessThan(String value) {
            addCriterion("spe_service <", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceLessThanOrEqualTo(String value) {
            addCriterion("spe_service <=", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceLike(String value) {
            addCriterion("spe_service like", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceNotLike(String value) {
            addCriterion("spe_service not like", value, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceIn(List<String> values) {
            addCriterion("spe_service in", values, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceNotIn(List<String> values) {
            addCriterion("spe_service not in", values, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceBetween(String value1, String value2) {
            addCriterion("spe_service between", value1, value2, "speService");
            return (Criteria) this;
        }

        public Criteria andSpeServiceNotBetween(String value1, String value2) {
            addCriterion("spe_service not between", value1, value2, "speService");
            return (Criteria) this;
        }

        public Criteria andTrafficIsNull() {
            addCriterion("traffic is null");
            return (Criteria) this;
        }

        public Criteria andTrafficIsNotNull() {
            addCriterion("traffic is not null");
            return (Criteria) this;
        }

        public Criteria andTrafficEqualTo(String value) {
            addCriterion("traffic =", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficNotEqualTo(String value) {
            addCriterion("traffic <>", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficGreaterThan(String value) {
            addCriterion("traffic >", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficGreaterThanOrEqualTo(String value) {
            addCriterion("traffic >=", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficLessThan(String value) {
            addCriterion("traffic <", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficLessThanOrEqualTo(String value) {
            addCriterion("traffic <=", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficLike(String value) {
            addCriterion("traffic like", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficNotLike(String value) {
            addCriterion("traffic not like", value, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficIn(List<String> values) {
            addCriterion("traffic in", values, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficNotIn(List<String> values) {
            addCriterion("traffic not in", values, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficBetween(String value1, String value2) {
            addCriterion("traffic between", value1, value2, "traffic");
            return (Criteria) this;
        }

        public Criteria andTrafficNotBetween(String value1, String value2) {
            addCriterion("traffic not between", value1, value2, "traffic");
            return (Criteria) this;
        }

        public Criteria andNearbyIsNull() {
            addCriterion("nearby is null");
            return (Criteria) this;
        }

        public Criteria andNearbyIsNotNull() {
            addCriterion("nearby is not null");
            return (Criteria) this;
        }

        public Criteria andNearbyEqualTo(String value) {
            addCriterion("nearby =", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyNotEqualTo(String value) {
            addCriterion("nearby <>", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyGreaterThan(String value) {
            addCriterion("nearby >", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyGreaterThanOrEqualTo(String value) {
            addCriterion("nearby >=", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyLessThan(String value) {
            addCriterion("nearby <", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyLessThanOrEqualTo(String value) {
            addCriterion("nearby <=", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyLike(String value) {
            addCriterion("nearby like", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyNotLike(String value) {
            addCriterion("nearby not like", value, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyIn(List<String> values) {
            addCriterion("nearby in", values, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyNotIn(List<String> values) {
            addCriterion("nearby not in", values, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyBetween(String value1, String value2) {
            addCriterion("nearby between", value1, value2, "nearby");
            return (Criteria) this;
        }

        public Criteria andNearbyNotBetween(String value1, String value2) {
            addCriterion("nearby not between", value1, value2, "nearby");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeIsNull() {
            addCriterion("office_type is null");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeIsNotNull() {
            addCriterion("office_type is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeEqualTo(Integer value) {
            addCriterion("office_type =", value, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeNotEqualTo(Integer value) {
            addCriterion("office_type <>", value, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeGreaterThan(Integer value) {
            addCriterion("office_type >", value, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("office_type >=", value, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeLessThan(Integer value) {
            addCriterion("office_type <", value, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeLessThanOrEqualTo(Integer value) {
            addCriterion("office_type <=", value, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeIn(List<Integer> values) {
            addCriterion("office_type in", values, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeNotIn(List<Integer> values) {
            addCriterion("office_type not in", values, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeBetween(Integer value1, Integer value2) {
            addCriterion("office_type between", value1, value2, "officeType");
            return (Criteria) this;
        }

        public Criteria andOfficeTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("office_type not between", value1, value2, "officeType");
            return (Criteria) this;
        }

        public Criteria andIsStableIsNull() {
            addCriterion("is_stable is null");
            return (Criteria) this;
        }

        public Criteria andIsStableIsNotNull() {
            addCriterion("is_stable is not null");
            return (Criteria) this;
        }

        public Criteria andIsStableEqualTo(Integer value) {
            addCriterion("is_stable =", value, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableNotEqualTo(Integer value) {
            addCriterion("is_stable <>", value, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableGreaterThan(Integer value) {
            addCriterion("is_stable >", value, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_stable >=", value, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableLessThan(Integer value) {
            addCriterion("is_stable <", value, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableLessThanOrEqualTo(Integer value) {
            addCriterion("is_stable <=", value, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableIn(List<Integer> values) {
            addCriterion("is_stable in", values, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableNotIn(List<Integer> values) {
            addCriterion("is_stable not in", values, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableBetween(Integer value1, Integer value2) {
            addCriterion("is_stable between", value1, value2, "isStable");
            return (Criteria) this;
        }

        public Criteria andIsStableNotBetween(Integer value1, Integer value2) {
            addCriterion("is_stable not between", value1, value2, "isStable");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceIsNull() {
            addCriterion("stable_open_price is null");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceIsNotNull() {
            addCriterion("stable_open_price is not null");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceEqualTo(Integer value) {
            addCriterion("stable_open_price =", value, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceNotEqualTo(Integer value) {
            addCriterion("stable_open_price <>", value, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceGreaterThan(Integer value) {
            addCriterion("stable_open_price >", value, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("stable_open_price >=", value, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceLessThan(Integer value) {
            addCriterion("stable_open_price <", value, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceLessThanOrEqualTo(Integer value) {
            addCriterion("stable_open_price <=", value, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceIn(List<Integer> values) {
            addCriterion("stable_open_price in", values, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceNotIn(List<Integer> values) {
            addCriterion("stable_open_price not in", values, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceBetween(Integer value1, Integer value2) {
            addCriterion("stable_open_price between", value1, value2, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableOpenPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("stable_open_price not between", value1, value2, "stableOpenPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceIsNull() {
            addCriterion("stable_specialized_price is null");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceIsNotNull() {
            addCriterion("stable_specialized_price is not null");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceEqualTo(Integer value) {
            addCriterion("stable_specialized_price =", value, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceNotEqualTo(Integer value) {
            addCriterion("stable_specialized_price <>", value, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceGreaterThan(Integer value) {
            addCriterion("stable_specialized_price >", value, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("stable_specialized_price >=", value, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceLessThan(Integer value) {
            addCriterion("stable_specialized_price <", value, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceLessThanOrEqualTo(Integer value) {
            addCriterion("stable_specialized_price <=", value, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceIn(List<Integer> values) {
            addCriterion("stable_specialized_price in", values, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceNotIn(List<Integer> values) {
            addCriterion("stable_specialized_price not in", values, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceBetween(Integer value1, Integer value2) {
            addCriterion("stable_specialized_price between", value1, value2, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStableSpecializedPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("stable_specialized_price not between", value1, value2, "stableSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleIsNull() {
            addCriterion("is_flexible is null");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleIsNotNull() {
            addCriterion("is_flexible is not null");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleEqualTo(Integer value) {
            addCriterion("is_flexible =", value, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleNotEqualTo(Integer value) {
            addCriterion("is_flexible <>", value, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleGreaterThan(Integer value) {
            addCriterion("is_flexible >", value, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_flexible >=", value, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleLessThan(Integer value) {
            addCriterion("is_flexible <", value, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleLessThanOrEqualTo(Integer value) {
            addCriterion("is_flexible <=", value, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleIn(List<Integer> values) {
            addCriterion("is_flexible in", values, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleNotIn(List<Integer> values) {
            addCriterion("is_flexible not in", values, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleBetween(Integer value1, Integer value2) {
            addCriterion("is_flexible between", value1, value2, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andIsFlexibleNotBetween(Integer value1, Integer value2) {
            addCriterion("is_flexible not between", value1, value2, "isFlexible");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceIsNull() {
            addCriterion("flexible_open_price is null");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceIsNotNull() {
            addCriterion("flexible_open_price is not null");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceEqualTo(Integer value) {
            addCriterion("flexible_open_price =", value, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceNotEqualTo(Integer value) {
            addCriterion("flexible_open_price <>", value, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceGreaterThan(Integer value) {
            addCriterion("flexible_open_price >", value, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("flexible_open_price >=", value, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceLessThan(Integer value) {
            addCriterion("flexible_open_price <", value, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceLessThanOrEqualTo(Integer value) {
            addCriterion("flexible_open_price <=", value, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceIn(List<Integer> values) {
            addCriterion("flexible_open_price in", values, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceNotIn(List<Integer> values) {
            addCriterion("flexible_open_price not in", values, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceBetween(Integer value1, Integer value2) {
            addCriterion("flexible_open_price between", value1, value2, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleOpenPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("flexible_open_price not between", value1, value2, "flexibleOpenPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceIsNull() {
            addCriterion("flexible_specialized_price is null");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceIsNotNull() {
            addCriterion("flexible_specialized_price is not null");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceEqualTo(Integer value) {
            addCriterion("flexible_specialized_price =", value, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceNotEqualTo(Integer value) {
            addCriterion("flexible_specialized_price <>", value, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceGreaterThan(Integer value) {
            addCriterion("flexible_specialized_price >", value, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("flexible_specialized_price >=", value, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceLessThan(Integer value) {
            addCriterion("flexible_specialized_price <", value, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceLessThanOrEqualTo(Integer value) {
            addCriterion("flexible_specialized_price <=", value, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceIn(List<Integer> values) {
            addCriterion("flexible_specialized_price in", values, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceNotIn(List<Integer> values) {
            addCriterion("flexible_specialized_price not in", values, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceBetween(Integer value1, Integer value2) {
            addCriterion("flexible_specialized_price between", value1, value2, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andFlexibleSpecializedPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("flexible_specialized_price not between", value1, value2, "flexibleSpecializedPrice");
            return (Criteria) this;
        }

        public Criteria andStationNumIsNull() {
            addCriterion("station_num is null");
            return (Criteria) this;
        }

        public Criteria andStationNumIsNotNull() {
            addCriterion("station_num is not null");
            return (Criteria) this;
        }

        public Criteria andStationNumEqualTo(Integer value) {
            addCriterion("station_num =", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumNotEqualTo(Integer value) {
            addCriterion("station_num <>", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumGreaterThan(Integer value) {
            addCriterion("station_num >", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("station_num >=", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumLessThan(Integer value) {
            addCriterion("station_num <", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumLessThanOrEqualTo(Integer value) {
            addCriterion("station_num <=", value, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumIn(List<Integer> values) {
            addCriterion("station_num in", values, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumNotIn(List<Integer> values) {
            addCriterion("station_num not in", values, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumBetween(Integer value1, Integer value2) {
            addCriterion("station_num between", value1, value2, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStationNumNotBetween(Integer value1, Integer value2) {
            addCriterion("station_num not between", value1, value2, "stationNum");
            return (Criteria) this;
        }

        public Criteria andStarNumIsNull() {
            addCriterion("star_num is null");
            return (Criteria) this;
        }

        public Criteria andStarNumIsNotNull() {
            addCriterion("star_num is not null");
            return (Criteria) this;
        }

        public Criteria andStarNumEqualTo(Integer value) {
            addCriterion("star_num =", value, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumNotEqualTo(Integer value) {
            addCriterion("star_num <>", value, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumGreaterThan(Integer value) {
            addCriterion("star_num >", value, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("star_num >=", value, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumLessThan(Integer value) {
            addCriterion("star_num <", value, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumLessThanOrEqualTo(Integer value) {
            addCriterion("star_num <=", value, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumIn(List<Integer> values) {
            addCriterion("star_num in", values, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumNotIn(List<Integer> values) {
            addCriterion("star_num not in", values, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumBetween(Integer value1, Integer value2) {
            addCriterion("star_num between", value1, value2, "starNum");
            return (Criteria) this;
        }

        public Criteria andStarNumNotBetween(Integer value1, Integer value2) {
            addCriterion("star_num not between", value1, value2, "starNum");
            return (Criteria) this;
        }

        public Criteria andMinTimeIsNull() {
            addCriterion("min_time is null");
            return (Criteria) this;
        }

        public Criteria andMinTimeIsNotNull() {
            addCriterion("min_time is not null");
            return (Criteria) this;
        }

        public Criteria andMinTimeEqualTo(Integer value) {
            addCriterion("min_time =", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeNotEqualTo(Integer value) {
            addCriterion("min_time <>", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeGreaterThan(Integer value) {
            addCriterion("min_time >", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("min_time >=", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeLessThan(Integer value) {
            addCriterion("min_time <", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeLessThanOrEqualTo(Integer value) {
            addCriterion("min_time <=", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeIn(List<Integer> values) {
            addCriterion("min_time in", values, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeNotIn(List<Integer> values) {
            addCriterion("min_time not in", values, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeBetween(Integer value1, Integer value2) {
            addCriterion("min_time between", value1, value2, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("min_time not between", value1, value2, "minTime");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("update_date is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("update_date is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("update_date =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("update_date <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("update_date >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("update_date >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("update_date <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("update_date <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("update_date in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("update_date not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("update_date between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("update_date not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNull() {
            addCriterion("update_by is null");
            return (Criteria) this;
        }

        public Criteria andUpdateByIsNotNull() {
            addCriterion("update_by is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateByEqualTo(String value) {
            addCriterion("update_by =", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotEqualTo(String value) {
            addCriterion("update_by <>", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThan(String value) {
            addCriterion("update_by >", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByGreaterThanOrEqualTo(String value) {
            addCriterion("update_by >=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThan(String value) {
            addCriterion("update_by <", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLessThanOrEqualTo(String value) {
            addCriterion("update_by <=", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByLike(String value) {
            addCriterion("update_by like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotLike(String value) {
            addCriterion("update_by not like", value, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByIn(List<String> values) {
            addCriterion("update_by in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotIn(List<String> values) {
            addCriterion("update_by not in", values, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByBetween(String value1, String value2) {
            addCriterion("update_by between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andUpdateByNotBetween(String value1, String value2) {
            addCriterion("update_by not between", value1, value2, "updateBy");
            return (Criteria) this;
        }

        public Criteria andDetailAddrIsNull() {
            addCriterion("detail_addr is null");
            return (Criteria) this;
        }

        public Criteria andDetailAddrIsNotNull() {
            addCriterion("detail_addr is not null");
            return (Criteria) this;
        }

        public Criteria andDetailAddrEqualTo(String value) {
            addCriterion("detail_addr =", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrNotEqualTo(String value) {
            addCriterion("detail_addr <>", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrGreaterThan(String value) {
            addCriterion("detail_addr >", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrGreaterThanOrEqualTo(String value) {
            addCriterion("detail_addr >=", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrLessThan(String value) {
            addCriterion("detail_addr <", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrLessThanOrEqualTo(String value) {
            addCriterion("detail_addr <=", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrLike(String value) {
            addCriterion("detail_addr like", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrNotLike(String value) {
            addCriterion("detail_addr not like", value, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrIn(List<String> values) {
            addCriterion("detail_addr in", values, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrNotIn(List<String> values) {
            addCriterion("detail_addr not in", values, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrBetween(String value1, String value2) {
            addCriterion("detail_addr between", value1, value2, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andDetailAddrNotBetween(String value1, String value2) {
            addCriterion("detail_addr not between", value1, value2, "detailAddr");
            return (Criteria) this;
        }

        public Criteria andContactMobileIsNull() {
            addCriterion("contact_mobile is null");
            return (Criteria) this;
        }

        public Criteria andContactMobileIsNotNull() {
            addCriterion("contact_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andContactMobileEqualTo(String value) {
            addCriterion("contact_mobile =", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotEqualTo(String value) {
            addCriterion("contact_mobile <>", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileGreaterThan(String value) {
            addCriterion("contact_mobile >", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileGreaterThanOrEqualTo(String value) {
            addCriterion("contact_mobile >=", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileLessThan(String value) {
            addCriterion("contact_mobile <", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileLessThanOrEqualTo(String value) {
            addCriterion("contact_mobile <=", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileLike(String value) {
            addCriterion("contact_mobile like", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotLike(String value) {
            addCriterion("contact_mobile not like", value, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileIn(List<String> values) {
            addCriterion("contact_mobile in", values, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotIn(List<String> values) {
            addCriterion("contact_mobile not in", values, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileBetween(String value1, String value2) {
            addCriterion("contact_mobile between", value1, value2, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andContactMobileNotBetween(String value1, String value2) {
            addCriterion("contact_mobile not between", value1, value2, "contactMobile");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(String value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(String value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(String value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(String value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(String value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(String value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLike(String value) {
            addCriterion("area like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotLike(String value) {
            addCriterion("area not like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<String> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<String> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(String value1, String value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(String value1, String value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andFeatureIsNull() {
            addCriterion("feature is null");
            return (Criteria) this;
        }

        public Criteria andFeatureIsNotNull() {
            addCriterion("feature is not null");
            return (Criteria) this;
        }

        public Criteria andFeatureEqualTo(String value) {
            addCriterion("feature =", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotEqualTo(String value) {
            addCriterion("feature <>", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureGreaterThan(String value) {
            addCriterion("feature >", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureGreaterThanOrEqualTo(String value) {
            addCriterion("feature >=", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureLessThan(String value) {
            addCriterion("feature <", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureLessThanOrEqualTo(String value) {
            addCriterion("feature <=", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureLike(String value) {
            addCriterion("feature like", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotLike(String value) {
            addCriterion("feature not like", value, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureIn(List<String> values) {
            addCriterion("feature in", values, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotIn(List<String> values) {
            addCriterion("feature not in", values, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureBetween(String value1, String value2) {
            addCriterion("feature between", value1, value2, "feature");
            return (Criteria) this;
        }

        public Criteria andFeatureNotBetween(String value1, String value2) {
            addCriterion("feature not between", value1, value2, "feature");
            return (Criteria) this;
        }

        public Criteria andOtherTagsIsNull() {
            addCriterion("other_tags is null");
            return (Criteria) this;
        }

        public Criteria andOtherTagsIsNotNull() {
            addCriterion("other_tags is not null");
            return (Criteria) this;
        }

        public Criteria andOtherTagsEqualTo(String value) {
            addCriterion("other_tags =", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsNotEqualTo(String value) {
            addCriterion("other_tags <>", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsGreaterThan(String value) {
            addCriterion("other_tags >", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsGreaterThanOrEqualTo(String value) {
            addCriterion("other_tags >=", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsLessThan(String value) {
            addCriterion("other_tags <", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsLessThanOrEqualTo(String value) {
            addCriterion("other_tags <=", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsLike(String value) {
            addCriterion("other_tags like", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsNotLike(String value) {
            addCriterion("other_tags not like", value, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsIn(List<String> values) {
            addCriterion("other_tags in", values, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsNotIn(List<String> values) {
            addCriterion("other_tags not in", values, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsBetween(String value1, String value2) {
            addCriterion("other_tags between", value1, value2, "otherTags");
            return (Criteria) this;
        }

        public Criteria andOtherTagsNotBetween(String value1, String value2) {
            addCriterion("other_tags not between", value1, value2, "otherTags");
            return (Criteria) this;
        }

        public Criteria andContactIsNull() {
            addCriterion("contact is null");
            return (Criteria) this;
        }

        public Criteria andContactIsNotNull() {
            addCriterion("contact is not null");
            return (Criteria) this;
        }

        public Criteria andContactEqualTo(String value) {
            addCriterion("contact =", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotEqualTo(String value) {
            addCriterion("contact <>", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactGreaterThan(String value) {
            addCriterion("contact >", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactGreaterThanOrEqualTo(String value) {
            addCriterion("contact >=", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactLessThan(String value) {
            addCriterion("contact <", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactLessThanOrEqualTo(String value) {
            addCriterion("contact <=", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactLike(String value) {
            addCriterion("contact like", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotLike(String value) {
            addCriterion("contact not like", value, "contact");
            return (Criteria) this;
        }

        public Criteria andContactIn(List<String> values) {
            addCriterion("contact in", values, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotIn(List<String> values) {
            addCriterion("contact not in", values, "contact");
            return (Criteria) this;
        }

        public Criteria andContactBetween(String value1, String value2) {
            addCriterion("contact between", value1, value2, "contact");
            return (Criteria) this;
        }

        public Criteria andContactNotBetween(String value1, String value2) {
            addCriterion("contact not between", value1, value2, "contact");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}