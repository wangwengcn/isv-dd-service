package com.prisetree.isv.domain.query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OfficeResourcesRentExample {

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OfficeResourcesRentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andCountIsNull() {
            addCriterion("count is null");
            return (Criteria) this;
        }

        public Criteria andCountIsNotNull() {
            addCriterion("count is not null");
            return (Criteria) this;
        }

        public Criteria andCountEqualTo(Integer value) {
            addCriterion("count =", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotEqualTo(Integer value) {
            addCriterion("count <>", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThan(Integer value) {
            addCriterion("count >", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("count >=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThan(Integer value) {
            addCriterion("count <", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountLessThanOrEqualTo(Integer value) {
            addCriterion("count <=", value, "count");
            return (Criteria) this;
        }

        public Criteria andCountIn(List<Integer> values) {
            addCriterion("count in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotIn(List<Integer> values) {
            addCriterion("count not in", values, "count");
            return (Criteria) this;
        }

        public Criteria andCountBetween(Integer value1, Integer value2) {
            addCriterion("count between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andCountNotBetween(Integer value1, Integer value2) {
            addCriterion("count not between", value1, value2, "count");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceIsNull() {
            addCriterion("staiton_price is null");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceIsNotNull() {
            addCriterion("staiton_price is not null");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceEqualTo(Integer value) {
            addCriterion("staiton_price =", value, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceNotEqualTo(Integer value) {
            addCriterion("staiton_price <>", value, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceGreaterThan(Integer value) {
            addCriterion("staiton_price >", value, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceGreaterThanOrEqualTo(Integer value) {
            addCriterion("staiton_price >=", value, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceLessThan(Integer value) {
            addCriterion("staiton_price <", value, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceLessThanOrEqualTo(Integer value) {
            addCriterion("staiton_price <=", value, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceIn(List<Integer> values) {
            addCriterion("staiton_price in", values, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceNotIn(List<Integer> values) {
            addCriterion("staiton_price not in", values, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceBetween(Integer value1, Integer value2) {
            addCriterion("staiton_price between", value1, value2, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStaitonPriceNotBetween(Integer value1, Integer value2) {
            addCriterion("staiton_price not between", value1, value2, "staitonPrice");
            return (Criteria) this;
        }

        public Criteria andStationAllnumIsNull() {
            addCriterion("station_allnum is null");
            return (Criteria) this;
        }

        public Criteria andStationAllnumIsNotNull() {
            addCriterion("station_allnum is not null");
            return (Criteria) this;
        }

        public Criteria andStationAllnumEqualTo(Integer value) {
            addCriterion("station_allnum =", value, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumNotEqualTo(Integer value) {
            addCriterion("station_allnum <>", value, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumGreaterThan(Integer value) {
            addCriterion("station_allnum >", value, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("station_allnum >=", value, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumLessThan(Integer value) {
            addCriterion("station_allnum <", value, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumLessThanOrEqualTo(Integer value) {
            addCriterion("station_allnum <=", value, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumIn(List<Integer> values) {
            addCriterion("station_allnum in", values, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumNotIn(List<Integer> values) {
            addCriterion("station_allnum not in", values, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumBetween(Integer value1, Integer value2) {
            addCriterion("station_allnum between", value1, value2, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStationAllnumNotBetween(Integer value1, Integer value2) {
            addCriterion("station_allnum not between", value1, value2, "stationAllnum");
            return (Criteria) this;
        }

        public Criteria andStandardNumIsNull() {
            addCriterion("standard_num is null");
            return (Criteria) this;
        }

        public Criteria andStandardNumIsNotNull() {
            addCriterion("standard_num is not null");
            return (Criteria) this;
        }

        public Criteria andStandardNumEqualTo(Integer value) {
            addCriterion("standard_num =", value, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumNotEqualTo(Integer value) {
            addCriterion("standard_num <>", value, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumGreaterThan(Integer value) {
            addCriterion("standard_num >", value, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("standard_num >=", value, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumLessThan(Integer value) {
            addCriterion("standard_num <", value, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumLessThanOrEqualTo(Integer value) {
            addCriterion("standard_num <=", value, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumIn(List<Integer> values) {
            addCriterion("standard_num in", values, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumNotIn(List<Integer> values) {
            addCriterion("standard_num not in", values, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumBetween(Integer value1, Integer value2) {
            addCriterion("standard_num between", value1, value2, "standardNum");
            return (Criteria) this;
        }

        public Criteria andStandardNumNotBetween(Integer value1, Integer value2) {
            addCriterion("standard_num not between", value1, value2, "standardNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumIsNull() {
            addCriterion("surplus_num is null");
            return (Criteria) this;
        }

        public Criteria andSurplusNumIsNotNull() {
            addCriterion("surplus_num is not null");
            return (Criteria) this;
        }

        public Criteria andSurplusNumEqualTo(Integer value) {
            addCriterion("surplus_num =", value, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumNotEqualTo(Integer value) {
            addCriterion("surplus_num <>", value, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumGreaterThan(Integer value) {
            addCriterion("surplus_num >", value, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("surplus_num >=", value, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumLessThan(Integer value) {
            addCriterion("surplus_num <", value, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumLessThanOrEqualTo(Integer value) {
            addCriterion("surplus_num <=", value, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumIn(List<Integer> values) {
            addCriterion("surplus_num in", values, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumNotIn(List<Integer> values) {
            addCriterion("surplus_num not in", values, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumBetween(Integer value1, Integer value2) {
            addCriterion("surplus_num between", value1, value2, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andSurplusNumNotBetween(Integer value1, Integer value2) {
            addCriterion("surplus_num not between", value1, value2, "surplusNum");
            return (Criteria) this;
        }

        public Criteria andStationTypeIsNull() {
            addCriterion("station_type is null");
            return (Criteria) this;
        }

        public Criteria andStationTypeIsNotNull() {
            addCriterion("station_type is not null");
            return (Criteria) this;
        }

        public Criteria andStationTypeEqualTo(Integer value) {
            addCriterion("station_type =", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeNotEqualTo(Integer value) {
            addCriterion("station_type <>", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeGreaterThan(Integer value) {
            addCriterion("station_type >", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("station_type >=", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeLessThan(Integer value) {
            addCriterion("station_type <", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeLessThanOrEqualTo(Integer value) {
            addCriterion("station_type <=", value, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeIn(List<Integer> values) {
            addCriterion("station_type in", values, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeNotIn(List<Integer> values) {
            addCriterion("station_type not in", values, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeBetween(Integer value1, Integer value2) {
            addCriterion("station_type between", value1, value2, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("station_type not between", value1, value2, "stationType");
            return (Criteria) this;
        }

        public Criteria andStationAttrIsNull() {
            addCriterion("station_attr is null");
            return (Criteria) this;
        }

        public Criteria andStationAttrIsNotNull() {
            addCriterion("station_attr is not null");
            return (Criteria) this;
        }

        public Criteria andStationAttrEqualTo(Integer value) {
            addCriterion("station_attr =", value, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrNotEqualTo(Integer value) {
            addCriterion("station_attr <>", value, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrGreaterThan(Integer value) {
            addCriterion("station_attr >", value, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrGreaterThanOrEqualTo(Integer value) {
            addCriterion("station_attr >=", value, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrLessThan(Integer value) {
            addCriterion("station_attr <", value, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrLessThanOrEqualTo(Integer value) {
            addCriterion("station_attr <=", value, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrIn(List<Integer> values) {
            addCriterion("station_attr in", values, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrNotIn(List<Integer> values) {
            addCriterion("station_attr not in", values, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrBetween(Integer value1, Integer value2) {
            addCriterion("station_attr between", value1, value2, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andStationAttrNotBetween(Integer value1, Integer value2) {
            addCriterion("station_attr not between", value1, value2, "stationAttr");
            return (Criteria) this;
        }

        public Criteria andBillingModeIsNull() {
            addCriterion("billing_mode is null");
            return (Criteria) this;
        }

        public Criteria andBillingModeIsNotNull() {
            addCriterion("billing_mode is not null");
            return (Criteria) this;
        }

        public Criteria andBillingModeEqualTo(Integer value) {
            addCriterion("billing_mode =", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeNotEqualTo(Integer value) {
            addCriterion("billing_mode <>", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeGreaterThan(Integer value) {
            addCriterion("billing_mode >", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeGreaterThanOrEqualTo(Integer value) {
            addCriterion("billing_mode >=", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeLessThan(Integer value) {
            addCriterion("billing_mode <", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeLessThanOrEqualTo(Integer value) {
            addCriterion("billing_mode <=", value, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeIn(List<Integer> values) {
            addCriterion("billing_mode in", values, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeNotIn(List<Integer> values) {
            addCriterion("billing_mode not in", values, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeBetween(Integer value1, Integer value2) {
            addCriterion("billing_mode between", value1, value2, "billingMode");
            return (Criteria) this;
        }

        public Criteria andBillingModeNotBetween(Integer value1, Integer value2) {
            addCriterion("billing_mode not between", value1, value2, "billingMode");
            return (Criteria) this;
        }

        public Criteria andMinTimeIsNull() {
            addCriterion("min_time is null");
            return (Criteria) this;
        }

        public Criteria andMinTimeIsNotNull() {
            addCriterion("min_time is not null");
            return (Criteria) this;
        }

        public Criteria andMinTimeEqualTo(Integer value) {
            addCriterion("min_time =", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeNotEqualTo(Integer value) {
            addCriterion("min_time <>", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeGreaterThan(Integer value) {
            addCriterion("min_time >", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("min_time >=", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeLessThan(Integer value) {
            addCriterion("min_time <", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeLessThanOrEqualTo(Integer value) {
            addCriterion("min_time <=", value, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeIn(List<Integer> values) {
            addCriterion("min_time in", values, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeNotIn(List<Integer> values) {
            addCriterion("min_time not in", values, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeBetween(Integer value1, Integer value2) {
            addCriterion("min_time between", value1, value2, "minTime");
            return (Criteria) this;
        }

        public Criteria andMinTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("min_time not between", value1, value2, "minTime");
            return (Criteria) this;
        }

        public Criteria andUnitPriceIsNull() {
            addCriterion("unit_price is null");
            return (Criteria) this;
        }

        public Criteria andUnitPriceIsNotNull() {
            addCriterion("unit_price is not null");
            return (Criteria) this;
        }

        public Criteria andUnitPriceEqualTo(Double value) {
            addCriterion("unit_price =", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceNotEqualTo(Double value) {
            addCriterion("unit_price <>", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceGreaterThan(Double value) {
            addCriterion("unit_price >", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("unit_price >=", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceLessThan(Double value) {
            addCriterion("unit_price <", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceLessThanOrEqualTo(Double value) {
            addCriterion("unit_price <=", value, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceIn(List<Double> values) {
            addCriterion("unit_price in", values, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceNotIn(List<Double> values) {
            addCriterion("unit_price not in", values, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceBetween(Double value1, Double value2) {
            addCriterion("unit_price between", value1, value2, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andUnitPriceNotBetween(Double value1, Double value2) {
            addCriterion("unit_price not between", value1, value2, "unitPrice");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(Double value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(Double value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(Double value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(Double value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(Double value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(Double value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<Double> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<Double> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(Double value1, Double value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(Double value1, Double value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaPriceIsNull() {
            addCriterion("area_price is null");
            return (Criteria) this;
        }

        public Criteria andAreaPriceIsNotNull() {
            addCriterion("area_price is not null");
            return (Criteria) this;
        }

        public Criteria andAreaPriceEqualTo(Double value) {
            addCriterion("area_price =", value, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceNotEqualTo(Double value) {
            addCriterion("area_price <>", value, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceGreaterThan(Double value) {
            addCriterion("area_price >", value, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("area_price >=", value, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceLessThan(Double value) {
            addCriterion("area_price <", value, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceLessThanOrEqualTo(Double value) {
            addCriterion("area_price <=", value, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceIn(List<Double> values) {
            addCriterion("area_price in", values, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceNotIn(List<Double> values) {
            addCriterion("area_price not in", values, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceBetween(Double value1, Double value2) {
            addCriterion("area_price between", value1, value2, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andAreaPriceNotBetween(Double value1, Double value2) {
            addCriterion("area_price not between", value1, value2, "areaPrice");
            return (Criteria) this;
        }

        public Criteria andUsageRateIsNull() {
            addCriterion("usage_rate is null");
            return (Criteria) this;
        }

        public Criteria andUsageRateIsNotNull() {
            addCriterion("usage_rate is not null");
            return (Criteria) this;
        }

        public Criteria andUsageRateEqualTo(Double value) {
            addCriterion("usage_rate =", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateNotEqualTo(Double value) {
            addCriterion("usage_rate <>", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateGreaterThan(Double value) {
            addCriterion("usage_rate >", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateGreaterThanOrEqualTo(Double value) {
            addCriterion("usage_rate >=", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateLessThan(Double value) {
            addCriterion("usage_rate <", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateLessThanOrEqualTo(Double value) {
            addCriterion("usage_rate <=", value, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateIn(List<Double> values) {
            addCriterion("usage_rate in", values, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateNotIn(List<Double> values) {
            addCriterion("usage_rate not in", values, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateBetween(Double value1, Double value2) {
            addCriterion("usage_rate between", value1, value2, "usageRate");
            return (Criteria) this;
        }

        public Criteria andUsageRateNotBetween(Double value1, Double value2) {
            addCriterion("usage_rate not between", value1, value2, "usageRate");
            return (Criteria) this;
        }

        public Criteria andRentTypeIsNull() {
            addCriterion("rent_type is null");
            return (Criteria) this;
        }

        public Criteria andRentTypeIsNotNull() {
            addCriterion("rent_type is not null");
            return (Criteria) this;
        }

        public Criteria andRentTypeEqualTo(Integer value) {
            addCriterion("rent_type =", value, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeNotEqualTo(Integer value) {
            addCriterion("rent_type <>", value, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeGreaterThan(Integer value) {
            addCriterion("rent_type >", value, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("rent_type >=", value, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeLessThan(Integer value) {
            addCriterion("rent_type <", value, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeLessThanOrEqualTo(Integer value) {
            addCriterion("rent_type <=", value, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeIn(List<Integer> values) {
            addCriterion("rent_type in", values, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeNotIn(List<Integer> values) {
            addCriterion("rent_type not in", values, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeBetween(Integer value1, Integer value2) {
            addCriterion("rent_type between", value1, value2, "rentType");
            return (Criteria) this;
        }

        public Criteria andRentTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("rent_type not between", value1, value2, "rentType");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parent_id is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(String value) {
            addCriterion("parent_id =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(String value) {
            addCriterion("parent_id <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(String value) {
            addCriterion("parent_id >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(String value) {
            addCriterion("parent_id >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(String value) {
            addCriterion("parent_id <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(String value) {
            addCriterion("parent_id <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLike(String value) {
            addCriterion("parent_id like", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotLike(String value) {
            addCriterion("parent_id not like", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<String> values) {
            addCriterion("parent_id in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<String> values) {
            addCriterion("parent_id not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(String value1, String value2) {
            addCriterion("parent_id between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(String value1, String value2) {
            addCriterion("parent_id not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("create_date is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("create_date is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("create_date =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("create_date <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("create_date >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("create_date >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("create_date <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("create_date <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("create_date in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("create_date not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("create_date between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("create_date not between", value1, value2, "createDate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}