package com.prisetree.isv.domain;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;

public class Orders {
    /**
     * ID
     */
    private String id;
    
    /**
     * 订单编号
     */
    private String orderNumber;
    
    /**
     * 房源ID
     */
    private String officeResourcesId;
    
    /**
     * 房源名称
     */
    private String officeResourcesName;

    /**
     * 公司id（钉钉）
     */
    private String companyId;
    
    /**
     * 公司名称（钉钉）
     */
    private String companyName;
    
    /**
     * 公司手机号
     */
    private String companyMobile;

    /**
     * 入驻时间
     */
    @JSONField(format = "yyyy-MM-dd")
    private Date enterTime;

    /**
     * 到期时间
     */
    @JSONField(format = "yyyy-MM-dd")
    private Date leaveTime;

    /**
     * 工位数
     */
    private Integer stationNum;

    /**
     * 总价
     */
    private Double totalPrice;
    
    /**
     * 单价
     */
    private Double unitPrice;
    
    /**
     * 押金
     */
    private Double deposit;

    /**
     * 状态(审核中0,审核通过1,审核未通过2,订单已完成3)
     */
    private String state;
    
    /**
     * 订单类型(0租固定；1租写字楼；预定临时；预定看场地)
     */
    private String type;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 房源工位id
     */
    private String officeResourcesRentId;

    /**
     * 房源工位id
     */
    private String officeResourcesRentName;


    /**
     * 房源地址
     */
    private String officeResourcesAddr;

    /**
     * 房源联系方式
     */
    private String officeResourcesMobile;

    /**
     * 周期
     */
    private Integer unit;

    /**
     * 计费模式 ：0：天  1：周  2：月
     */
    private String billingMode;

    /**
     * 工位类型(0独立，1开放)
     */
    private String stationType;
    
    /**
     * 面积
     */
    private Double area;
    
    /**
     * 得房率(使用率)
     */
    private Double usageRate;
    
    /**
     * 钉钉userid，无业务意义
     */
    private String dingUserId;
    
	public String getDingUserId() {
		return dingUserId;
	}

	public void setDingUserId(String dingUserId) {
		this.dingUserId = dingUserId;
	}

	public String getOfficeResourcesRentId() {
		return officeResourcesRentId;
	}

	public void setOfficeResourcesRentId(String officeResourcesRentId) {
		this.officeResourcesRentId = officeResourcesRentId;
	}

	public String getOfficeResourcesAddr() {
		return officeResourcesAddr;
	}

	public void setOfficeResourcesAddr(String officeResourcesAddr) {
		this.officeResourcesAddr = officeResourcesAddr;
	}

	public String getOfficeResourcesMobile() {
		return officeResourcesMobile;
	}

	public void setOfficeResourcesMobile(String officeResourcesMobile) {
		this.officeResourcesMobile = officeResourcesMobile;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public String getBillingMode() {
		return billingMode;
	}

	public void setBillingMode(String billingMode) {
		this.billingMode = billingMode;
	}

	public String getOfficeResourcesName() {
		return officeResourcesName;
	}

	public void setOfficeResourcesName(String officeResourcesName) {
		this.officeResourcesName = officeResourcesName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfficeResourcesId() {
		return officeResourcesId;
	}

	public void setOfficeResourcesId(String officeResourcesId) {
		this.officeResourcesId = officeResourcesId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	public Date getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(Date enterTime) {
		this.enterTime = enterTime;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	public Date getLeaveTime() {
		return leaveTime;
	}

	public void setLeaveTime(Date leaveTime) {
		this.leaveTime = leaveTime;
	}

	public Integer getStationNum() {
		return stationNum;
	}

	public void setStationNum(Integer stationNum) {
		this.stationNum = stationNum;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCompanyMobile() {
		return companyMobile;
	}

	public void setCompanyMobile(String companyMobile) {
		this.companyMobile = companyMobile;
	}

	public String getStationType() {
		return stationType;
	}

	public void setStationType(String stationType) {
		this.stationType = stationType;
	}

	public String getOfficeResourcesRentName() {
		return officeResourcesRentName;
	}

	public void setOfficeResourcesRentName(String officeResourcesRentName) {
		this.officeResourcesRentName = officeResourcesRentName;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public Double getUsageRate() {
		return usageRate;
	}

	public void setUsageRate(Double usageRate) {
		this.usageRate = usageRate;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Double getDeposit() {
		return deposit;
	}

	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}

}