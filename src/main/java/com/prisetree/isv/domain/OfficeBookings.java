package com.prisetree.isv.domain;

import java.util.Date;

public class OfficeBookings {

	//预约订单id
    private String id;
    //预约订单状态(预约中0,已预约1)
    private String bookingState;
    //企业ID
    private String companyId;
    //企业名称
    private String companyName;
    //企业联系电话
    private String companyMobile;
    //房源ID
    private String officeResourcesId;
    //房源名称
    private String officeResourcesName;
    //房源地址
    private String officeResourcesAdd;
    //房源联系电话
    private String officeResourcesMobile;
    //更新时间
    private Date createDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getBookingState() {
        return bookingState;
    }

    public void setBookingState(String bookingState) {
        this.bookingState = bookingState == null ? null : bookingState.trim();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getCompanyMobile() {
        return companyMobile;
    }

    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile == null ? null : companyMobile.trim();
    }

    public String getOfficeResourcesId() {
        return officeResourcesId;
    }

    public void setOfficeResourcesId(String officeResourcesId) {
        this.officeResourcesId = officeResourcesId == null ? null : officeResourcesId.trim();
    }

    public String getOfficeResourcesName() {
        return officeResourcesName;
    }

    public void setOfficeResourcesName(String officeResourcesName) {
        this.officeResourcesName = officeResourcesName == null ? null : officeResourcesName.trim();
    }

    public String getOfficeResourcesAdd() {
        return officeResourcesAdd;
    }

    public void setOfficeResourcesAdd(String officeResourcesAdd) {
        this.officeResourcesAdd = officeResourcesAdd == null ? null : officeResourcesAdd.trim();
    }

    public String getOfficeResourcesMobile() {
        return officeResourcesMobile;
    }

    public void setOfficeResourcesMobile(String officeResourcesMobile) {
        this.officeResourcesMobile = officeResourcesMobile == null ? null : officeResourcesMobile.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}