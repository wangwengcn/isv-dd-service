package com.prisetree.isv.response.vo;

/**
 * 新增用户接口请求参数
 * 
 * @author Wenchao
 *
 */
public class UserLoginResponse {

	private String token;
	private String userId;
	private String lastLoginDate;
	private String userType;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
