package com.prisetree.isv.response.vo;

/**
 * csrftoken
 * @author y
 */
public class CSRFTokenResponse {
	
	private String token; //csrftoken

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
