package com.prisetree.isv.response.vo;

/**
 * 接口统一返回数据对象类.
* Class Name: ResultData
* Description: 统一返回数据对象类
*
 */
public class ResultData {
//	private static ResultData resultData = null;
	
    /** 响应码：0成功.   */
    private int code;

    /** 响应数据. */
    private Object data;

    /** 消息文本. */
    private Object msg;


	/**
     * 默认构造方法-code:NACK.
    * ResultData Constructor
    *
     */
    public ResultData() {
        this.code = 0;
        this.msg = "SUCCESS";
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public static ResultData getInstance()
    {
    	return new ResultData();
    }

}
