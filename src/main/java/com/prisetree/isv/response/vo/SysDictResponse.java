package com.prisetree.isv.response.vo;

import java.util.List;

import com.prisetree.isv.domain.SysDict;

/**
 * 字典列表
 * 
 * @author y
 */
public class SysDictResponse {

	private List<SysDict> dictList;

	public List<SysDict> getDictList() {
		return dictList;
	}

	public void setDictList(List<SysDict> dictList) {
		this.dictList = dictList;
	}


}
