package com.prisetree.isv.response.vo;

import java.util.List;

import com.prisetree.isv.domain.Orders;

/**
 * @author y
 */
public class YuyueListResponse {

	private List<Orders> list;

	public List<Orders> getList() {
		return list;
	}

	public void setList(List<Orders> list) {
		this.list = list;
	}

}
