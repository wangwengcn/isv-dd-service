package com.prisetree.isv.response.vo;

/**
 * @author y
 */
public class OrderCountInfoResponse {

	private String yuyueNum; // 预约数
	private String doingNum; // 审核中数
	private String successNum; // 审核通过数
	private String failNum; // 审核失败数
	private String qa; // qa

	public String getYuyueNum() {
		return yuyueNum;
	}

	public void setYuyueNum(String yuyueNum) {
		this.yuyueNum = yuyueNum;
	}

	public String getDoingNum() {
		return doingNum;
	}

	public void setDoingNum(String doingNum) {
		this.doingNum = doingNum;
	}

	public String getSuccessNum() {
		return successNum;
	}

	public void setSuccessNum(String successNum) {
		this.successNum = successNum;
	}

	public String getFailNum() {
		return failNum;
	}

	public void setFailNum(String failNum) {
		this.failNum = failNum;
	}

	public String getQa() {
		return qa;
	}

	public void setQa(String qa) {
		this.qa = qa;
	}

}
