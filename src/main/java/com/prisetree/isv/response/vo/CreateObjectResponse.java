package com.prisetree.isv.response.vo;

/**
 * 新增项目接口返回参数
 * @author y
 */
public class CreateObjectResponse {
	
	private String id; //项目ID

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
