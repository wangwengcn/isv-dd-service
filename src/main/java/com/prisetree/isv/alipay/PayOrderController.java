package com.prisetree.isv.alipay;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.domain.Feedback;
import com.prisetree.isv.request.parameter.CreateFeedbackRequest;
import com.prisetree.isv.response.vo.CreateObjectResponse;
import com.prisetree.isv.response.vo.ResponseVOFactory;
import com.prisetree.isv.response.vo.ResultData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: WangWenchao
 * Date: 2018/2/27 9:38
 * Description:
 */
@Controller
@RequestMapping("alipay/order")
public class PayOrderController {

    @RequestMapping(value = { "info" }, method = RequestMethod.GET)
    @ResponseBody
    public ResultData info(String orderId){
        ResultData resultData = ResultData.getInstance();

        return resultData;
    }
}
