package com.prisetree.isv.request.parameter;

/**
 * 新增用户接口请求参数
 * 
 * @author Wenchao
 *
 */
public class CreateUserRequest extends BaseRequestParameter {

	private String mobile;
	private String password;
	private String realName;
	private String userType;
	private String sex;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

}
