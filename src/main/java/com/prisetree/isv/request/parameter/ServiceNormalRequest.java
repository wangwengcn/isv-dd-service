package com.prisetree.isv.request.parameter;

public class ServiceNormalRequest extends BaseRequestParameter {

	private String serviceId;
	private String isNormal;
	
	public String getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getIsNormal() {
		return isNormal;
	}
	
	public void setIsNormal(String isNormal) {
		this.isNormal = isNormal;
	}

}
