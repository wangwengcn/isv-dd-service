package com.prisetree.isv.request.parameter;

public class HeaderData {

	// 用户id
	private String userId;
	// 登录后取得的令牌
	private String token;
	// 客户端版本号-clientVersion
	private String cVersion;
	// 接口版本号-interfaceVersion
	private String iVersion;
	// 请求签名，对每个请求做安全校验，暂时不用，留待后期扩展
	private String sign;

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getcVersion() {
		return cVersion;
	}

	public void setcVersion(String cVersion) {
		this.cVersion = cVersion;
	}

	public String getiVersion() {
		return iVersion;
	}

	public void setiVersion(String iVersion) {
		this.iVersion = iVersion;
	}
}
