package com.prisetree.isv.request.parameter;

public class CreateBookingsRequest extends BaseRequestParameter {

	private String companyId;//公司id（钉钉）
	private String companyName;//公司名称（钉钉）
	private String companyMobile;//公司手机号
    private String officeResourcesId;//房源ID
	private String officeResourcesName;//房源名字
	private String officeResourcesAddr;//房源地址
	private String officeResourcesMobile;//房源电话
	private String type;//订单类型：0租固定；1租写字楼；2预定临时；3预定看固定；4预定看写字楼
    
	public String getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	public String getCompanyMobile() {
		return companyMobile;
	}
	
	public void setCompanyMobile(String companyMobile) {
		this.companyMobile = companyMobile;
	}
	
	public String getOfficeResourcesId() {
		return officeResourcesId;
	}
	
	public void setOfficeResourcesId(String officeResourcesId) {
		this.officeResourcesId = officeResourcesId;
	}

	public String getOfficeResourcesName() {
		return officeResourcesName;
	}

	public void setOfficeResourcesName(String officeResourcesName) {
		this.officeResourcesName = officeResourcesName;
	}

	public String getOfficeResourcesAddr() {
		return officeResourcesAddr;
	}

	public void setOfficeResourcesAddr(String officeResourcesAddr) {
		this.officeResourcesAddr = officeResourcesAddr;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOfficeResourcesMobile() {
		return officeResourcesMobile;
	}

	public void setOfficeResourcesMobile(String officeResourcesMobile) {
		this.officeResourcesMobile = officeResourcesMobile;
	}

}
