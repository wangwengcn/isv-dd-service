package com.prisetree.isv.request.parameter;

public class ChangePwdRequest {
	
	private String password;
	private String pwdNew;
	private String pwdNewAgain;
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPwdNew() {
		return pwdNew;
	}
	
	public void setPwdNew(String pwdNew) {
		this.pwdNew = pwdNew;
	}
	
	public String getPwdNewAgain() {
		return pwdNewAgain;
	}
	
	public void setPwdNewAgain(String pwdNewAgain) {
		this.pwdNewAgain = pwdNewAgain;
	}
	
}
