package com.prisetree.isv.request.parameter;

import java.util.Date;

import javax.validation.constraints.Digits;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

public class CreateOrdersRequest extends BaseRequestParameter {

	@Length(max = 32, message = "长度不大于32")
	private String officeResourcesId;//房源ID

	@Length(max = 40, message = "长度不大于20")
	private String officeResourcesName;//房源名字
	
	@Length(max = 32, message = "长度不大于32")
	private String officeResourcesRentId;//房源工位ID
	
	@Length(max = 40, message = "长度不大于20")
	private String officeResourcesRentName;//房源工位名字
	
	@Length(max = 40, message = "长度不大于20")
	private String officeResourcesAddr;//房源地址
	
	private String officeResourcesMobile;//房源电话
	
	@Length(max = 36, message = "长度不大于36")
	private String companyId;//公司id（钉钉）

	@Length(max = 40, message = "长度不大于20")
	private String companyName;//公司名称（钉钉）
	
	private String companyMobile;//公司手机号
	
	@Range(max = 365, min = 1, message = "周期范围应在1-365之间")
	private Integer unit;//周期

	@Length(max = 1, message = "长度不超过1")
	private String billingMode;//计费模式 ：0：天  1：周  2：月

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date enterTime;//入驻时间
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date leaveTime;//到期时间

	@Range(max = 50, min = 1, message = "范围应在1-50之间")
	private Integer stationNum;//工位数

	@Digits(integer = 10, fraction = 2, message = "超出范围")
	private Double area;//面积
	
	@Range(max = 100, min = 1, message = "范围应在1-100之间")
	private Double usageRate;//得房率
	
	@Digits(integer = 10, fraction = 2, message = "超出范围")
	private Double unitPrice;//单价
	
	@Digits(integer = 10, fraction = 2, message = "超出范围")
	private Double totalPrice;//总价
	
	@Digits(integer = 10, fraction = 2, message = "超出范围")
	private Double deposit;//总价
	
	@Length(max = 1, message = "长度不超过1")
	private String type;//订单类型：0租固定；1租写字楼；2预定临时；3预定看固定；4预定看写字楼
	
	@Length(max = 1, message = "长度不超过1")
	private String stationType;//工位类型：开放；独立；
	
	private String token;
	
	/**
     * 钉钉userid，无业务意义
     */
    private String dingUserId;
	
	public String getDingUserId() {
		return dingUserId;
	}

	public void setDingUserId(String dingUserId) {
		this.dingUserId = dingUserId;
	}

	public String getOfficeResourcesId() {
		return officeResourcesId;
	}

	public void setOfficeResourcesId(String officeResourcesId) {
		this.officeResourcesId = officeResourcesId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getDeposit() {
		return deposit;
	}

	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public Date getEnterTime() {
		return enterTime;
	}
	
	public void setEnterTime(Date enterTime) {
		this.enterTime = enterTime;
	}
	
	public Date getLeaveTime() {
		return leaveTime;
	}
	
	public void setLeaveTime(Date leaveTime) {
		this.leaveTime = leaveTime;
	}
	
	public Integer getStationNum() {
		return stationNum;
	}
	
	public void setStationNum(Integer stationNum) {
		this.stationNum = stationNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStationType() {
		return stationType;
	}

	public void setStationType(String stationType) {
		this.stationType = stationType;
	}

	public String getCompanyMobile() {
		return companyMobile;
	}

	public void setCompanyMobile(String companyMobile) {
		this.companyMobile = companyMobile;
	}

	public String getOfficeResourcesRentId() {
		return officeResourcesRentId;
	}

	public void setOfficeResourcesRentId(String officeResourcesRentId) {
		this.officeResourcesRentId = officeResourcesRentId;
	}

	public String getOfficeResourcesAddr() {
		return officeResourcesAddr;
	}

	public void setOfficeResourcesAddr(String officeResourcesAddr) {
		this.officeResourcesAddr = officeResourcesAddr;
	}

	public String getOfficeResourcesMobile() {
		return officeResourcesMobile;
	}

	public void setOfficeResourcesMobile(String officeResourcesMobile) {
		this.officeResourcesMobile = officeResourcesMobile;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public String getBillingMode() {
		return billingMode;
	}

	public void setBillingMode(String billingMode) {
		this.billingMode = billingMode;
	}

	public String getOfficeResourcesName() {
		return officeResourcesName;
	}

	public void setOfficeResourcesName(String officeResourcesName) {
		this.officeResourcesName = officeResourcesName;
	}

	public String getOfficeResourcesRentName() {
		return officeResourcesRentName;
	}

	public void setOfficeResourcesRentName(String officeResourcesRentName) {
		this.officeResourcesRentName = officeResourcesRentName;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public Double getUsageRate() {
		return usageRate;
	}

	public void setUsageRate(Double usageRate) {
		this.usageRate = usageRate;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
