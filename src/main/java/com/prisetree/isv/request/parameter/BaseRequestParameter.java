package com.prisetree.isv.request.parameter;

/**
 * 所有接口入参必须继承本类，用于封装请求header中的参数
 * @author Wenchao
 *
 */
public class BaseRequestParameter {
	
	private static ThreadLocal<HeaderData> proxy = new ThreadLocal<HeaderData>(){
		@Override
		protected HeaderData initialValue() {
			return new HeaderData();
		}
	};
	
	// 请求签名，对每个请求做安全校验，暂时不用，留待后期扩展
	public String getSign() {
		return proxy.get().getSign();
	}
	
	// 用户id
	public String getUserId() {
		return proxy.get().getUserId();
	}
	
	// 登录后取得的令牌
	public String getToken() {
		return proxy.get().getToken();
	}
	
	// 客户端版本号-clientVersion
	public String getcVersion() {
		return proxy.get().getcVersion();
	}
	
	// 接口版本号-interfaceVersion
	public String getiVersion() {
		return proxy.get().getiVersion();
	}
	
	public static void setHeaderData(HeaderData data)
	{
		proxy.set(data);
	}
	
}
