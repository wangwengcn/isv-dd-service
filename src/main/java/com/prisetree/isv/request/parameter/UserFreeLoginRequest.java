package com.prisetree.isv.request.parameter;

/**
 * 用户面登接口
 * 
 * @author y
 *
 */
public class UserFreeLoginRequest extends BaseRequestParameter {

	private String userId;
	private String token;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
