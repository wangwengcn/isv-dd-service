package com.prisetree.isv.request.parameter;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class CreateFeedbackRequest extends BaseRequestParameter {

	private String companyName;// 公司名称（钉钉）
	private String companyMobile;// 公司手机号
	private String content;// 反馈内容

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyMobile() {
		return companyMobile;
	}

	public void setCompanyMobile(String companyMobile) {
		this.companyMobile = companyMobile;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
