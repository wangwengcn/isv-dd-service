package com.prisetree.isv.request.parameter;

public class FilterRequest {

	//private String province;//省
	private String city;//市
	private String type;//房源类型(流动工位1,固定工位0)
	private String payType;// 计费方式：0-每工位/天， 1-每工位/周， 2-每工位/月
	private Integer stationNum;//工位数
	private String priceFrom;
	private String priceTo;
//	@DateTimeFormat(pattern = "yyyy-MM-dd")
//	private Date enterTime;//入驻时间
//	@DateTimeFormat(pattern = "yyyy-MM-dd")
//	private Date leaveTime;//离开时间
	private String term;//租期
	private String price;//价格区间
	private String lon;//经度
	private String lat;//纬度
	private String stationType;// 工位类型(开放办公区1,独立办公区0,不限-1)
	private String officeType;// 场所类型(联合办公 0,园区 1,写字楼 2,不限-1)
	private String other;//其他条件
	
//	public String getProvince() {
//		return province;
//	}
//	
//	public void setProvince(String province) {
//		this.province = province;
//	}
	
	public String getCity() {
		return city;
	}
	
	public String getPriceFrom() {
		return priceFrom;
	}

	public void setPriceFrom(String priceFrom) {
		this.priceFrom = priceFrom;
	}

	public String getPriceTo() {
		return priceTo;
	}

	public void setPriceTo(String priceTo) {
		this.priceTo = priceTo;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Integer getStationNum() {
		return stationNum;
	}
	
	public void setStationNum(Integer stationNum) {
		this.stationNum = stationNum;
	}
	
//	public Date getEnterTime() {
//		return enterTime;
//	}
//	
//	public void setEnterTime(Date enterTime) {
//		this.enterTime = enterTime;
//	}
//	
//	public Date getLeaveTime() {
//		return leaveTime;
//	}
//	
//	public void setLeaveTime(Date leaveTime) {
//		this.leaveTime = leaveTime;
//	}
	
	public String getPrice() {
		return price;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}
	
	public String getLon() {
		return lon;
	}
	
	public void setLon(String lon) {
		this.lon = lon;
	}
	
	public String getLat() {
		return lat;
	}
	
	public void setLat(String lat) {
		this.lat = lat;
	}
	
	public String getStationType() {
		return stationType;
	}
	
	public void setStationType(String stationType) {
		this.stationType = stationType;
	}
	
	public String getOfficeType() {
		return officeType;
	}
	
	public void setOfficeType(String officeType) {
		this.officeType = officeType;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}
	
	
}
