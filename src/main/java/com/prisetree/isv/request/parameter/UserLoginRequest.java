package com.prisetree.isv.request.parameter;

/**
 * 新增用户接口请求参数
 * 
 * @author Wenchao
 *
 */
public class UserLoginRequest extends BaseRequestParameter {

	private String mobile;
	private String password;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
