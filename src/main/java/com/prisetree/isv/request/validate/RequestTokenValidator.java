package com.prisetree.isv.request.validate;

import org.springframework.beans.factory.annotation.Autowired;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.request.parameter.BaseRequestParameter;
import com.prisetree.isv.service.TokenGeneratorService;

/**
 * TOKEN校验器
 * @author Wenchao
 *
 */
public class RequestTokenValidator extends RequestValidator {
	
	@Autowired
	private TokenGeneratorService tokenGeneratorService;

	@Override
	boolean doValidate(BaseRequestParameter parameter) {
		String trueToken = tokenGeneratorService.generateToken(parameter);
		if(trueToken == null || !trueToken.equalsIgnoreCase(parameter.getToken()))
		{
			return false;
		}
		return true;
	}

	@Override
	SystemCode getErrorCode() {
		return SystemCode.TOKEN_ERROR;
	}

}
