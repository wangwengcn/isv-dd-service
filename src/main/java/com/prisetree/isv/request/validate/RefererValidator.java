package com.prisetree.isv.request.validate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class RefererValidator extends HandlerInterceptorAdapter {
    private final Logger logger = LoggerFactory.getLogger(RefererValidator.class);
    
    public final String DOMIAN = "https://isv.prisetree.com/";
    public final String DOMIAN_IP = "https://47.96.11.252/";
    

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	//校验referer
    	String referer = request.getHeader("referer");
    	System.out.println(referer);
        
        if(StringUtils.isBlank(referer) || (!referer.startsWith(DOMIAN) && !referer.startsWith(DOMIAN_IP))) {
        	response.getWriter().print("{\"code\": 50002,\"msg\": \"非法来源\"}");
    		return false;
        }

        return true;
    }
    
}
