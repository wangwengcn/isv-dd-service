package com.prisetree.isv.request.validate;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.request.parameter.BaseRequestParameter;

public abstract class RequestValidator {

	/**
	 * 
	 * @param parameter
	 * @return 返回0代表校验成功，否则校验失败
	 */
	abstract boolean doValidate(BaseRequestParameter parameter);
	
	abstract SystemCode getErrorCode();

}
