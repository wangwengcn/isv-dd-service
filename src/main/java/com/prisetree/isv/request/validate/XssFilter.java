package com.prisetree.isv.request.validate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class XssFilter implements Filter {
	
	FilterConfig filterConfig = null;
	//白名单
	List<String> uris = new ArrayList<String>();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("xss url :" + ((HttpServletRequest) request).getRequestURI());
		if(!uris.contains(((HttpServletRequest) request).getRequestURI()))
		{
			chain.doFilter(new XssHttpServletRequestWrapper((HttpServletRequest) request), response);
		}
		else{
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
		this.filterConfig = null;
	}

}
