package com.prisetree.isv.request.validate;

import java.util.List;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.request.parameter.BaseRequestParameter;

/**
 * 校验器链，这里作为校验入口
 * @author Wenchao
 *
 */
public class RequestValidatorChain {

	private List<RequestValidator> validators;
	
	public SystemCode doValidate(BaseRequestParameter parameter)
	{
		if(validators.size() > 0)
		{
			for(RequestValidator validator : validators)
			{
				if(!validator.doValidate(parameter))
				{
					return validator.getErrorCode();
				}
			}
		}
		return SystemCode.SUCCESS;
	}

	public List<RequestValidator> getValidators() {
		return validators;
	}

	public void setValidators(List<RequestValidator> validators) {
		this.validators = validators;
	}
}
