package com.prisetree.isv.common.constant;

public class ConstantsUtil {
    public final static String SESSION_CURRENT_USER_NAME = "SESSION_CURRENT_USER_NAME";
    public static final String SESSION_CURRENT_USER="USER";
    
    public static final String ORDER_TYPE_RENT_GUDING = "0";
    public static final String ORDER_TYPE_RENT_XIEZILOU = "1";
    public static final String ORDER_TYPE_BOOKING_LINSHI = "2";
    public static final String ORDER_TYPE_BOOKING_LOOK_GUDING = "3";
    public static final String ORDER_TYPE_BOOKING_LOOK_XIEZILOU = "4";
    
    public static final String RESOURCE_TYPE_GUDING_STR = "固定工位";
    public static final String RESOURCE_TYPE_LINSHI_STR = "临时工位";
    public static final String RESOURCE_TYPE_XIEZILOU_STR = "写字楼";
    
    public static final String ORDER_STATE_DOING = "0";
    public static final String ORDER_STATE_SUCCESS = "1";
    public static final String ORDER_STATE_FAIL = "2";
    
    public static final String ORDER_STATE_BOOKING_DOING_STR = "预约中";
    public static final String ORDER_STATE_BOOKING_SUCCESS_STR = "预约成功";
    public static final String ORDER_STATE_BOOKING_FAIL_STR = "预约失败";
    
    public static final String ORDER_STATE_ZU_DOING_STR = "预约中";
    public static final String ORDER_STATE_ZU_SUCCESS_STR = "预约成功";
    public static final String ORDER_STATE_ZU_FAIL_STR = "预约失败";
}
