package com.prisetree.isv.common.constant;

public enum SystemCode {

	SUCCESS(0, "成功"),
	TOKEN_ERROR(1001, "Token校验失败"),
	INTERFACE_AUTH_ERROR(1002, "接口认证失败"),
	LOGIN_EXPIRED(1003, "授权过期"),
	MISSING_PARAMETER(1004, "参数缺失"),
	USER_NOTEXIST(1005, "用户不存在"),
	CREATE_TOKEN_FAIL(1006, "token生成失败"),
	CREATE_USER_FAIL(1007, "添加用户失败"),
	PASWORD_ERROR(1008,"密码不正确"), 
	UPDATE_PASSWORD_FAIL(1009,"修改密码失败"),
	CREATE_BOOKING_FAIL(1010,"预约看场地失败"),
	CREATE_ORDERS_FAIL(1011,"工位预定失败"),
	COUNT_ORDERS_FAIL(1012,"工位数获取失败"),
	UPDATE_ORDERS_FAIL(1013,"更新工位订单失败"),
	DELETE_ORDERS_FAIL(1014,"删除工位订单失败"),
	OFFICERESOURCES_NOTFOUND(1015,"工位未找到"),
	LATLON_NOTFOUND(1016,"未传入经纬度"),
	DELETE_BOOKING_FAIL(1017,"删除预约订单失败"),
	PRICE_PARAM_ERROR(1018,"价格参数错误"),
	OFFICE_RESOURCES_NOTFOUND(1019, "未找到房源"),
	OFFICE_RESOURCES_RENT_NOTFOUND(1020, "未找到房源工位"),
	PRICE_ERROR(1021, "价格异常"),
	ORDER_NOTFOUND(1022, "未找到订单"),
	PARAMETER_TOOLONG(1023, "参数过长"),
	ERROR_OPERATION(1025, "无效操作"),
	TRY_TO_RELOGIN(1024, "请重新登陆"),
	ILLEGAL_TOKEN(50001, "非法token"),
	ILLEGAL_REFERER(50002, "非法来源");
	
	
	private int code;
	private String desc;
	private Object resultObject;
	
	SystemCode(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Object getResultObject() {
		return resultObject;
	}

	public void setResultObject(Object resultObject) {
		this.resultObject = resultObject;
	}
	
	
}
