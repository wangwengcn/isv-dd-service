package com.prisetree.isv.common.converter;

import java.io.IOException;
import java.io.OutputStream;

import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;

public class JsonHttpMessageConverter extends FastJsonHttpMessageConverter {
	
	@Override
	protected void writeInternal(Object obj, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
		// 过滤掉callbacks方法
		PropertyFilter profilter = new PropertyFilter(){  
			@Override
            public boolean apply(Object object, String name, Object value) {  
                if(name.equalsIgnoreCase("callbacks")){  
                    return false;  
                }  
                return true;  
            }  
        };
        OutputStream out = outputMessage.getBody();
        String text = JSON.toJSONString(JSON.toJSON(obj), profilter);
        byte[] bytes = text.getBytes(getCharset());
        out.write(bytes);
	}
}
