package com.prisetree.isv.common.controller;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.prisetree.isv.common.entity.DataTableVO;
import com.prisetree.isv.common.entity.PageableDataVO;

public class BaseController {

    public PageableDataVO setPageableData(DataTableVO dataTableVO, List list) {
        PageInfo page = new PageInfo(list);
        PageableDataVO pageResult = new PageableDataVO();
        pageResult.setRecordsTotal(page.getTotal());
        pageResult.setRecordsFiltered(page.getTotal());
        pageResult.addData(page.getList());
        pageResult.setDraw(dataTableVO.getDraw());

        return pageResult;
    }
    
}
