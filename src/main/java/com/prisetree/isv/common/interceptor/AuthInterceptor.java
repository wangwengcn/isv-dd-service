package com.prisetree.isv.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.prisetree.isv.common.constant.ConstantsUtil;
import com.prisetree.isv.domain.User;


public class AuthInterceptor extends HandlerInterceptorAdapter {
	
	private final Logger logger = Logger.getLogger(AuthInterceptor.class);

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		User user = (User) request.getSession().getAttribute(ConstantsUtil.SESSION_CURRENT_USER);
		if (user == null) {
			logger.info("Interceptor：跳转到login页面！");
			String contextPath = request.getContextPath();
			
			logger.info(request.getRequestURI());
			
			response.sendRedirect(contextPath + "/admin");
			response.setHeader("refresh", "1;url='" + request.getContextPath() + "/admin'");
			return false;
		}

		return true;
	}
}
