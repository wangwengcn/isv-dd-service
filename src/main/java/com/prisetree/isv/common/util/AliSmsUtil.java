package com.prisetree.isv.common.util;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.request.AlibabaAliqinFcTtsNumSinglecallRequest;
import com.taobao.api.request.AlibabaAliqinFcVoiceNumSinglecallRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import com.taobao.api.response.AlibabaAliqinFcTtsNumSinglecallResponse;
import com.taobao.api.response.AlibabaAliqinFcVoiceNumSinglecallResponse;

public class AliSmsUtil {
	private final static Logger logger = LoggerFactory.getLogger(AliSmsUtil.class);

    /* 公用常量参数1：Url
     * * httpFormal： 正式http环境
     * * httpsFormal：正式https环境
     * * httpTest：   测试http环境
     * * httpsTest：  测试https环境
     */
    public final static String httpFormal = "http://gw.api.taobao.com/router/rest";
    public final static String httpsFormal = "https://eco.taobao.com/router/rest";
    public final static String httpTest = "http://gw.api.tbsandbox.com/router/rest";
    public final static String httpsTest = "https://gw.api.tbsandbox.com/router/rest";

   /* 常量参数2：sms_type
    * * 短信类型，传入值请填写normal
    */

   public final static String appKey = "23517204";
   public final static String appSecret = "a1cca9cc2353dd72d0d831184c28428e";
   
    public static final String SMS_TYPE="normal";
    
    public static boolean sendSms(String url, String appKey, String appSecret,String signName,String smsParam,
            String mobile, String tplId) throws IOException {
        boolean result=false;
        TaobaoClient client = new DefaultTaobaoClient(url, appKey, appSecret);
        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setSmsType(SMS_TYPE);
        req.setSmsFreeSignName(signName);
        req.setSmsParamString(smsParam);
        req.setRecNum(mobile);
        req.setSmsTemplateCode(tplId);
        try {
            AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
            if(StringUtils.isBlank(rsp.getErrorCode())){
                result=true;
            }
        } catch (ApiException e) {
        	logger.error("发送短信异常", e);
        }
        
        return result;
    }
    
    public static boolean sendVoice(String url, String appKey, String appSecret,String mobile,
    		String calledShowNum,String voiceId){
    	boolean result=false;
    	TaobaoClient client = new DefaultTaobaoClient(url, appKey, appSecret);
    	AlibabaAliqinFcVoiceNumSinglecallRequest req = new AlibabaAliqinFcVoiceNumSinglecallRequest();

    	req.setCalledNum(mobile);
    	req.setCalledShowNum(calledShowNum);
    	req.setVoiceCode(voiceId);
    	AlibabaAliqinFcVoiceNumSinglecallResponse rsp;
		try {
			rsp = client.execute(req);
			if(StringUtils.isBlank(rsp.getErrorCode())){
				result=true;
			}
		} catch (ApiException e) {
			logger.error("发送短信异常", e);
		}
    	return result;
    }
    
    public static boolean sendVoiceText(String url, String appKey, String appSecret,String smsParam,
    		String mobile,String calledShowNum,String tplId){
    	boolean result=false;
    	TaobaoClient client = new DefaultTaobaoClient(url, appKey, appSecret);
    	AlibabaAliqinFcTtsNumSinglecallRequest req = new AlibabaAliqinFcTtsNumSinglecallRequest();
    	
    	req.setTtsParam(smsParam);
    	req.setCalledNum(mobile);
    	req.setCalledShowNum(calledShowNum);
    	req.setTtsCode(tplId);
    	AlibabaAliqinFcTtsNumSinglecallResponse rsp;
		try {
			rsp = client.execute(req);
			if(StringUtils.isBlank(rsp.getErrorCode())){
				result=true;
			}
		} catch (ApiException e) {
			logger.error("发送短信异常", e);
		}

		return result;
    }
    
    /**
     * 注册短信
     * @param mobile
     * @param code
     * @return
     */
    public static boolean sendLoginSms(String mobile, String code){
    	JSONObject shortMsgMb = new JSONObject();
    	shortMsgMb.put("code", code);
    	shortMsgMb.put("product", "企树科技");
    	try {
    		sendSms(httpFormal, appKey, appSecret, "企树网络科技 ", shortMsgMb.toString(), mobile, "SMS_25340004");
    	} catch (Exception e) {
    		logger.error("发送注册短信异常", e);
    		return false;
    	}
    	return true;
    }
    
    /**
     * 注册成功短信
     * @param mobile
     * @param code
     * @return
     */
    public static boolean sendSuccessSms(String mobile){
    	JSONObject shortMsgMb = new JSONObject();
    	shortMsgMb.put("tel", mobile);
    	shortMsgMb.put("product", "企树科技");
    	try {
    		sendSms(httpFormal, appKey, appSecret, "企树网络科技 ", shortMsgMb.toString(), "13501721648", "SMS_86650020");
    		sendSms(httpFormal, appKey, appSecret, "企树网络科技 ", shortMsgMb.toString(), "15921282357", "SMS_86650020");
    	} catch (Exception e) {
    		logger.error("发送注册成功短信异常", e);
    		return false;
    	}
    	return true;
    }

}
