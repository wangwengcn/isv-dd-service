package com.prisetree.isv.common.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 读取属性工具类.
* Class Name: PropertiesUtil
* Description: 
* @author jiajun
*
 */
public class PropertiesUtil extends PropertyPlaceholderConfigurer {

    private static Map<String, Object> ctxPropertiesMap;

    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
            throws BeansException {

        super.processProperties(beanFactory, props);
        // load properties to ctxPropertiesMap
        ctxPropertiesMap = new HashMap<String, Object>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String value = props.getProperty(keyStr);
            ctxPropertiesMap.put(keyStr, value);
        }
    }

    /**
     * static method for accessing context properties.
    * Description: 
    *
    * @param name Property Name
    * @return Object
     */
    public static Object getContextProperty(String name) {
        return ctxPropertiesMap.get(name);
    }
    
    /**
     * static method for accessing context properties and return with String.
    * Description: 
    *
    * @param name Property Name
    * @return String
     */
    public static String getContexrtParam(String name) {
        return (String) ctxPropertiesMap.get(name);
    }
}
