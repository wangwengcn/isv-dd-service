package com.prisetree.isv.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5工具类.
 * 
 * @author Jiajun,Ding
 * @date 2016年1月29日 下午2:13:21
 * @version V1.0
 */
public class MD5Util {

    // 全局数组
    private static final String[] strDigits =
            {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    public MD5Util() {}

    // 返回形式为数字跟字符串
    private static String byteToArrayString(byte byteValue) {
        int intRet = byteValue;
        if (intRet < 0) {
            intRet += 256;
        }
        int intD1 = intRet / 16;
        int intD2 = intRet % 16;
        return strDigits[intD1] + strDigits[intD2];
    }

    // 返回形式只为数字
    private static String byteToNum(byte byteValue) {
        int intRet = byteValue;
        if (intRet < 0) {
            intRet += 256;
        }
        return String.valueOf(intRet);
    }

    // 转换字节数组为16进制字串
    private static String byteToString(byte[] byteValue) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < byteValue.length; i++) {
            buffer.append(byteToArrayString(byteValue[i]));
        }
        return buffer.toString();
    }

    // 获取MD5编码
    public static String GetMD5Code(String strObj) {
        String resultString = null;
        try {
            resultString = new String(strObj);
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return resultString;
    }

    public static void main(String[] args) {
        MD5Util getMD5 = new MD5Util();
        System.out.println(getMD5.GetMD5Code("13735456098"));
    }


}
