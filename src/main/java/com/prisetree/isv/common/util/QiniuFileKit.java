package com.prisetree.isv.common.util;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.qiniu.util.StringMap;

import net.sf.json.JSONObject;

public class QiniuFileKit {
	
	private static final Logger logger = Logger.getLogger(QiniuFileKit.class);
	private static final String ACCESS_KEY = "q5hbz30CarvyNzcupyYjO8RuhjwdAFBjV2bxOVme";
	private static final String SECRET_KEY = "jIjHHFkQY1JScvDJrbp6qcC7VB0CXLCghdB0cM4e";

	public static final String HOME_URL = "http://oxndq0rq4.bkt.clouddn.com/";// 七牛上传域名
	public static final String BUCKET_NAME = "prisetree-isv-dd";// 七牛存储空间名

	// 密钥配置
	static Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
	//构造一个带指定Zone对象的配置类
	static Configuration cfg = new Configuration(Zone.zone0());
	// 创建上传对象
	static UploadManager uploadManager = new UploadManager(cfg);

	static BucketManager bucketManager = new BucketManager(auth, cfg);

	// 简单上传，使用默认策略，只需要设置上传的空间名就可以了
	public static String getUpToken() {
		return auth.uploadToken(BUCKET_NAME);
	}
	
	// 覆盖上传,key上传到七牛后保存的文件名
	public static String getUpToken(String key) {
		// <bucket>:<key>，表示只允许用户上传指定key的文件。在这种格式下文件默认允许“修改”，已存在同名资源则会被本次覆盖。
		// 如果希望只能上传指定key的文件，并且不允许修改，那么可以将下面的 insertOnly 属性值设为 1。
		// 第三个参数是token的过期时间
		return auth.uploadToken(BUCKET_NAME, key, 3600,
				new StringMap().put("insertOnly", 1));
	}
	
	/**
	 * 上传文件至七牛云储存
	 * 
	 * @param UploadFile
	 *            file
	 * @return String filePath
	 */
	public static JSONObject uploadFileToQiniu(File file, String key) {
		JSONObject obj = new JSONObject();
		try {
			// 调用put方法上传
			Response res = uploadManager.put(file, key, getUpToken());
			if( res.statusCode == 1){
				obj.put("error", 0);
				obj.put("url", HOME_URL + key);
			}
			return obj;
		} catch (QiniuException e) {
			e.printStackTrace();
			logger.info("QiniuException msg ----->" + e.getMessage());
			obj.put("error", 1);
    		obj.put("message", "上传失败");
		}
		return obj;
	}
	
	/**
	 * 上传文件至七牛云储存
	 * 
	 * @param UploadFile
	 *            file
	 * @return String filePath
	 */
	public static JSONObject uploadFileToQiniu(byte[] buffer, String key) {
		JSONObject obj = new JSONObject();
		try {
			// 调用put方法上传
			Response res = uploadManager.put(buffer, key, getUpToken());
			//解析上传成功的结果
		    DefaultPutRet putRet = new Gson().fromJson(res.bodyString(), DefaultPutRet.class);
			obj.put("error", 0);
			obj.put("url", HOME_URL + putRet.key);
			obj.put("key", putRet.key);
			return obj;
		} catch (QiniuException ex) {
			 Response r = ex.response;
			    System.err.println(r.toString());
			    try {
			        logger.error(r.bodyString());
			    } catch (QiniuException ex2) {
			        //ignore
			    }
			    
			logger.info("QiniuException msg ----->" + ex.getMessage());
			obj.put("error", 1);
    		obj.put("message", "上传失败");
		}
		return obj;
	}

	/**
	 * 上传文件至七牛云储存,覆盖上传
	 * 
	 * @param UploadFile
	 *            要上传的文件
	 * @param key
	 *            在七牛上保存的文件名
	 * @return String filePath
	 */
	public static String uploadOverWrite(File file, String key)
			throws IOException {
		String str = null;
		try {
			// 调用put方法上传，这里指定的key和上传策略中的key要一致
			Response res = uploadManager.put(file, key, getUpToken(key));
			str = res.bodyString();
			// 打印返回的信息
			logger.info(res.bodyString());
		} catch (QiniuException e) {
			Response r = e.response;
			// 请求失败时打印的异常的信息
			logger.error("QiniuException --->" + r.toString());
			try {
				// 响应的文本信息
				logger.info(r.bodyString());
			} catch (QiniuException e1) {
				logger.error("QiniuException error --->", e1);
			}
		}
		return str;
	}

	/**
	 * 删除单个文件
	 * 
	 * @param key
	 *            七牛上的文件名
	 * */
	public static boolean deleteOne(String key) {
		boolean b = true;
		try {
			if (key.indexOf("/") == 0) {
				key = key.substring(1);
	        }
			logger.info(key);
			// 调用delete方法移动文件
			bucketManager.delete(BUCKET_NAME, key);
			logger.info("delete success !");
			return true;
		} catch (QiniuException e) {
			b = false;
			// 捕获异常信息
			Response r = e.response;
			logger.error("delete error ---->" + r.toString());
		}
		return b;
	}

	/**
	 * 获取私有空间 url 
	 * @param baseUrl
	 * @return
	 */
	public static String privateDownloadUrl(String baseUrl){
		return auth.privateDownloadUrl(baseUrl);
	}
	
}
