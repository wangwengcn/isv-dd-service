package com.prisetree.isv.common.util;

import java.text.SimpleDateFormat;

public class DateFormmatUtils {

	public static ThreadLocal<SimpleDateFormat> MM_dd_mm = new ThreadLocal<SimpleDateFormat>(){
        @Override
        protected SimpleDateFormat initialValue() {
            return  new SimpleDateFormat("MM-dd mm");
        }
    };
		
}
