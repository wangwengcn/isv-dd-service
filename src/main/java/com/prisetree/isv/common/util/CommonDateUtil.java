package com.prisetree.isv.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CommonDateUtil {
    private static final SimpleDateFormat monthSdf = new SimpleDateFormat("yyyy-MM");
    public static final String YYYY_MM_DD = "yyyy-MM-dd" ;
    public static final String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm" ;
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss" ;
    public static final String HH_MM = "HH:mm" ;
    public static final String SSSSSS = "ssSSSS" ;
    public static final String YYYYMMDD = "yyyyMMdd" ;
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss" ;

    public static String format(Date date,String format){
    	String result=null;
    	try {
    		SimpleDateFormat sdf = new SimpleDateFormat(format);
    		result=sdf.format(date);
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return result;
    }
    
    public static Date format(String str,String format ){
    	Date result=null;
    	try {
    		SimpleDateFormat sdf = new SimpleDateFormat(format);
    		result=sdf.parse(str);
		} catch (Exception e) {
			// TODO: handle exception
		}
    	return result;
    }
    
    /**
     * 获得一年的开始时间.
     * 
     * @author：Jiajun
     * @date
     * @param:
     * @return: String
     * @throws:
     */
    public static String getYearStartTime() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR) + "-01-01 00:00:00";
    }

    /**
     * Description: 获取一年的结束时间.
     * 
     * @author：Jiajun
     * @date 
     * @param:
     * @return: String
     * @throws:
     */
    public static String getYearEndTime() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR) + "-12-31 23:59:59";
    }

    /**
     * Description: 获取每个月的最早时间.
     * 
     * @author：Jiajun
     * @date 
     * @param:
     * @return: String
     * @throws:
     */
    public static String getMonthStartTime() {
        Calendar cal = Calendar.getInstance();
        return monthSdf.format(cal.getTime()) + "-" + cal.getActualMinimum(Calendar.DAY_OF_MONTH)
                + " 00:00:00";
    }

    /**
     * Description: 获取每个月的最后时间.
     * 
     * @author：Jiajun
     * @param:
     * @return: String
     * @throws:
     */
    public static String getMonthLastTime() {
        Calendar cal = Calendar.getInstance();
        return monthSdf.format(cal.getTime()) + "-" + cal.getActualMaximum(Calendar.DAY_OF_MONTH)
                + " 23:59:59";
    }

    /**
     * Description: 获取一周的开始时间.
     * 
     * @author：Jiajun
     * @param:
     * @return: String
     * @throws:
     */
    public static String getWeekStartTime() {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek()); // Monday
        return  CommonDateUtil.format(cal.getTime(), YYYY_MM_DD) + " 00:00:00";
    }
    
    /**
     * Description: 获取一周的开始时间.
     * 
     * @author：Jiajun
     * @param:
     * @return: String
     * @throws:
     */
    public static Date getWeekStartTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek()); // Monday
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return  cal.getTime();
    }

    /**
     * Description: 获取一周的结束时间.
     * 
     * @author：Jiajun
     * @param:
     * @return: String
     * @throws:
     */
    public static String getWeekEndTime() {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 6); // Sunday
        return CommonDateUtil.format(cal.getTime(), YYYY_MM_DD) + " 23:59:59";
    }
    
    /**
     * Description: 获取一周的结束时间.
     * 
     * @author：Jiajun
     * @param:
     * @return: String
     * @throws:
     */
    public static Date getWeekEndTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 6); // Sunday
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    /**
     * Description: 获取一天的开始时间.
     * 
     * @author：Jiajun
     * @param:
     * @return: String
     * @throws:
     */
    public static String getDayStartTime() {
        return CommonDateUtil.format(new Date(), YYYY_MM_DD) + " 00:00:00";
    }

    /**
     * Description: 获取一天的结束时间.
     * 
     * @author：Jiajun
     * @param:
     * @return: String
     * @throws:
     */
    public static String getDayEndTime() {
        return CommonDateUtil.format(new Date(), YYYY_MM_DD) + " 23:59:59";
    }

    /**
     * Description: 是否为每年的第一天.
     * 
     * @author：Jiajun
     * @param:
     * @return: boolean
     * @throws:
     */
    public static boolean isFirstDayOfYear() {
        Calendar cal = Calendar.getInstance();
        if (cal.get(Calendar.MONTH) == 0 && cal.get(Calendar.DAY_OF_MONTH) == 1) {
            return true;
        }
        return false;
    }

    /**
     * Description: 是否为每月的第一天.
     * 
     * @author：Jiajun
     * @param:
     * @return: boolean
     * @throws:
     */
    public static boolean isFirstDayOfMonth() {
        Calendar cal = Calendar.getInstance();
        if (cal.get(Calendar.DAY_OF_MONTH) == 1) {
            return true;
        }
        return false;
    }

    /**
     * Description: 是否为每周的第一天(星期一).
     * 
     * @author：Jiajun
     * @param:
     * @return: boolean
     * @throws:
     */
    public static boolean isFirstDayOfWeek() {
        Calendar cal = Calendar.getInstance();
        if (cal.get(Calendar.DAY_OF_WEEK) == 2) {
            return true;
        }
        return false;
    }

    /**
     * 当前时间转换.
     * 
     * @return 时间字符串
     */
    public static String getDateString() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String strDate = sdf.format(date);
        return strDate;
    }

    /**
     * 主函数.
     * 
     * @param args 参数
     */
    public static void main(String[] args) {
/*    	Calendar c = Calendar.getInstance();
    	c.add(Calendar.DAY_OF_YEAR, -7);
        System.out.println(getWeekStartTime(c.getTime()));
        System.out.println(getWeekEndTime(c.getTime()));*/
        Date d1=format("2016-08-31",YYYY_MM_DD);
        System.out.println("--------->"+getTimeInterval(new Date(),d1));
    }

    /**
     * 返回当前时间是上午或下午 0:上午 1:下午.
     * 
     * @return 结果值
     */
    public static int getNoonType() {
        GregorianCalendar ca = new GregorianCalendar();
        return ca.get(GregorianCalendar.AM_PM);
    }

    /**
     * 日期比较.
     * 
     * @param date1 日期
     * @param date2 日期
     * @return 结果值
     */
    public static int compareDate(Date date1, Date date2) {
        if (date1.getTime() < date2.getTime()) {
            return -1;
        } else if (date1.getTime() == date2.getTime()) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * 计算时间差,data1较大，data2较小
     * @param date1
     * @param date2
     * @return
     */
    public static String getTimeInterval(Date date1, Date date2) {
        String result="刚刚";
        long between = (date1.getTime()-date2.getTime())/1000;
//        long days=between/(24*3600);
        long hours=between/3600;
        long minutes=between/60;
        if(minutes>1){
            if(minutes<60){
                result=minutes+"分钟前";
            }else if(hours<24){
                result=hours+"小时前";
            }else{
                result= format(date2,YYYY_MM_DD);
            }
        }

        return result;
    }
    
    public static Date getLastMonthStartDate(){
    	 Calendar calendar= Calendar.getInstance();
         calendar.add(Calendar.MONTH, -1);
         calendar.set(Calendar.DAY_OF_MONTH, 1);
         calendar.set(Calendar.HOUR_OF_DAY, 0);
         calendar.set(Calendar.MINUTE, 0);
         
         return calendar.getTime();
    }
    
    /**
     * 相差月数
     * @param date1 <String>
     * @param date2 <String>
     * @return int
     * @throws ParseException
     */
    public static int getMonthSpace (Date date1, Date date2) throws ParseException{

        
    	int result=0;

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        c1.setTime(date1);
        c2.setTime(date2);

      return   result = c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH)+12*(c2.get(Calendar.YEAR)-c1.get(Calendar.YEAR));
    }
    /**  
     * 计算两个日期之间相差的天数  
     * @param date1 较小的时间 
     * @param date2  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date date1,Date date2) throws ParseException    
    {    
             
        long between_days=(date2.getTime()-date1.getTime())/(1000*3600*24);  
            
       return (int) between_days;           
    }    

}
