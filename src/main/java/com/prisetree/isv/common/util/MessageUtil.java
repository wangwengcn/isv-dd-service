package com.prisetree.isv.common.util;

import org.springframework.context.MessageSource;

import java.util.Locale;

public class MessageUtil {

    private static MessageSource messageSource;

    public void setMessageSource(MessageSource messageSource) {
        MessageUtil.messageSource = messageSource;
    }

    public static String getMessage(String key) {
        return messageSource.getMessage(key, null, "", Locale.CHINA);
    }

    public static String getMessage(String key, String[] params) {
        return messageSource.getMessage(key, params, Locale.CHINA);
    }
    
    public static String getMessage(String key, Object[] params) {
        return messageSource.getMessage(key, params, Locale.CHINA);
    }
}
