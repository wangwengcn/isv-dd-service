package com.prisetree.isv.common.util;

import java.util.Random;

public class CodeUtils {

	/**
	 * 获取任意长度数字验证码
	 * @param length
	 * @return
	 */
	public static String createValidateCode(int length) {
		char[] codeSeq = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		Random random = new Random();
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < length; i++) {
			String r = String.valueOf(codeSeq[random.nextInt(codeSeq.length)]);
			s.append(r);
		}
		return s.toString();
	}
}
