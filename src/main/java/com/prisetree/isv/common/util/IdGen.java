package com.prisetree.isv.common.util;

import java.util.UUID;

public class IdGen {
    public static final String EXCEPTION_PREFIX_APP = "APP_";

    /**
     * 生成错误ID.
     *
     * @param prefix 前缀
     * @return 错误编号
     */
    public static String getErrorId(String prefix) {
        StringBuffer sb = new StringBuffer();
        String currentDate = CommonDateUtil.format(new java.util.Date(), CommonDateUtil.SSSSSS);
        sb.append(prefix).append(currentDate);
        return sb.toString();
    }
    
	
	/**
	 * 封装JDK自带的UUID, 通过Random数字生成, 中间无-分割.
	 */
	public static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
