package com.prisetree.isv.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class BaseEntity<T>{

	protected Date createDate;	// 创建日期
	protected String delFlag; 	// 删除标记（0：正常；1：删除；2：审核）
	
	/**
	 * 实体编号（唯一标识）
	 */
	protected String id;
	
	public BaseEntity() {
		super();
		this.delFlag = DEL_FLAG_NORMAL;
	}
	
	public BaseEntity(String id) {
		this();
		this.id = id;
	}
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}
	/**
	 * 删除标记（0：正常；1：删除；）
	 */
	public static final String DEL_FLAG_NORMAL = "0";
	public static final String DEL_FLAG_DELETE = "1";
}
