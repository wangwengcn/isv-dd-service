package com.prisetree.isv.common.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class PageableDataVO<T> {

	private Long draw;
	private long recordsTotal;
	private long recordsFiltered;
	private List<T> data;

	private String error;

	public PageableDataVO() {}

	public List<T> getData() {
		return data;
	}

	public long getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public long getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(long recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Long getDraw() {
		return draw;
	}

	public void setDraw(Long draw) {
		this.draw = draw;
	}

	public void addData(T datas) {
		if (data == null) {
			data = new ArrayList<T>();
		}
		data.add(datas);
	}

	public void addData(List<T> datas) {
		if (data == null) {
			data = new ArrayList<T>();
		}
		data.addAll(datas);
	}
}
