package com.prisetree.isv.common.entity;

public class DataTableVO {

	// 响应编号
	private Long draw;
	private int start;
	private int length;

	private int pageNo;
	
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}

	public int getPageNo() {
		return this.start/this.length+1;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public Long getDraw() {
		return draw;
	}

	public void setDraw(Long draw) {
		this.draw = draw;
	}
}
