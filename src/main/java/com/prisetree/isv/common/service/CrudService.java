package com.prisetree.isv.common.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.prisetree.isv.common.dao.CrudDao;
import com.prisetree.isv.common.entity.BaseEntity;
import com.prisetree.isv.common.util.IdGen;

/**
 * Created by jiajun on 2016/7/4 0004.
 */
public abstract class CrudService<D extends CrudDao<T>, T extends BaseEntity<T>> {
    /**
     * 持久层对象
     */
    @Autowired
    protected D dao;

    public T get(T entity){
     return dao.get(entity);
    }

    public T get(String id){
        return dao.get(id);
    }

    public List<T> findList(T entity){
        return dao.findList(entity);
    }

    public List<T> findAllList(T entity){
        return dao.findAllList(entity);
    }

    public int update(T entity){
        return dao.update(entity);
    }

    public int updateSelective(T entity){
        return dao.updateSelective(entity);
    }

    public void delete(T entity){
        dao.delete(entity);
    }

    public int insert(T entity){
        entity.setCreateDate(new Date());
        entity.setId(IdGen.uuid());
        return  dao.insert(entity);
    }
}
