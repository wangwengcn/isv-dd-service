package com.prisetree.isv.controller.admin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.common.entity.DataTableVO;
import com.prisetree.isv.common.entity.PageableDataVO;
import com.prisetree.isv.domain.User;
import com.prisetree.isv.service.IsvUserService;

@Controller
@RequestMapping(value = "a/user")
public class SysUserController extends BaseController {

	final String ADD_PROJECT = "0";
	final String REMOVE_PROJECT = "1";

    @Autowired
    private IsvUserService userService;

    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index() {
        return "user/user-list";
    }

    @RequestMapping(value = { "add" }, method = RequestMethod.GET)
    public String add(Model model) {
    	model.addAttribute("entity", new User());
        return "user/user-add";
    }

    @RequestMapping(value = { "edit" }, method = RequestMethod.GET)
    public String edit(String id, Model model) {
        if (id != null) {
        	User entity = userService.get(id);
            model.addAttribute("entity", entity);
        }
        return "user/user-edit";
    }

    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public PageableDataVO<User> getList(DataTableVO dataTableVO, User entity, HttpServletRequest request) {
//        String label = request.getParameter("label");
//        String type = request.getParameter("type");
//        String remark = request.getParameter("remark");
//        if (StringUtils.isNotBlank(label)) {
//            dict.setLabel(label);
//        }
//        if (StringUtils.isNotBlank(type)) {
//            dict.setType(type);
//        }
//        if (StringUtils.isNotBlank(remark)) {
//            dict.setRemark(remark);
//        }

        PageHelper.startPage(dataTableVO.getPageNo(), dataTableVO.getLength());
        List<User> list = userService.findList(entity);
        return this.setPageableData(dataTableVO, list);
    }

    @RequestMapping(value = { "save" }, method = RequestMethod.POST)
    @ResponseBody
    public void save(User entity) {
        if (entity.getId() == null) {
            entity.setCreateDate(new Date());
            userService.insert(entity);
        } else {
        	userService.updateSelective(entity);
        }
    }

    @RequestMapping(value = { "delete" }, method = RequestMethod.POST)
    @ResponseBody
    public void delete(String id) {
        if (id != null) {
        	User entity= new User();
        	entity.setId(id);
			userService.delete(entity);
        }
    }

    @RequestMapping(value = { "delete/batch" }, method = RequestMethod.POST)
    @ResponseBody
    public void deletes(String ids) {
        if (StringUtils.isNotBlank(ids)) {
            String[] idArry = ids.split(",");
            for (String id : idArry) {
            	User entity= new User();
				entity.setId(id);
				userService.delete(entity);
            }
        }
    }
}
