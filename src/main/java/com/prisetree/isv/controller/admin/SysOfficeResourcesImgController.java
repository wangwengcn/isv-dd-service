package com.prisetree.isv.controller.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.dingtalk.isv.access.biz.prisetree.dao.OfficeResourcesImgMapper;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.common.util.IdGen;
import com.prisetree.isv.common.util.QiniuFileKit;
import com.prisetree.isv.domain.OfficeResourcesImg;
import com.prisetree.isv.domain.query.OfficeResourcesImgExample;

@Controller
@RequestMapping(value = "a/sysOfficeResources/img")
public class SysOfficeResourcesImgController extends BaseController {

	@Autowired
    private OfficeResourcesImgMapper officeResourcesImgMapper;
	
	@RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index(String id, Model model) {
		model.addAttribute("entity", new OfficeResourcesImg());
		OfficeResourcesImgExample example = new OfficeResourcesImgExample();
		example.createCriteria().andOfficeResourcesIdEqualTo(id);
		List<OfficeResourcesImg> datas = officeResourcesImgMapper.selectByExample(example);
		model.addAttribute("datas", datas);
        return "officeResourcesImg/sysOfficeResourcesImg-list";
    }

    @RequestMapping(value = { "save" }, method = RequestMethod.POST)
    public String save(OfficeResourcesImg entity, MultipartFile imgFile, HttpServletRequest request) {
    	if(StringUtils.isNotBlank(entity.getImgUrl())) {
    		
    		OfficeResourcesImgExample example = new OfficeResourcesImgExample();
    		example.setOrderByClause("order_num desc");
    		example.createCriteria().andOfficeResourcesIdEqualTo(entity.getOfficeResourcesId());
    		List<OfficeResourcesImg> tempList = officeResourcesImgMapper.selectByExample(example);
    		Integer orderNum = 10;
    		if(CollectionUtils.isNotEmpty(tempList)) {
    			orderNum = (tempList.get(0).getOrderNum() / 10 + 1) * 10;
    		}
    		
    		List<String> urlList = Arrays.asList(entity.getImgUrl().split(","));
    		List<OfficeResourcesImg> imgList = new ArrayList<OfficeResourcesImg>();
    		for(String item : urlList) {
    			OfficeResourcesImg img = new OfficeResourcesImg();
    			img.setId(IdGen.uuid());
    			img.setOfficeResourcesId(entity.getOfficeResourcesId());
    			img.setOrderNum(orderNum);
    			img.setCreateDate(new Date());
    			img.setImgUrl(item);
    			imgList.add(img);
    			orderNum += 10;
    		}
    		officeResourcesImgMapper.insertBatch(imgList);
    	}
        return "redirect:/a/sysOfficeResources/img?id=" + entity.getOfficeResourcesId();
    }

    @RequestMapping(value = { "delete" }, method = RequestMethod.POST)
    public String delete(String id) {
    	OfficeResourcesImg entity = null;
        if (id != null) {
        	entity = officeResourcesImgMapper.selectByPrimaryKey(id);
        	QiniuFileKit.deleteOne(entity.getImgUrl());
        	officeResourcesImgMapper.deleteByPrimaryKey(id);
        }
        return "redirect:/a/sysOfficeResources/img?id=" + entity.getOfficeResourcesId();
    }
}
