package com.prisetree.isv.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SysIndexController {

	@RequestMapping("index")
	public String index(){
		return "index";
	}
	
	@RequestMapping("a/welcome")
	public String welcome(){
		return "welcome";
	}
	
	
}
