package com.prisetree.isv.controller.admin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dingtalk.isv.access.biz.prisetree.dao.OrderRefuseReasonsMapper;
import com.dingtalk.isv.access.biz.prisetree.dao.OrdersMapper;
import com.github.pagehelper.PageHelper;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.common.entity.DataTableVO;
import com.prisetree.isv.common.entity.PageableDataVO;
import com.prisetree.isv.common.util.IdGen;
import com.prisetree.isv.domain.OrderRefuseReasons;
import com.prisetree.isv.domain.Orders;
import com.prisetree.isv.domain.query.OrderRefuseReasonsExample;
import com.prisetree.isv.domain.query.OrdersExample;

@Controller
@RequestMapping(value = "a/sysOrders")
public class SysOrdersController extends BaseController {

	@Autowired
    private OrdersMapper ordersMapper;
	@Autowired
	private OrderRefuseReasonsMapper orderRefuseReasonsMapper;
	
	@RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index() {
        return "orders/sysOrders-list";
    }
	
	@RequestMapping(value = { "audit" }, method = RequestMethod.GET)
    public String auditPage(String id, Model model) {
        if (id != null) {
        	Orders entity = ordersMapper.selectByPrimaryKey(id);
            model.addAttribute("entity", entity);
            OrderRefuseReasons res = new OrderRefuseReasons();
            res.setOrderId(entity.getId());
            model.addAttribute("res", res);
        }
        return "orders/sysOrders-audit";
    }
	
	@RequestMapping(value = { "auditinfo" }, method = RequestMethod.GET)
    public String auditinfoPage(String id, Model model) {
        if (id != null) {
            OrderRefuseReasonsExample res = new OrderRefuseReasonsExample();
            res.createCriteria().andOrderIdEqualTo(id);
            List<OrderRefuseReasons> reasons = orderRefuseReasonsMapper.selectByExample(res);
            if(reasons != null && reasons.size() > 0)
            {
            	model.addAttribute("res", reasons.get(0));
            }
        }
        return "orders/sysOrders-audit-info";
    }
	
	@RequestMapping(value = { "audit" }, method = RequestMethod.POST)
	@ResponseBody
    public String auditResult(String id, String result, String reason) {
        if (id != null) {
        	Orders entity = ordersMapper.selectByPrimaryKey(id);
        	// 拒绝
        	if("0".equals(result))
        	{
        		entity.setState("2");
        		OrderRefuseReasons reasons = new OrderRefuseReasons();
        		reasons.setCreateDate(new Date());
        		reasons.setId(IdGen.uuid());
        		reasons.setOrderId(id);
        		reasons.setReason(reason);
        		orderRefuseReasonsMapper.insert(reasons);
        	}
        	else
        	{
        		entity.setState("1");
        	}
        	ordersMapper.updateByPrimaryKey(entity);
        }
        return "success";
    }

    @SuppressWarnings("unchecked")
	@RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public PageableDataVO<Orders> getList(DataTableVO dataTableVO, Orders entity, HttpServletRequest request) {
    	OrdersExample example = new OrdersExample();
        if (StringUtils.isNotBlank(entity.getCompanyName())) {
        	example.createCriteria().andCompanyNameLike(entity.getCompanyName());
        }
        PageHelper.startPage(dataTableVO.getPageNo(), dataTableVO.getLength());
        List<Orders> list = ordersMapper.selectByExample(example);
        return this.setPageableData(dataTableVO, list);
    }
}
