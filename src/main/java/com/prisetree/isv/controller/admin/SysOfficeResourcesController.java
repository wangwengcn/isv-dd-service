package com.prisetree.isv.controller.admin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dingtalk.isv.access.biz.prisetree.dao.OfficeResourcesImgMapper;
import com.dingtalk.isv.access.biz.prisetree.dao.OfficeResourcesMapper;
import com.github.pagehelper.PageHelper;
import com.prisetree.isv.common.constant.ConstantsUtil;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.common.entity.DataTableVO;
import com.prisetree.isv.common.entity.PageableDataVO;
import com.prisetree.isv.common.util.DictUtils;
import com.prisetree.isv.common.util.IdGen;
import com.prisetree.isv.common.util.QiniuFileKit;
import com.prisetree.isv.domain.OfficeResources;
import com.prisetree.isv.domain.OfficeResourcesImg;
import com.prisetree.isv.domain.SysDict;
import com.prisetree.isv.domain.User;
import com.prisetree.isv.domain.query.OfficeResourcesExample;
import com.prisetree.isv.domain.query.OfficeResourcesImgExample;

@Controller
@RequestMapping(value = "a/sysOfficeResources")
public class SysOfficeResourcesController extends BaseController {

	@Autowired
    private OfficeResourcesMapper officeResourcesMapper;
	@Autowired
    private OfficeResourcesImgMapper officeResourcesImgMapper;
	
	@RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index() {
        return "officeResources/sysOfficeResources-list";
    }
	
	@RequestMapping(value = { "add" }, method = RequestMethod.GET)
    public String add(Model model) {
		model.addAttribute("entity", new OfficeResources());
		List<SysDict> tagsList = DictUtils.getDictList("office_tag");
    	String[] tags = new String[tagsList.size()];
    	for(int i=0; i<tagsList.size(); i++)
    	{
    		tags[i] = tagsList.get(i).getLabel();
    	}
    	model.addAttribute("tagsList", tagsList);
        return "officeResources/sysOfficeResources-add";
    }
	
	@RequestMapping(value = { "map" }, method = RequestMethod.GET)
    public String map(String city,  Model model) {
		model.addAttribute("city", city);
        return "officeResources/sysOfficeResources-map";
    }
	
	@RequestMapping(value = { "edit" }, method = RequestMethod.GET)
    public String edit(String id, Model model) {
        if (id != null) {
        	OfficeResources dict = officeResourcesMapper.selectByPrimaryKey(id);
        	String otherTags = dict.getOtherTags();
        	if(org.apache.commons.lang3.StringUtils.isNotEmpty(otherTags))
        	{
        		String[] tags = otherTags.split(",");
        		dict.setTagsvo(tags);
        	}
        	
        	List<SysDict> tagsList = DictUtils.getDictList("office_tag");
        	String[] tags = new String[tagsList.size()];
        	for(int i=0; i<tagsList.size(); i++)
        	{
        		tags[i] = tagsList.get(i).getLabel();
        	}
        	model.addAttribute("tagsList", tagsList);
            model.addAttribute("entity", dict);
        }
        return "officeResources/sysOfficeResources-edit";
    }

    @SuppressWarnings("unchecked")
	@RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public PageableDataVO<OfficeResources> getList(DataTableVO dataTableVO, OfficeResources resources, HttpServletRequest request) {
    	OfficeResourcesExample example = new OfficeResourcesExample();
        if (StringUtils.isNotBlank(resources.getName())) {
        	example.createCriteria().andNameLike(resources.getName());
        }
        if (StringUtils.isNotBlank(resources.getProvince())) {
        	example.createCriteria().andProvinceEqualTo(resources.getProvince());
        }
        if (StringUtils.isNotBlank(resources.getCity())) {
        	example.createCriteria().andCityEqualTo(resources.getCity());
        }
        
        PageHelper.startPage(dataTableVO.getPageNo(), dataTableVO.getLength());
        List<OfficeResources> list = officeResourcesMapper.selectByExample(example);
        return this.setPageableData(dataTableVO, list);
    }

    @RequestMapping(value = { "save" }, method = RequestMethod.POST)
    @ResponseBody
    public void save(OfficeResources entity, HttpServletRequest request) {
    	String updateBy = ((User)request.getSession().getAttribute(ConstantsUtil.SESSION_CURRENT_USER)).getRealName();
    	entity.setUpdateDate(new Date());
    	entity.setUpdateBy(updateBy);
    	String otherTags = "";
    	String[] tagsvo = entity.getTagsvo();
    	if(tagsvo != null && tagsvo.length > 0)
    	{
    		for(String dict : tagsvo)
    		{
    			otherTags += dict + ",";
    		}
    	}
    	entity.setOtherTags(otherTags);
        if (org.apache.commons.lang3.StringUtils.isEmpty(entity.getId())) {
            entity.setCreateDate(new Date());
            entity.setId(IdGen.uuid());
            officeResourcesMapper.insert(entity);
        } else {
        	officeResourcesMapper.updateByPrimaryKey(entity);
        }
    }

    @RequestMapping(value = { "delete" }, method = RequestMethod.POST)
    @ResponseBody
    public void delete(String id) {
        if (id != null) {
			officeResourcesMapper.deleteByPrimaryKey(id);
			
			// 删除关联的图片
			OfficeResourcesImgExample example = new OfficeResourcesImgExample();
			example.createCriteria().andOfficeResourcesIdEqualTo(id);
			List<OfficeResourcesImg> imgs = officeResourcesImgMapper.selectByExample(example);
			for(OfficeResourcesImg img : imgs)
			{
				QiniuFileKit.deleteOne(img.getImgUrl());
	        	officeResourcesImgMapper.deleteByPrimaryKey(img.getId());
			}
        }
    }
}
