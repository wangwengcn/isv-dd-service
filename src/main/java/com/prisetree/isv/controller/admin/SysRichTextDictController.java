package com.prisetree.isv.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dingtalk.isv.access.biz.prisetree.dao.SysRichtextDictMapper;
import com.github.pagehelper.PageHelper;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.common.entity.DataTableVO;
import com.prisetree.isv.common.entity.PageableDataVO;
import com.prisetree.isv.domain.SysRichtextDict;
import com.prisetree.isv.domain.query.SysRichtextDictExample;
import com.prisetree.isv.domain.query.SysRichtextDictExample.Criteria;


@Controller
@RequestMapping(value = "a/sysRichTextDict")
public class SysRichTextDictController extends BaseController {

    @Autowired
    private SysRichtextDictMapper richtextDictMapper;

    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index() {
        return "sysRichTextDict/sysRichTextDict-list";
    }

    @RequestMapping(value = { "add" }, method = RequestMethod.GET)
    public String add() {
        return "sysRichTextDict/sysRichTextDict-add";
    }

    @RequestMapping(value = { "edit" }, method = RequestMethod.GET)
    public String edit(Integer id, Model model) {
        if (id != null) {
        	SysRichtextDict dict = richtextDictMapper.selectByPrimaryKey(id);
            model.addAttribute("entity", dict);
        }
        return "sysRichTextDict/sysRichTextDict-edit";
    }

    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public PageableDataVO<SysRichtextDict> getList(DataTableVO dataTableVO, HttpServletRequest request) {
    	SysRichtextDictExample dict = new SysRichtextDictExample();
        String label = request.getParameter("label");
        String type = request.getParameter("type");
        Criteria criteria = dict.createCriteria();
        if (StringUtils.isNotBlank(label)) {
        	criteria.andLabelLike(label);
        }
        if (StringUtils.isNotBlank(type)) {
        	criteria.andTypeLike(type);
        }
        PageHelper.startPage(dataTableVO.getPageNo(), dataTableVO.getLength());
        List<SysRichtextDict> list = richtextDictMapper.selectByExample(dict);
        return this.setPageableData(dataTableVO, list);
    }

    @RequestMapping(value = { "save" }, method = RequestMethod.POST)
    @ResponseBody
    public void save(SysRichtextDict entity) {
        if (entity.getId() == null) {
        	richtextDictMapper.insert(entity);
        } else {
        	richtextDictMapper.updateByPrimaryKeySelective(entity);
        }
    }

    @RequestMapping(value = { "delete" }, method = RequestMethod.POST)
    @ResponseBody
    public void delete(Integer id) {
        if (id != null) {
			richtextDictMapper.deleteByPrimaryKey(id);
        }
    }
}
