package com.prisetree.isv.controller.admin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.common.entity.DataTableVO;
import com.prisetree.isv.common.entity.PageableDataVO;
import com.prisetree.isv.domain.SysDict;
import com.prisetree.isv.service.SysDictService;


@Controller
@RequestMapping(value = "a/sysDict")
public class SysDictController extends BaseController {

    @Autowired
    private SysDictService dictService;

    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index() {
        return "sys/sysDict-list";
    }

    @RequestMapping(value = { "add" }, method = RequestMethod.GET)
    public String add() {
        return "sys/sysDict-add";
    }

    @RequestMapping(value = { "edit" }, method = RequestMethod.GET)
    public String edit(String id, Model model) {
        if (id != null) {
            SysDict dict = dictService.get(id);
            model.addAttribute("entity", dict);
        }
        return "sys/sysDict-edit";
    }

    @RequestMapping(value = { "" }, method = RequestMethod.POST)
    @ResponseBody
    public PageableDataVO<SysDict> getList(DataTableVO dataTableVO, SysDict dict, HttpServletRequest request) {
        String label = request.getParameter("label");
        String type = request.getParameter("type");
        String desc = request.getParameter("description");
        if (StringUtils.isNotBlank(label)) {
            dict.setLabel(label);
        }
        if (StringUtils.isNotBlank(type)) {
            dict.setType(type);
        }
        if (StringUtils.isNotBlank(desc)) {
            dict.setDescription(desc);
        }

        PageHelper.startPage(dataTableVO.getPageNo(), dataTableVO.getLength());
        List<SysDict> list = dictService.findList(dict);
        return this.setPageableData(dataTableVO, list);
    }

    @RequestMapping(value = { "save" }, method = RequestMethod.POST)
    @ResponseBody
    public void save(SysDict entity) {
    	entity.setUpdateDate(new Date());
        if (entity.getId() == null) {
            entity.setCreateDate(new Date());
            dictService.insert(entity);
        } else {
        	entity.setUpdateDate(new Date());
            dictService.update(entity);
        }
    }

    @RequestMapping(value = { "delete" }, method = RequestMethod.POST)
    @ResponseBody
    public void delete(String id) {
        if (id != null) {
			SysDict sysDict= new SysDict();
			sysDict.setId(id);
            dictService.delete(sysDict);
        }
    }

    @RequestMapping(value = { "delete/batch" }, method = RequestMethod.POST)
    @ResponseBody
    public void deletes(String ids) {
        if (StringUtils.isNotBlank(ids)) {
            String[] idArry = ids.split(",");
            for (String id : idArry) {
				SysDict sysDict= new SysDict();
				sysDict.setId(id);
                dictService.delete(sysDict);
            }
        }
    }
}
