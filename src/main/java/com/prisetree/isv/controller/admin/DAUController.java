package com.prisetree.isv.controller.admin;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.service.UserStatisticalService;

@Controller
@RequestMapping(value = "a/dau")
public class DAUController extends BaseController {
	
	@Autowired
	private UserStatisticalService userStatisticalService;

    @RequestMapping(value = { "week" }, method = RequestMethod.GET)
    public String index(Model model) {
    	List<Map<Object, Object>> countList = userStatisticalService.countWeek();
    	StringBuilder date = new StringBuilder();
    	StringBuilder data = new StringBuilder();
    	for(Map<Object, Object> obj : countList)
    	{
    		date.append("'").append(obj.get("date")).append("',");
			data.append(obj.get("counts")).append(",");
    	}
    	String dateStr = "";
    	String dataStr = "";
    	if(date.length() > 0)
    	{
    		dateStr = date.substring(0, date.length() - 1);
    		dataStr = data.substring(0, data.length() - 1);
    	}
//    	System.out.println(date.toString());
//    	System.out.println(data.toString());
    	
    	model.addAttribute("date", dateStr);
    	model.addAttribute("data", dataStr);
        return "statistical/dau-week";
    }
    
    @RequestMapping(value = { "month" }, method = RequestMethod.GET)
    public String month(Model model) {
    	List<Map<Object, Object>> countList = userStatisticalService.countMonth();
    	StringBuilder date = new StringBuilder();
    	StringBuilder data = new StringBuilder();
    	for(Map<Object, Object> obj : countList)
    	{
    		date.append("'").append(obj.get("date")).append("',");
			data.append(obj.get("counts")).append(",");
    	}
    	String dateStr = "";
    	String dataStr = "";
    	if(date.length() > 0)
    	{
    		dateStr = date.substring(0, date.length() - 1);
    		dataStr = data.substring(0, data.length() - 1);
    	}
//    	System.out.println(date.toString());
//    	System.out.println(data.toString());
    	
    	model.addAttribute("date", dateStr);
    	model.addAttribute("data", dataStr);
        return "statistical/dau-month";
    }
    
}
