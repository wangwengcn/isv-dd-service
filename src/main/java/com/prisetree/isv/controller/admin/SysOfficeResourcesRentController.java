package com.prisetree.isv.controller.admin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dingtalk.isv.access.biz.prisetree.dao.OfficeResourcesRentMapper;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.common.util.IdGen;
import com.prisetree.isv.domain.OfficeResourcesRent;
import com.prisetree.isv.domain.query.OfficeResourcesRentExample;

@Controller
@RequestMapping(value = "a/sysOfficeResources/rent")
public class SysOfficeResourcesRentController extends BaseController {

	@Autowired
	private OfficeResourcesRentMapper officeResourcesRentMapper;
	
	@RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index(String id, Model model) {
		model.addAttribute("entity", new OfficeResourcesRent());
		OfficeResourcesRentExample example = new OfficeResourcesRentExample();
		example.createCriteria().andParentIdEqualTo(id).andRentTypeEqualTo(0);
		List<OfficeResourcesRent> datasStation = officeResourcesRentMapper.selectByExample(example);
		OfficeResourcesRentExample example2 = new OfficeResourcesRentExample();
		example2.createCriteria().andParentIdEqualTo(id).andRentTypeEqualTo(1);
		List<OfficeResourcesRent> datasArea = officeResourcesRentMapper.selectByExample(example2);
		model.addAttribute("datasStation", datasStation);
		model.addAttribute("datasArea", datasArea);
        return "officeResourcesRent/sysOfficeResourcesRent-list";
        
    }
	
    @RequestMapping(value = { "save" }, method = RequestMethod.POST)
    public String save(OfficeResourcesRent entity, HttpServletRequest request) {
		entity.setId(IdGen.uuid());
		entity.setCreateDate(new Date());
		if (entity.getUnitPrice() == null) {
			entity.setRentType(0);
			entity.setName(entity.getName1());
		} else {
			entity.setRentType(1);
			entity.setName(entity.getName2());
			entity.setStationType(null);
			entity.setStationAttr(null);
			entity.setBillingMode(null);
		}
		officeResourcesRentMapper.insert(entity);
    	return "redirect:/a/sysOfficeResources/rent?id=" + entity.getParentId();
    }
    
    @RequestMapping(value = { "delete" }, method = RequestMethod.POST)
    @ResponseBody
    public void delete(String id) {
        if (id != null) {
        	officeResourcesRentMapper.deleteByPrimaryKey(id);
        }
    }
	
}
