package com.prisetree.isv.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dingtalk.isv.access.biz.prisetree.dao.OfficeResourcesTagMapper;
import com.prisetree.isv.common.controller.BaseController;
import com.prisetree.isv.domain.OfficeResourcesTag;
import com.prisetree.isv.domain.query.OfficeResourcesTagExample;

@Controller
@RequestMapping(value = "a/sysOfficeResources/tag")
public class SysOfficeResourcesTagController extends BaseController {

	@Autowired
    private OfficeResourcesTagMapper officeResourcesTagMapper;
	
	@RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index(String id, Model model) {
		model.addAttribute("entity", new OfficeResourcesTag());
		OfficeResourcesTagExample example = new OfficeResourcesTagExample();
		example.createCriteria().andOfficeResourcesIdEqualTo(id);
		List<OfficeResourcesTag> datas = officeResourcesTagMapper.selectByExample(example);
		model.addAttribute("datas", datas);
        return "officeResourcesTag/sysOfficeResourcesTag-list";
    }

    @RequestMapping(value = { "save" }, method = RequestMethod.POST)
    public String save(OfficeResourcesTag entity) {
        // 保存数据入库
    	try {
    		officeResourcesTagMapper.insert(entity);
		} catch (Exception e) {
		}
        return "redirect:/a/sysOfficeResources/tag?id=" + entity.getOfficeResourcesId();
    }

    @RequestMapping(value = { "delete" }, method = RequestMethod.POST)
    public String delete(Integer id) {
    	OfficeResourcesTag entity = null;
        if (id != null) {
        	entity = officeResourcesTagMapper.selectByPrimaryKey(id);
        	officeResourcesTagMapper.deleteByPrimaryKey(id);
        }
        return "redirect:/a/sysOfficeResources/tag?id=" + entity.getOfficeResourcesId();
    }
}
