package com.prisetree.isv.controller.admin;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dingtalk.isv.access.biz.prisetree.dao.InterfaceStatisticalDao;
import com.prisetree.isv.common.util.DateFormmatUtils;
import com.prisetree.isv.domain.InterfaceStatistical;

@Controller
@RequestMapping(value = "a/interface")
public class SysInterfaceStatisticalController {

	@Autowired
	private InterfaceStatisticalDao interfaceStatisticalDao;

    @RequestMapping(value = { "" }, method = RequestMethod.GET)
    public String index(Model model, String interfaceId) {
    	Map<String, Object> param = new HashMap<>();
    	Calendar now = Calendar.getInstance();
    	now.add(Calendar.MINUTE, -1);
    	param.put("endTime", now.getTime());
    	now.add(Calendar.HOUR_OF_DAY, -8);
    	param.put("startTime", now.getTime());
    	param.put("interfaceId", interfaceId);
    	List<InterfaceStatistical> countList = interfaceStatisticalDao.getList(param);
    	if(countList != null)
    	{
    		StringBuilder date = new StringBuilder();
        	StringBuilder count = new StringBuilder();
        	StringBuilder rt = new StringBuilder();
        	for(InterfaceStatistical statistical : countList)
        	{
        		date.append("'").append(DateFormmatUtils.MM_dd_mm.get().format(statistical.getDateMinute())).append("',");
        		count.append(statistical.getCount()).append(",");
        		rt.append(statistical.getRt()).append(",");
        	}
        	String dateStr = "";
        	String countStr = "";
        	String rtStr = "";
        	if(date.length() > 0)
        	{
        		dateStr = date.substring(0, date.length() - 1);
        		countStr = count.substring(0, count.length() - 1);
        		rtStr = rt.substring(0, rt.length() - 1);
        	}
        	model.addAttribute("date", dateStr);
        	model.addAttribute("countStr", countStr);
        	model.addAttribute("rtStr", rtStr);
    	}
    	model.addAttribute("interfaceId", interfaceId);
        return "statistical/interface";
    }
}
