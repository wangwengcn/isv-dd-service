package com.prisetree.isv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.request.parameter.CreateBookingsRequest;
import com.prisetree.isv.response.vo.ResultData;
import com.prisetree.isv.service.OfficeBookingsService;

@Controller
@RequestMapping("api/booking")
@CrossOrigin
public class OfficeBookingsController {
	
	@Autowired
	OfficeBookingsService officeBookingsService;
	
	@RequestMapping(value = { "add" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultData create(CreateBookingsRequest req){
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeBookingsService.insert(req);
		if (sysCode == SystemCode.SUCCESS) {
			
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "detail" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData detail(String id) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeBookingsService.detail(id);
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "delete" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultData delete(String id){
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeBookingsService.delete(id);
		if (sysCode == SystemCode.SUCCESS) {
			
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
}