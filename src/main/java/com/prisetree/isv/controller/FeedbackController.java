package com.prisetree.isv.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.domain.Feedback;
import com.prisetree.isv.request.parameter.CreateFeedbackRequest;
import com.prisetree.isv.response.vo.CreateObjectResponse;
import com.prisetree.isv.response.vo.ResponseVOFactory;
import com.prisetree.isv.response.vo.ResultData;
import com.prisetree.isv.service.FeedbackService;

@Controller
@RequestMapping("api/feedback")
@CrossOrigin
public class FeedbackController {

	@Autowired
	FeedbackService feedbackService;
	
	@RequestMapping(value = { "add" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultData create(CreateFeedbackRequest req, HttpServletRequest request){
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = feedbackService.insert(req, (String) request.getSession().getAttribute("corpId"));
		if (sysCode == SystemCode.SUCCESS) {
			CreateObjectResponse rep = ResponseVOFactory.getInstance(CreateObjectResponse.class);
			Feedback feedback = (Feedback) sysCode.getResultObject();
			rep.setId(feedback.getId());
			resultData.setData(rep);
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
}
