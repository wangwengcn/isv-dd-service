package com.prisetree.isv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.dingtalk.isv.access.api.model.suite.CorpSuiteAuthVO;
import com.dingtalk.isv.access.api.service.suite.CorpSuiteAuthService;
import com.dingtalk.isv.common.model.ServiceResult;

@Controller
@RequestMapping("api/test")
@CrossOrigin
public class TestController {

	
	 @Autowired
	 private CorpSuiteAuthService corpSuiteAuthService;
	 
	@ResponseBody
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String printHello() {
		ServiceResult<CorpSuiteAuthVO> sr = corpSuiteAuthService.saveOrUpdateCorpSuiteAuth("suiteegdlqffgf36z1lb6", "61d4bbffedca369195be61448dd7aae3");
	    System.out.println(JSON.toJSON(sr));
        return "hello world";
    }
}
