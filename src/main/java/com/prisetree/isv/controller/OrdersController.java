package com.prisetree.isv.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.common.util.AliSmsUtil;
import com.prisetree.isv.common.util.CodeUtils;
import com.prisetree.isv.domain.Orders;
import com.prisetree.isv.request.parameter.CreateOrdersRequest;
import com.prisetree.isv.response.vo.CreateObjectResponse;
import com.prisetree.isv.response.vo.OrderCountInfoResponse;
import com.prisetree.isv.response.vo.ResponseVOFactory;
import com.prisetree.isv.response.vo.ResultData;
import com.prisetree.isv.service.OfficeBookingsService;
import com.prisetree.isv.service.OrdersService;
import com.prisetree.isv.statistical.Interfaces;
import com.prisetree.isv.statistical.StatisticalService;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("api/orders")
@CrossOrigin
public class OrdersController {

	@Autowired
	OrdersService ordersService;
	@Autowired
	OfficeBookingsService officeBookingsService;
	@Resource
	private StatisticalService statisticalService;
	
	@RequestMapping(value = { "code" },method = RequestMethod.POST)
	@ResponseBody
    public ResultData code(HttpServletRequest request,String companyMobile) {
		ResultData resultData = ResultData.getInstance();
        String telCode = CodeUtils.createValidateCode(6);
        AliSmsUtil.sendLoginSms(companyMobile,telCode);//验证码
        resultData.setData(telCode);
		return resultData;
    }
	
	@RequestMapping(value = { "add" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultData create(CreateOrdersRequest req, HttpServletRequest request){
		Long begin = System.currentTimeMillis();
		ResultData resultData = ResultData.getInstance();
		String userId = (String) request.getSession().getAttribute("userId");
		req.setDingUserId(userId);
		SystemCode sysCode = ordersService.insert(req, (String) request.getSession().getAttribute("token"));
		if (sysCode == SystemCode.SUCCESS) {
			CreateObjectResponse rep = ResponseVOFactory.getInstance(CreateObjectResponse.class);
			Orders orders = (Orders) sysCode.getResultObject();
			rep.setId(orders.getId());
			resultData.setData(rep);
			request.getSession().removeAttribute("token");
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		Long end = System.currentTimeMillis();
		statisticalService.statistica(Interfaces.PLACE_ORDER, end - begin);
		return resultData;
	}
	
	@RequestMapping(value = { "delete" }, method = RequestMethod.POST)
	@ResponseBody
	public ResultData delete(String id){
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.delete(id);
		if (sysCode == SystemCode.SUCCESS) {
			
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "update" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData update(String id ,String state) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.updateState(id, state);
		if (sysCode == SystemCode.SUCCESS) {
			
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "orderList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData orderList(String type, String state, HttpServletRequest request) {
		//type:all全部,linshi临时,guding固定,xiezilou写字楼
		//state:0预约中,1预约成功,2预约失败,3预约看场地
		request.getSession().setAttribute("corpId", "ding0a4914b189d2884435c2f4657eb6378f");
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.findOrderListBytype(type, state, (String) request.getSession().getAttribute("corpId"));
		if (sysCode == SystemCode.SUCCESS) {
			JSONObject json = (JSONObject) sysCode.getResultObject();
			resultData.setData(json);
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "listCount" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData listCount(HttpServletRequest request) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.listCount((String) request.getSession().getAttribute("corpId"));
		if (sysCode == SystemCode.SUCCESS) {
			OrderCountInfoResponse rep = ResponseVOFactory.getInstance(OrderCountInfoResponse.class);
			JSONObject json = (JSONObject) sysCode.getResultObject();
			rep.setYuyueNum(json.getString("yuyueNum"));
			rep.setDoingNum(json.getString("doingNum"));
			rep.setSuccessNum(json.getString("successdNum"));
			rep.setFailNum(json.getString("failNum"));
			rep.setQa(json.getString("qa"));
			resultData.setData(rep);
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "yuyueList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData yuyueList(String type, HttpServletRequest request) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.findYuyueListBytype(type, (String) request.getSession().getAttribute("corpId"));
		if (sysCode == SystemCode.SUCCESS) {
			JSONObject json = (JSONObject) sysCode.getResultObject();
			resultData.setData(json);
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "shenheList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData shenheList(String state, HttpServletRequest request) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.findShenheListBytype(state, (String) request.getSession().getAttribute("corpId"));
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "detail" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData detail(String id) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.getDetailInfoById(id);
		if (sysCode == SystemCode.SUCCESS) {
			JSONObject json = (JSONObject) sysCode.getResultObject();
			resultData.setData(json);
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "contact" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData contact(String id) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = ordersService.contact(id);
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
}
