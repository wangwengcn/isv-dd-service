package com.prisetree.isv.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dingtalk.isv.access.api.service.corp.CorpManageService;
import com.dingtalk.isv.access.api.service.suite.SuiteManageService;
import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.request.parameter.FilterRequest;
import com.prisetree.isv.response.vo.ResultData;
import com.prisetree.isv.service.OfficeResourcesService;
import com.prisetree.isv.statistical.Interfaces;
import com.prisetree.isv.statistical.StatisticalService;

@Controller
@RequestMapping(value = "api/officeResources")
@CrossOrigin
public class OfficeResourcesController {
	
	@Autowired
	OfficeResourcesService officeResourcesService;
	@Resource
    private CorpManageService corpManageService;
	@Resource
    private SuiteManageService suiteManageService;
	@Resource
	private StatisticalService statisticalService;
	
	@RequestMapping(value = { "listByType" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData listByType(Integer pageNo, FilterRequest req) {
		Long begin = System.currentTimeMillis();
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.listByType(pageNo, req);
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		Long end = System.currentTimeMillis();
		statisticalService.statistica(Interfaces.OFFICE_RESOURCES_LIST, end - begin);
		return resultData;
	}
	
	@RequestMapping(value = { "detail" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData detail(String id, Integer stationAttr) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.detail(id, stationAttr);
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "officeTypeList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData officeTypeList() {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.officeTypeList();
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "tagList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData tagList() {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.otherFilterList();
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "cityTagList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData cityTagList() {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.cityTagList();
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "hotcityTagList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData hotcityTagList() {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.cityTagList();
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "fixedPriceTagList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData fixedPriceTagList(String officeType) {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.fixedPriceTagList(officeType);
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
	@RequestMapping(value = { "flowPriceTagList" }, method = RequestMethod.GET)
	@ResponseBody
	public ResultData flowPriceTagList() {
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = officeResourcesService.flowPriceTagList();
		if (sysCode == SystemCode.SUCCESS) {
			resultData.setData(sysCode.getResultObject());
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}

}
