package com.prisetree.isv.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.domain.Feedback;
import com.prisetree.isv.response.vo.CSRFTokenResponse;
import com.prisetree.isv.response.vo.ResponseVOFactory;
import com.prisetree.isv.response.vo.ResultData;
import com.prisetree.isv.service.CSRFTokenService;

@Controller
@RequestMapping("api/token")
@CrossOrigin
public class CSRFTokenController {

	@Autowired
	CSRFTokenService cSRFTokenService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public ResultData illegalToken(HttpServletRequest request){
		ResultData resultData = ResultData.getInstance();
		SystemCode sysCode = cSRFTokenService.createToken((String) request.getSession().getAttribute("corpId"));
		if (sysCode == SystemCode.SUCCESS) {
			CSRFTokenResponse rep = ResponseVOFactory.getInstance(CSRFTokenResponse.class);
			String token = (String) sysCode.getResultObject();
			rep.setToken(token);
			request.getSession().removeAttribute("token");
			request.getSession().setAttribute("token", token);
			resultData.setData(rep);
		} else {
			resultData.setCode(sysCode.getCode());
			resultData.setMsg(sysCode.getDesc());
		}
		return resultData;
	}
	
}
