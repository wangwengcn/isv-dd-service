package com.prisetree.isv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.response.vo.ResultData;

@Controller
@RequestMapping("api/illegal")
@CrossOrigin
public class IllegalController {

	@RequestMapping("illegalToken")
	@ResponseBody
	public ResultData illegalToken(){
		ResultData resultData = ResultData.getInstance();
		resultData.setCode(SystemCode.ILLEGAL_TOKEN.getCode());
		resultData.setMsg(SystemCode.ILLEGAL_TOKEN.getDesc());
		return resultData;
	}
	
	@RequestMapping("illegalReferer")
	@ResponseBody
	public ResultData illegalReferer(){
		ResultData resultData = ResultData.getInstance();
		resultData.setCode(SystemCode.ILLEGAL_REFERER.getCode());
		resultData.setMsg(SystemCode.ILLEGAL_REFERER.getDesc());
		return resultData;
	}
	
}
