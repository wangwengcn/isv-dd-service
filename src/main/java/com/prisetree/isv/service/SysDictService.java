package com.prisetree.isv.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dingtalk.isv.access.biz.prisetree.dao.SysDictDao;
import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.common.service.CrudService;
import com.prisetree.isv.common.util.DictUtils;
import com.prisetree.isv.domain.SysDict;

@Service
public class SysDictService extends CrudService<SysDictDao,SysDict> {

    /**
     * 查询字段类型列表
     * @return
     */
    public List<String> findTypeList(){
        return dao.findTypeList(new SysDict());
    }
    
    /**
     * 查询字段类型列表
     * @return
     */
    public String getLabelByTypeValue(String type, String value){
        if(value.equals("0"))
    	{
        	return "男";
    	}
        else{
        	return "女";
        }
    }

	public SystemCode findProcessTypeList() {
		List<SysDict> list = DictUtils.getDictList("process_type");
		SystemCode.SUCCESS.setResultObject(list);
		return SystemCode.SUCCESS;
	}
}
