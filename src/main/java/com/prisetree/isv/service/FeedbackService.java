package com.prisetree.isv.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dingtalk.isv.access.biz.prisetree.dao.FeedbackMapper;
import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.common.util.IdGen;
import com.prisetree.isv.domain.Feedback;
import com.prisetree.isv.request.parameter.CreateFeedbackRequest;

@Service
@Transactional(readOnly = true)
public class FeedbackService {
	
	@Autowired
	FeedbackMapper feedbackMapper;
	
	public SystemCode insert(CreateFeedbackRequest req, String corpId){
		if(StringUtils.isBlank(corpId)) {
			return SystemCode.TRY_TO_RELOGIN;
		}
		
		Feedback feedback = new Feedback();
		feedback.setId(IdGen.uuid());
		feedback.setCompanyId(corpId);
		feedback.setCompanyName(req.getCompanyName());
		feedback.setCompanyMobile(req.getCompanyMobile());
		feedback.setContent(req.getContent());
		feedback.setCreateDate(new Date());
        int res = feedbackMapper.insert(feedback);
        if (res != 1) {
        	return SystemCode.CREATE_BOOKING_FAIL;
        }
        SystemCode.SUCCESS.setResultObject(feedback);
        return SystemCode.SUCCESS;
	}


}