package com.prisetree.isv.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dingtalk.isv.access.biz.prisetree.dao.SysUserDao;
import com.prisetree.isv.common.interceptor.ParamInterceptor;
import com.prisetree.isv.domain.User;
import com.prisetree.isv.request.parameter.BaseRequestParameter;

@Service
public class TokenGeneratorService {
	
	private final Logger logger = Logger.getLogger(ParamInterceptor.class);

	@Autowired
	SysUserDao userDao;
	
	public String generateToken(BaseRequestParameter parameter)
	{
		User user = userDao.get(parameter.getUserId());
		if(user != null){
			logger.info("用户：" + user.getRealName() + ", 当前登陆token：" + parameter.getToken());
			return user.getToken();
		}
		return null;
	}
}
