package com.prisetree.isv.service;

import com.prisetree.isv.request.parameter.CreateUserRequest;


/**
 * 用户相关接口，请将所有业务写到service层，对接controller和dao
 * @author Wenchao
 *
 */
public interface UserService {

	void addUser(CreateUserRequest request);
}
