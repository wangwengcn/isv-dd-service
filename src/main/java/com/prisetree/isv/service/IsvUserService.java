package com.prisetree.isv.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dingtalk.isv.access.biz.prisetree.dao.SysUserDao;
import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.common.service.CrudService;
import com.prisetree.isv.common.util.IdGen;
import com.prisetree.isv.common.util.MD5Util;
import com.prisetree.isv.domain.User;
import com.prisetree.isv.request.parameter.BaseRequestParameter;
import com.prisetree.isv.request.parameter.CreateUserRequest;
import com.prisetree.isv.request.parameter.UserFreeLoginRequest;
import com.prisetree.isv.request.parameter.UserLoginRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@Transactional(readOnly = true)
public class IsvUserService extends CrudService<SysUserDao, User> {

	public SystemCode insert(CreateUserRequest req){
		User user = new User();
		user.setId(IdGen.uuid());
		user.setMobile(req.getMobile());
		user.setPassword(req.getPassword());
		user.setRealName(req.getRealName());
		user.setUserType(req.getUserType());
		user.setSex(req.getSex());
		user.setCreateDate(new Date());
		
		dao.insert(user);
		if(StringUtils.isBlank(user.getId())){
			return SystemCode.CREATE_USER_FAIL;
		}
		SystemCode.SUCCESS.setResultObject(user);
		return SystemCode.SUCCESS;
	}
	
	public SystemCode findList() {
		User entity = new User();
		JSONArray arr = new JSONArray();
		List<User> list = dao.findList(entity);
		if (list != null && list.size() > 0) {
			for (User user : list) {
				JSONObject jsub = new JSONObject();
				jsub.put("id", user.getId());
				jsub.put("name", user.getRealName());
				arr.add(jsub);
			}
		}
		JSONObject json = new JSONObject();
		json.put("leaderList", arr);
		SystemCode.SUCCESS.setResultObject(json);
		return SystemCode.SUCCESS;
	}

	public SystemCode login(UserLoginRequest req) {
		String mobile = req.getMobile();
		String password = req.getPassword();
		String token = null;
		
		User user = dao.getByMobile(mobile);
		
		if(user == null){
			return SystemCode.USER_NOTEXIST;
		}
		
		if(!StringUtils.equals(password, user.getPassword())){
			return SystemCode.PASWORD_ERROR;
		}
		
		token = MD5Util.GetMD5Code(mobile + System.currentTimeMillis());
		user.setToken(token);
		dao.update(user);
		
		SystemCode.SUCCESS.setResultObject(user);
		return SystemCode.SUCCESS;
		
	}

	public SystemCode freeLogin(UserFreeLoginRequest req) {
		String token = req.getToken();
		String userId = req.getUserId();
		
		User user = dao.getByToken(token);
		
		if(user == null || !StringUtils.equals(user.getId(), userId)){
			return SystemCode.LOGIN_EXPIRED;
		}
		
		token = MD5Util.GetMD5Code(user.getMobile() + System.currentTimeMillis());
		user.setToken(token);
		dao.update(user);

		SystemCode.SUCCESS.setResultObject(user);
		return SystemCode.SUCCESS;
	}
	
	public User getByMobile(String mobile) {
		return dao.getByMobile(mobile);
	}
	
	public SystemCode getByToken(String token) {
		User user = dao.getByToken(token);
		if(user == null ){
			return SystemCode.USER_NOTEXIST;
		}
		SystemCode.SUCCESS.setResultObject(user);
		return SystemCode.SUCCESS;
	}
	
	public SystemCode changePwd(String password,String id) {
		dao.updatePwd(password,id);
		return SystemCode.SUCCESS;
	}

	public SystemCode userLogout(BaseRequestParameter req) {
		User user = dao.get(req.getUserId());
		user.setToken("logout");
		dao.updateSelective(user);
		
		return SystemCode.SUCCESS;
	}
	
}