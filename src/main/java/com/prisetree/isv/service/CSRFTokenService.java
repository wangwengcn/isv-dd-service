package com.prisetree.isv.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.common.util.MD5Util;

@Service
@Transactional(readOnly = true)
public class CSRFTokenService {
	
	public SystemCode createToken(String corpId){
		if(StringUtils.isBlank(corpId)) {
			return SystemCode.TRY_TO_RELOGIN;
		}
		
		String token = MD5Util.GetMD5Code(corpId + System.currentTimeMillis());
		System.out.println("当前token" + token);
        SystemCode.SUCCESS.setResultObject(token);
        return SystemCode.SUCCESS;
	}


}