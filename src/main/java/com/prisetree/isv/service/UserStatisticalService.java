package com.prisetree.isv.service;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dingtalk.isv.access.biz.prisetree.dao.UserStatisticalDao;
import com.prisetree.isv.common.service.CrudService;
import com.prisetree.isv.domain.UserStatistical;

@Service
@Transactional(readOnly = true)
public class UserStatisticalService extends CrudService<UserStatisticalDao, UserStatistical> {
	
	public UserStatistical saveOrUpdateUser(String corpId, String userId) {
		Date now = new Date();
		SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss");
		String str1 = df1.format(now);
		String str2 = df2.format(now);
		ParsePosition pos = new ParsePosition(0);
		
		UserStatistical userStatistical = new UserStatistical();
		userStatistical.setDate(df1.parse(str1, pos));
		userStatistical.setTime(str2);
		userStatistical.setUserId(userId);
		userStatistical.setId(corpId);
		
		// 保存最新用户数据入库
		dao.saveOrUpdateUserStatistical(userStatistical);
		return userStatistical;
	}
	
	public List<Map<Object,Object>> countWeek() {
		return dao.countWeek();
	}
	
	public List<Map<Object,Object>> countMonth() {
		return dao.countMonth();
	}
	
}