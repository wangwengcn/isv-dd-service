package com.prisetree.isv.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dingtalk.isv.access.biz.prisetree.dao.OfficeBookingsMapper;
import com.dingtalk.isv.access.biz.prisetree.dao.OfficeResourcesMapper;
import com.dingtalk.isv.access.biz.prisetree.dao.OrdersMapper;
import com.prisetree.isv.common.constant.ConstantsUtil;
import com.prisetree.isv.common.constant.SystemCode;
import com.prisetree.isv.common.util.IdGen;
import com.prisetree.isv.domain.OfficeBookings;
import com.prisetree.isv.domain.OfficeResources;
import com.prisetree.isv.domain.Orders;
import com.prisetree.isv.domain.query.OfficeBookingsExample;
import com.prisetree.isv.request.parameter.CreateBookingsRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@Transactional(readOnly = true)
public class OfficeBookingsService {
	
	@Autowired
	OfficeBookingsMapper officeBookingsMapper;
	@Autowired
	OfficeResourcesMapper officeResourcesMapper;
	@Autowired
	OrdersMapper ordersMapper;
	
	public SystemCode insert(CreateBookingsRequest req){
		if(StringUtils.isBlank(req.getOfficeResourcesId())) {
			return SystemCode.OFFICE_RESOURCES_NOTFOUND;
		}
		Orders order = new Orders();
		order.setId(IdGen.uuid());
		order.setOrderNumber("" + System.currentTimeMillis() + (int)((Math.random()*9+1)*1000));
		order.setOfficeResourcesId(req.getOfficeResourcesId());
		order.setOfficeResourcesName(req.getOfficeResourcesName());
		order.setOfficeResourcesAddr(req.getOfficeResourcesAddr());
		order.setOfficeResourcesMobile(req.getOfficeResourcesMobile());
		order.setCompanyId(req.getCompanyId());
		order.setCompanyName(req.getCompanyName());
		order.setCompanyMobile(req.getCompanyMobile());
		order.setState(ConstantsUtil.ORDER_STATE_DOING);
		order.setType(req.getType());
		order.setCreateDate(new Date());
        int a = ordersMapper.insert(order);
        if (a != 1) {
        	return SystemCode.CREATE_BOOKING_FAIL;
        }
        SystemCode.SUCCESS.setResultObject(order);
        return SystemCode.SUCCESS;
	}

	public SystemCode list(String corpId) {
		OfficeBookingsExample example = new OfficeBookingsExample();
		example.createCriteria().andCompanyIdEqualTo(corpId);
		List<OfficeBookings> list = officeBookingsMapper.selectByExample(example);
		JSONArray arr = new JSONArray();
		if (list != null && list.size() > 0) {
			for (OfficeBookings booking : list) {
				JSONObject jsub = new JSONObject();
				jsub.put("id", booking.getId());
				jsub.put("resourcesName", booking.getOfficeResourcesName());
				arr.add(jsub);
			}
		}
		JSONObject json = new JSONObject();
		json.put("orderList", arr);
		SystemCode.SUCCESS.setResultObject(json);
		return SystemCode.SUCCESS;
	}

	public SystemCode detail(String id) {
		OfficeBookings booking = officeBookingsMapper.selectByPrimaryKey(id);
		JSONObject json = new JSONObject();
		json.put("id", id);
		if (StringUtils.equals(booking.getBookingState(), "0")) {
			json.put("bookingState", "预约中");
		} else if (StringUtils.equals(booking.getBookingState(), "1")) {
			json.put("bookingState", "已预约");
		}
		json.put("companyId", booking.getCompanyId());
		json.put("companyName", booking.getCompanyName());
		json.put("companyMobile", booking.getCompanyMobile());
		json.put("resourcesName", booking.getOfficeResourcesName());
		json.put("resourcesAddr", booking.getOfficeResourcesAdd());
		json.put("resourcesMobile", booking.getOfficeResourcesMobile());
		SystemCode.SUCCESS.setResultObject(json);
		return SystemCode.SUCCESS;
	}
	
	public SystemCode delete(String id) {
		int a = officeBookingsMapper.deleteByPrimaryKey(id);
		if (a != 1) {
        	return SystemCode.DELETE_BOOKING_FAIL;
        }
        return SystemCode.SUCCESS;
	}

}
