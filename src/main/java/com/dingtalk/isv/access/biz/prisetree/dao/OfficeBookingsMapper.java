package com.dingtalk.isv.access.biz.prisetree.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.prisetree.isv.domain.OfficeBookings;
import com.prisetree.isv.domain.query.OfficeBookingsExample;

@Repository
public interface OfficeBookingsMapper {

    long countByExample(OfficeBookingsExample example);

    int deleteByExample(OfficeBookingsExample example);

    int deleteByPrimaryKey(String id);

    int insert(OfficeBookings record);

    int insertSelective(OfficeBookings record);

    List<OfficeBookings> selectByExample(OfficeBookingsExample example);

    OfficeBookings selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OfficeBookings record, @Param("example") OfficeBookingsExample example);

    int updateByExample(@Param("record") OfficeBookings record, @Param("example") OfficeBookingsExample example);

    int updateByPrimaryKeySelective(OfficeBookings record);

    int updateByPrimaryKey(OfficeBookings record);
}