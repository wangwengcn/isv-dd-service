package com.dingtalk.isv.access.biz.prisetree.dao;

import com.prisetree.isv.domain.OfficeResourcesRent;
import com.prisetree.isv.domain.query.OfficeResourcesRentExample;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeResourcesRentMapper {

    long countByExample(OfficeResourcesRentExample example);

    int deleteByExample(OfficeResourcesRentExample example);

    int deleteByPrimaryKey(String id);

    int insert(OfficeResourcesRent record);

    int insertSelective(OfficeResourcesRent record);

    List<OfficeResourcesRent> selectByExample(OfficeResourcesRentExample example);

    OfficeResourcesRent selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OfficeResourcesRent record, @Param("example") OfficeResourcesRentExample example);

    int updateByExample(@Param("record") OfficeResourcesRent record, @Param("example") OfficeResourcesRentExample example);

    int updateByPrimaryKeySelective(OfficeResourcesRent record);

    int updateByPrimaryKey(OfficeResourcesRent record);
}