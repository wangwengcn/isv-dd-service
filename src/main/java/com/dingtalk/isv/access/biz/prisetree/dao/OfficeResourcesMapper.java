package com.dingtalk.isv.access.biz.prisetree.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.prisetree.isv.domain.OfficeResources;
import com.prisetree.isv.domain.query.OfficeResourcesExample;

@Repository
public interface OfficeResourcesMapper {

    long countByExample(OfficeResourcesExample example);

    int deleteByExample(OfficeResourcesExample example);

    int deleteByPrimaryKey(String id);

    int insert(OfficeResources record);

    int insertSelective(OfficeResources record);

    List<OfficeResources> selectByExample(OfficeResourcesExample example);
    
    List<OfficeResources> selectByParam(Map<String, Object> param);

    OfficeResources selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OfficeResources record, @Param("example") OfficeResourcesExample example);

    int updateByExample(@Param("record") OfficeResources record, @Param("example") OfficeResourcesExample example);

    int updateByPrimaryKeySelective(OfficeResources record);

    int updateByPrimaryKey(OfficeResources record);
}