package com.dingtalk.isv.access.biz.prisetree.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.prisetree.isv.domain.OfficeResourcesImg;
import com.prisetree.isv.domain.query.OfficeResourcesImgExample;

@Repository
public interface OfficeResourcesImgMapper {

    long countByExample(OfficeResourcesImgExample example);

    int deleteByExample(OfficeResourcesImgExample example);

    int deleteByPrimaryKey(String id);

    int insert(OfficeResourcesImg record);

    int insertSelective(OfficeResourcesImg record);

    List<OfficeResourcesImg> selectByExample(OfficeResourcesImgExample example);

    OfficeResourcesImg selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OfficeResourcesImg record, @Param("example") OfficeResourcesImgExample example);

    int updateByExample(@Param("record") OfficeResourcesImg record, @Param("example") OfficeResourcesImgExample example);

    int updateByPrimaryKeySelective(OfficeResourcesImg record);

    int updateByPrimaryKey(OfficeResourcesImg record);

	void insertBatch(List<OfficeResourcesImg> list);
}