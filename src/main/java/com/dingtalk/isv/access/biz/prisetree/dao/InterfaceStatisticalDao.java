package com.dingtalk.isv.access.biz.prisetree.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.prisetree.isv.domain.InterfaceStatistical;

@Repository
public interface InterfaceStatisticalDao {
	
	void saveOrUpdateInterfaceStatistical(InterfaceStatistical interfaceStatistical);
	
	List<InterfaceStatistical> getList(Map<String, Object> param);
	
}