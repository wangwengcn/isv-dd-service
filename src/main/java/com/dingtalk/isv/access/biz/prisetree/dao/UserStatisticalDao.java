package com.dingtalk.isv.access.biz.prisetree.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.prisetree.isv.common.dao.CrudDao;
import com.prisetree.isv.domain.UserStatistical;

@Repository
public interface UserStatisticalDao extends CrudDao<UserStatistical> {
	
	void saveOrUpdateUserStatistical(UserStatistical userStatistical);

	UserStatistical getByUser(String userId, String date);
	
	List<Map<Object,Object>> countWeek();
	
	List<Map<Object,Object>> countMonth();
	
}