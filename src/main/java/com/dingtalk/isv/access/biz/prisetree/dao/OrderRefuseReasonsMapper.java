package com.dingtalk.isv.access.biz.prisetree.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.prisetree.isv.domain.OrderRefuseReasons;
import com.prisetree.isv.domain.query.OrderRefuseReasonsExample;

@Repository
public interface OrderRefuseReasonsMapper {

    long countByExample(OrderRefuseReasonsExample example);

    int deleteByExample(OrderRefuseReasonsExample example);

    int deleteByPrimaryKey(String id);

    int insert(OrderRefuseReasons record);

    int insertSelective(OrderRefuseReasons record);

    List<OrderRefuseReasons> selectByExample(OrderRefuseReasonsExample example);

    OrderRefuseReasons selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OrderRefuseReasons record, @Param("example") OrderRefuseReasonsExample example);

    int updateByExample(@Param("record") OrderRefuseReasons record, @Param("example") OrderRefuseReasonsExample example);

    int updateByPrimaryKeySelective(OrderRefuseReasons record);

    int updateByPrimaryKey(OrderRefuseReasons record);
}