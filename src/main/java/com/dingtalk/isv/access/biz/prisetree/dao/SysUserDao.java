package com.dingtalk.isv.access.biz.prisetree.dao;

import org.springframework.stereotype.Repository;

import com.prisetree.isv.common.dao.CrudDao;
import com.prisetree.isv.domain.User;

@Repository
public interface SysUserDao extends CrudDao<User> {

	User getByMobile(String mobile);

	User getByToken(String token);
	
	void updatePwd(String password,String id);
	
}