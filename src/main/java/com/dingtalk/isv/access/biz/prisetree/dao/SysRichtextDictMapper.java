package com.dingtalk.isv.access.biz.prisetree.dao;

import com.prisetree.isv.domain.SysRichtextDict;
import com.prisetree.isv.domain.query.SysRichtextDictExample;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SysRichtextDictMapper {

    long countByExample(SysRichtextDictExample example);

    int deleteByExample(SysRichtextDictExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysRichtextDict record);

    int insertSelective(SysRichtextDict record);

    List<SysRichtextDict> selectByExampleWithBLOBs(SysRichtextDictExample example);

    List<SysRichtextDict> selectByExample(SysRichtextDictExample example);

    SysRichtextDict selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysRichtextDict record, @Param("example") SysRichtextDictExample example);

    int updateByExampleWithBLOBs(@Param("record") SysRichtextDict record, @Param("example") SysRichtextDictExample example);

    int updateByExample(@Param("record") SysRichtextDict record, @Param("example") SysRichtextDictExample example);

    int updateByPrimaryKeySelective(SysRichtextDict record);

    int updateByPrimaryKeyWithBLOBs(SysRichtextDict record);

    int updateByPrimaryKey(SysRichtextDict record);
}