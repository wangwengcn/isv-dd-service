package com.dingtalk.isv.access.biz.prisetree.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.prisetree.isv.common.dao.CrudDao;
import com.prisetree.isv.domain.SysDict;

@Repository
public interface SysDictDao extends CrudDao<SysDict> {

    public List<String> findTypeList(SysDict dict);
    
    public List<SysDict> find(SysDict dict);
    
    public List<String> findOtherTag();
    
    public List<String> findCityTag();
    
    public List<SysDict> findFixedPriceTag();

    public List<SysDict> findFixedPriceTag2();
    
    public List<SysDict> findFlowPriceTag();

	public List<SysDict> findOfficeType();
}
