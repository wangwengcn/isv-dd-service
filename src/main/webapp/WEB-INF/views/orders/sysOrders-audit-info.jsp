<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<%@include file="/WEB-INF/views/include/formlib.jsp" %>
<title>订单审核</title>
</head>
<body>
<article class="page-container">
	<div class="box">
	<form class="form form-horizontal" id="form-edit" modelAttribute="res">
	<div class="row cl">
        <label class="form-label col-xs-4 col-sm-4">说明：</label>
        <div class="formControls col-xs-6 col-sm-6">
            <textarea rows="5" cols="" style="width:350px;" name="reason" readonly="readonly">${res.reason }</textarea>
        </div>
    </div>
    <div class="row cl">
        <label class="form-label col-xs-4 col-sm-4">审核时间：</label>
        <div class="formControls col-xs-6 col-sm-6">
            ${fns:format(res.createDate,"yyyy-MM-dd")}
        </div>
    </div>
	</form>
	</div>
</article>

<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript">
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	
	$("#form-edit").validate({
		rules:{
			reason:{
				required:true,
			}
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success: function (data) {
					var index = parent.layer.getFrameIndex(window.name);
				 	parent.$('.btn-success').click(); 
					parent.layer.close(index);  
                },
                error: function () {
                	layer.confirm("系统出现错误，请联系管理员！");
                }		
			});
		}
	});
});
</script> 
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>