<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/views/include/head.jsp" %>
    <%@include file="/WEB-INF/views/include/formlib.jsp" %>
    <title>添加字典</title>
    <script type="text/javascript" src="${ctxStatic}/h-ui/lib/kindeditor-4.1.7/kindeditor-all-min.js"></script>
</head>
<body>
<article class="page-container">
	<div class="box">
    <form class="form form-horizontal" id="form-add" modelAttribute="entity"
          action="<%=request.getContextPath()%>/a/sysRichTextDict/save" method="post">
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>类型：</label>

            <div class="formControls col-xs-9 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="" id="type" name="type">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>标签：</label>

            <div class="formControls col-xs-9 col-sm-9">
                <input type="text" class="input-text" value="" placeholder="" id="label" name="label">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2"><span class="c-red">*</span>键值：</label>
            <div class="formControls col-xs-9 col-sm-9">
                <textarea id="value" name="value"></textarea>
            </div>
        </div>
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
            </div>
        </div>
    </form>
    </div>
</article>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">
    $(function () {
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });

        $("#form-add").validate({
            rules: {
                value: {
                    required: true,
                },
                label: {
                    required: true,
                },
                type: {
                    required: true,
                }
            },
            onkeyup: false,
            focusCleanup: true,
            success: "valid",
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (data) {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.$('.btn-success').click();
                        parent.layer.close(index);
                    },
                    error: function () {
                        layer.confirm("系统出现错误，请联系管理员！");
                    }
                });
            }
        });
        
        KindEditor.ready(function(K) {
    		var editor = K.create('textarea[name="value"]', {
    			width: '100%',
    			height: '400px',
    			allowFileManager : false,
    			allowImageUpload : true,
    			allowFlashUpload : false,
    			allowMediaUpload : false,
    			allowFileUpload : true,
    			filterMode : false,
    			uploadJson : '${ctx}/files/kindeditor/imgUpload',
    			items : [ 'source', '|', 'undo', 'redo', '|', 'preview',
    					'print', 'template', 'code', 'cut', 'copy', 'paste',
    					'plainpaste', 'wordpaste', '|', 'justifyleft',
    					'justifycenter', 'justifyright', 'justifyfull',
    					'insertorderedlist', 'insertunorderedlist', 'indent',
    					'outdent', 'subscript', 'superscript', 'clearhtml',
    					'quickformat', 'selectall', '|', 'fullscreen', '/',
    					'formatblock', 'fontname', 'fontsize', '|',
    					'forecolor', 'hilitecolor', 'bold', 'italic',
    					'underline', 'strikethrough', 'lineheight',
    					'removeformat', '|', 'image',
    					'table', 'hr', 'emoticons', 'baidumap',
    					'pagebreak', 'anchor', 'link', 'unlink', '|', 'about' ],
    			afterBlur: function(){
    				this.sync();
    			}
    		});
    	});
    });
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>