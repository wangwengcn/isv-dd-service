<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/views/include/head.jsp" %>
    <%@include file="/WEB-INF/views/include/formlib.jsp" %>
    <title>添加用户</title>
</head>
<body>
<article class="page-container">
	<div class="box">
    <form class="form form-horizontal" id="form-add" modelAttribute="entity"
          action="${ctx}/a/user/save" method="post">
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">姓名：</label>
            <div class="formControls col-xs-4 col-sm-4">
                <input type="text" class="input-text" value="" placeholder="" id="realName" name="realName">
            </div>

            <label class="form-label col-xs-2 col-sm-2">联系电话：</label>
            <div class="formControls col-xs-4 col-sm-4">
                <input type="text" class="input-text" value="" placeholder="" id="mobile" name="mobile">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">公司：</label>
            <div class="formControls col-xs-4 col-sm-4">
                <input type="text" class="input-text" value="" placeholder="" id="company" name="company">
            </div>

            <label class="form-label col-xs-2 col-sm-2">用户类型：</label>
	        <div class="formControls col-xs-4 col-sm-4">
	            <form:select id="userType" path="entity.userType" class="select">
	                <form:options items="${fns:getDictList('user_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
	            </form:select>
	        </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">密码：</label>
            <div class="formControls col-xs-4 col-sm-4">
                <input type="password" class="input-text" value="" placeholder="" id="password" name="password">
            </div>
        </div>
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
            </div>
        </div>
    </form>
    </div>
</article>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">
    $(function () {
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });

        $("#form-add").validate({
            rules: {
                username: {
                    required: true,
                },
                password: {
                    required: true,
                },
                mobile:{
                    isMobile:true,
                },
            },
            onkeyup: false,
            focusCleanup: true,
            success: "valid",
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (data) {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.$('.btn-success').click();
                        parent.layer.close(index);
                    },
                    error: function () {
                        layer.confirm("系统出现错误，请联系管理员！");
                    }
                });
            }
        });
    });
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>