<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
	<%@include file="/WEB-INF/views/include/head.jsp" %>
	<%@include file="/WEB-INF/views/include/formlib.jsp" %>
<title>编辑字典</title>
</head>
<body>
<article class="page-container">
	<div class="box">
	<form class="form form-horizontal" id="form-edit" modelAttribute="entity" action="${ctx}/a/sysDict/save" method="post">
	<input type="hidden" name="id" value="${entity.id}">
	<div class="row cl">
		<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>类型：</label>
		<div class="formControls col-xs-8 col-sm-9">
			<input type="text" class="input-text" value="${entity.type}" placeholder="" id="type" name="type">
		</div>
	</div>
	<div class="row cl">
		<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>标签：</label>
		<div class="formControls col-xs-8 col-sm-9">
			<input type="text" class="input-text" value="${entity.label}" placeholder="" id="label" name="label">
		</div>
	</div>
	<div class="row cl">
		<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>键值：</label>
		<div class="formControls col-xs-8 col-sm-9">
			<input type="text" class="input-text" value="${entity.value}" placeholder="" id="value" name="value">
		</div>
	</div>
	<div class="row cl">
		<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>排序：</label>
		<div class="formControls col-xs-8 col-sm-9">
			<input type="text" class="input-text" value="${entity.sort}" placeholder="" id="sort" name="sort">
		</div>
	</div>
	<div class="row cl">
		<label class="form-label col-xs-4 col-sm-3"><span class="c-red"></span>描述：</label>
		<div class="formControls col-xs-8 col-sm-9">
			<input type="text" class="input-text" value="${entity.description}" placeholder="" id="description" name="description">
		</div>
	</div>
	<div class="row cl">
		<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
			<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
		</div>
	</div>
	</form>
	</div>
</article>

<!--请在下方写此页面业务相关的脚本--> 
<script type="text/javascript">
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	
	$("#form-edit").validate({
		rules:{
			value:{
				required:true,
			},
			label:{
				required:true,
			},
			type:{
				required:true,
			},
			sort:{
				required:true,
			},
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success: function (data) {
					var index = parent.layer.getFrameIndex(window.name);
				 	parent.$('.btn-success').click(); 
					parent.layer.close(index);  
                },
                error: function () {
                	layer.confirm("系统出现错误，请联系管理员！");
                }		
			});
		}
	});
});
</script> 
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>