<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.prisetree.isv.common.util.DictUtils"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<HTML>
<head>
<%@include file="/WEB-INF/views/include/head.jsp"%>
<title>房源列表</title>
</head>
<body>
 		<ul id="Huifold1" class="Huifold">
			<li class="item">
				<h4>
					点击新增图片<b><i class="Hui-iconfont Hui-iconfont-arrow2-bottom"></i></b>
				</h4>
				<div class="info">
					<form:form class="form form-horizontal" id="form-add" enctype="multipart/form-data" commandName="entity" action="${ctx}/a/sysOfficeResources/img/save" method="post">
						<input type="hidden" name="officeResourcesId" value="${param.id }">
						<div class="row cl">
				            <label class="form-label col-xs-2 col-sm-2">图片：</label>
				            <div class="formControls col-xs-4 col-sm-4" id="uurl">
				            	<input type="text" name="imgUrl" class="input-text upload-url radius" placeholder="七牛url地址，多个地址英文逗号分割" style="width:250px;">
				            </div>
<!-- 				            <div class="formControls col-xs-4 col-sm-4" id="ffile"> -->
<!-- 				            	<span class="btn-upload form-group"> -->
<!-- 								  <input class="input-text upload-url radius" style="width:200px;" type="text" name="uploadfile-1" id="uploadfile-1" readonly>&nbsp;<a href="javascript:void();" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe642;</i> 选择</a> -->
<!-- 								  <input type="file" name="imgFile" class="input-file"> -->
<!-- 								</span> -->
<!-- 				            </div> -->
<!-- 				            <label class="form-label col-xs-2 col-sm-2">上传方式：</label> -->
<!-- 				            <div class="formControls col-xs-4 col-sm-4"> -->
<%-- 				                <form:select path="uploadType" id="uploadType" onchange="changeInput()"> --%>
<%-- 					                <form:options items="${fns:getDictList('office_img_upload_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/> --%>
<%-- 					            </form:select> --%>
<!-- 				            </div> -->
				        </div>
				        <div class="row cl">
				        	<label class="form-label col-xs-2 col-sm-2">序号：</label>
				            <div class="formControls col-xs-4 col-sm-4">
				                <input type="text" name="orderNum" class="input-text upload-url radius" placeholder="请使用10，20，30等序号以便于修改" style="width:250px;">
				            </div>
<!-- 				            <label class="form-label col-xs-2 col-sm-2">图片类型：</label> -->
<!-- 				            <div class="formControls col-xs-4 col-sm-4"> -->
<%-- 				                <form:select path="isHeader"> --%>
<%-- 					                <form:options items="${fns:getDictList('office_img_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/> --%>
<%-- 					            </form:select> --%>
<!-- 				            </div> -->
				        </div>
				        <div class="row cl">
				            <label class="form-label col-xs-2 col-sm-6">
				            	<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;保存&nbsp;&nbsp;">
							</label>
				        </div>
					</form:form>
				</div>
			</li>
		</ul> 
	<div class="page-container">
		<table id="dataTable" class="table table-border table-bordered table-bg">
			<thead>
				<tr class="text-c">
					<th width="100">图片</th>
<!-- 					<th width="80">类型</th> -->
					<th width="80">序号</th>
					<th width="80">操作</th>
				</tr>
				<c:forEach items="${datas }" var="img">
					<tr class="text-c odd">
						<td><img src="http://oxndq0rq4.bkt.clouddn.com/${img.imgUrl }" style="max-height: 80px;"></td>
<!-- 						<td> -->
<%-- 							<c:choose> --%>
<%-- 								<c:when test="${img.isHeader == 1 }">轮播图</c:when> --%>
<%-- 								<c:when test="${img.isHeader == 2 }">列表头像</c:when> --%>
<%-- 								<c:when test="${img.isHeader == 3 }">地理图</c:when> --%>
<%-- 							</c:choose> --%>
<!-- 						</td> -->
						<td>${img.orderNum }</td>
						<td>
							<a title="删除" href="javascript:;" onclick="admin_del(this, '${ctx }/a/sysOfficeResources/img/delete?id=', '${img.id }')" class="ml-5" style="text-decoration:none">
								<i class="Hui-iconfont">&#xe6e2;</i>
							</a>
						</td>
					</tr>
				</c:forEach>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
<%-- 	var officeImgUploadType = <%=DictUtils.getJsonDict("office_img_upload_type")%>; --%>
// 	var changeInput = function(){
// 		if($('#uploadType').val() == "1"){
// 			$('#uurl').show();
// 			$('#ffile').hide();
// 		} else if($('#uploadType').val() == "2"){
// 			$('#uurl').hide();
// 			$('#ffile').show();
// 		}
// 	}
// 	changeInput();
	$(document).ready(function () {
		$.Huifold("#Huifold1 .item h4", "#Huifold1 .item .info",
				"fast", 1, "click");
	})
</script>
</body>
</html>