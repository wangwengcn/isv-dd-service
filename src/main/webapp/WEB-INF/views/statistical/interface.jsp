<%@page import="com.prisetree.isv.statistical.Interfaces"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.prisetree.isv.common.util.DictUtils"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<HTML>
<head>
<%@include file="/WEB-INF/views/include/head.jsp"%>
<title>房源列表</title>
</head>
<body>
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>
		数据监控 <span class="c-gray en">&gt;</span>接口统计
		<a class="btn btn-success radius r" style="line-height: 1.6em; margin-top: 3px; margin-left: 3px" href="javascript:location.replace(location.href);" title="刷新"><i	class="Hui-iconfont">&#xe68f;</i></a>
		<a class="btn btn-danger radius r" style="line-height: 1.6em; margin-top: 3px; margin-left: 3px" href="javascript:;" onclick="clean()" ><i class="Hui-iconfont">&#xe6e2;</i>清除</a>
	</nav>
	<div class="page-container">
		<div id="week" style="width: 600px;height:400px;"></div>
	</div>
	<script type="text/javascript" src="${ctxStatic}/js/echarts.common.min.js"></script>
	<script type="text/javascript">
		<%
			String interfaceId = (String)request.getAttribute("interfaceId");
			if(interfaceId.equals(Interfaces.OFFICE_RESOURCES_LIST.toString()))
			{
				interfaceId = "房源列表查询接口";
			}
			else if(interfaceId.equals(Interfaces.PLACE_ORDER.toString()))
			{
				interfaceId = "下单接口";
			}
		%>
		var weekChart = echarts.init(document.getElementById("week"));
		var option = {
			    title : {
			        text: '<%=interfaceId %>'
			    },
			    tooltip : {
			        trigger: 'axis'
			    },
			    legend: {
			        data:['QPS','RT']
			    },
			    toolbox: {
			        show : false,
			        feature : {
			            mark : {show: true},
			            dataView : {show: true, readOnly: false},
			            magicType : {show: true, type: ['line', 'bar']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'category',
			            boundaryGap : false,
			            data : [${date}]
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value',
			            axisLabel : {
			                formatter: '{value}'
			            }
			        }
			    ],
			    series : [
			        {
			            name:'QPS',
			            type:'line',
			            data:[${countStr}],
			            markLine : {
			                data : [
			                    {type : 'average', name: '平均值'}
			                ]
			            }
			        },
			        {
			            name:'RT',
			            type:'line',
			            data:[${rtStr}],
			            markLine : {
			                data : [
			                    {type : 'average', name : '平均值'}
			                ]
			            }
			        }
			    ]
			};
			                    
		weekChart.setOption(option);

	</script>
</body>
</html>