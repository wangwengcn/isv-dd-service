<%@ page contentType="text/html;charset=UTF-8" %>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta name="author" content="http://sdmobi.cn"/>
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
      content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
<meta http-equiv="Cache-Control" content="no-siteapp"/>
<!-- <meta name="keywords" content="货带后台管理系统"> -->
<!-- <meta name="description" content="货带后台管理系统，由杭州速蝶数据科技有限公司提供技术支持。"> -->
<LINK rel="Bookmark" href="/favicon.ico">
<LINK rel="Shortcut Icon" href="/favicon.ico"/>
<!--[if lt IE 9]>
<script type="text/javascript" src="${ctxStatic}/h-ui/lib/html5.js"></script>
<script type="text/javascript" src="${ctxStatic}/h-ui/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="${ctxStatic}/h-ui/css/H-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/h-ui/css/H-ui.admin.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/h-ui/lib/Hui-iconfont/1.0.7/iconfont.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/h-ui/lib/icheck/icheck.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/h-ui/skin/default/skin.css" id="skin"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/h-ui/css/style.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/css/select2.min.css" />
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->

<script type="text/javascript" src="${ctxStatic}/js/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="${ctxStatic}/js/layer/2.1/layer.js"></script>
<script type="text/javascript" src="${ctxStatic}/js/laypage/1.2/laypage.js"></script>
<script type="text/javascript" src="${ctxStatic}/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="${ctxStatic}/h-ui/js/H-ui.admin.js"></script>
<script type="text/javascript" src="${ctxStatic}/js/fns.js"></script>
<script type="text/javascript" src="${ctxStatic}/js/select2.min.js"></script>
<style type="text/css">
    .dataTables_wrapper .dataTables_length {
        padding-bottom: 0;
        padding-top: 10px;
        padding-left: 20px;
    }
</style>

<script type="text/javascript">
var projectName = "${ctx}";
</script>