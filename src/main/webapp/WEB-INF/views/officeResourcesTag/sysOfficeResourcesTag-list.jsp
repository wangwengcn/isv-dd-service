<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.prisetree.isv.common.util.DictUtils"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<HTML>
<head>
<%@include file="/WEB-INF/views/include/head.jsp"%>
<title>房源列表</title>
</head>
<body>
 		<ul id="Huifold1" class="Huifold">
			<li class="item">
				<h4>
					点击新增标签<b><i class="Hui-iconfont Hui-iconfont-arrow2-bottom"></i></b>
				</h4>
				<div class="info">
					<form:form class="form form-horizontal" id="form-add" commandName="entity" action="${ctx}/a/sysOfficeResources/tag/save" method="post">
						<input type="hidden" name="officeResourcesId" value="${param.id }">
						<div class="row cl">
				            <label class="form-label col-xs-2 col-sm-2">选择标签：</label>
				            <div class="formControls col-xs-4 col-sm-4">
				                <form:select path="tag">
					                <form:options items="${fns:getDictList('office_tag')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
					            </form:select>
				            </div>
				            <label class="form-label col-xs-2 col-sm-2">
				            	<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;保存&nbsp;&nbsp;">
							</label>
				        </div>
					</form:form>
				</div>
			</li>
		</ul> 
	<div class="page-container">
		<table id="dataTable" class="table table-border table-bordered table-bg">
			<thead>
				<tr class="text-c">
					<th width="80">标签</th>
					<th width="100">操作</th>
				</tr>
				<c:forEach items="${datas }" var="img">
					<tr class="text-c odd">
						<td>${fns:getDictLabel(img.tag, "office_tag", "") }</td>
						<td>
							<a title="删除" href="javascript:;" onclick="admin_del(this, '${ctx }/a/sysOfficeResources/tag/delete?id=', '${img.id }')" class="ml-5" style="text-decoration:none">
								<i class="Hui-iconfont">&#xe6e2;</i>
							</a>
						</td>
					</tr>
				</c:forEach>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
	var userType = <%=DictUtils.getJsonDict("user_type")%>;
	$(document).ready(function () {
		$.Huifold("#Huifold1 .item h4", "#Huifold1 .item .info",
				"fast", 1, "click");
	})
</script>
</body>
</html>