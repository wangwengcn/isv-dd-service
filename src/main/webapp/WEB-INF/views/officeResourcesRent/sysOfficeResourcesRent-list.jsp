<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.prisetree.isv.common.util.DictUtils"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<HTML>
<head>
<%@include file="/WEB-INF/views/include/head.jsp"%>
<title>列表</title>
</head>
<body>
 		<div class="page-container">
	<form:form class="form form-horizontal" id="form-article-add" commandName="entity" action="${ctx}/a/sysOfficeResources/rent/save" method="post">
		<input type="hidden" name="parentId" value="${param.id }">
		<div id="tab-system" class="HuiTab">
			<div class="tabBar cl">
				<span class="current">按工位</span>
				<span>按面积</span>
			</div>
			<div class="tabCon" style="display: block;">
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						名称：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" id="website-title" name="name1" placeholder="控制在25个字、50个字节以内" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						单价（工位/时间单位）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="website-Keywords" name="staitonPrice" placeholder="" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						工位数（总数）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="website-description" name="stationAllnum" placeholder="" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						标准入住人数（单间）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="website-static" name="standardNum" placeholder="" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						剩余工位数：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="website-uploadfile" name="surplusNum" placeholder="" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						工位类型（独立/开放）：</label>
					<div class="formControls col-xs-7 col-sm-7">
		                <form:select path="stationType">
			                <form:options items="${fns:getDictList('station_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			            </form:select>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">工位属性（固定/流动）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<form:select path="stationAttr">
			                <form:options items="${fns:getDictList('station_attr')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			            </form:select>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">时间单位（月／周／天）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<form:select path="billingMode">
			                <form:options items="${fns:getDictList('billing_mode')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
			            </form:select>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">最低出租时间单位（月／周／天）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d]/g,'')" id="website-uploadfile" name="minTime" placeholder="" value="" class="input-text">
					</div>
				</div>
				
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button onclick="article_save_submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont"></i> 保存</button>
						<button onclick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
					</div>
				</div>
				
				<div class="page-container">
					<table id="dataTable" class="table table-border table-bordered table-bg">
						<thead>
							<tr class="text-c">
								<th width="80">名称</th>
								<th width="80">单价（每工位/时间单位）</th>
								<th width="80">工位数（总数）</th>
								<th width="80">标准入住人数 （单间）</th>
								<th width="80">剩余工位数</th>
								<th width="80">工位类型</th>
								<th width="80">工位属性</th>
								<th width="80">时间单位</th>
								<th width="80">最低出租时间单位</th>
								<th width="100">操作</th>
							</tr>
							<c:forEach items="${datasStation }" var="datasStation">
								<tr class="text-c odd">
									<td>${datasStation.name }</td>
									<td>${datasStation.staitonPrice }</td>
									<td>${datasStation.stationAllnum }</td>
									<td>${datasStation.standardNum }</td>
									<td>${datasStation.surplusNum }</td>
									<td>
										<c:choose>
											<c:when test="${datasStation.stationType == 0 }">独立</c:when>
											<c:when test="${datasStation.stationType == 1 }">开放</c:when>
										</c:choose>
									</td>
									<td>
										<c:choose>
											<c:when test="${datasStation.stationAttr == 0 }">固定</c:when>
											<c:when test="${datasStation.stationAttr == 1 }">流动</c:when>
										</c:choose>
									</td>
									<td>
										<c:choose>
											<c:when test="${datasStation.billingMode == 0 }">按天</c:when>
											<c:when test="${datasStation.billingMode == 1 }">按周</c:when>
											<c:when test="${datasStation.billingMode == 2 }">按月</c:when>
										</c:choose>
									</td>
									<td>${datasStation.minTime }</td>
									<td>
										<a title="删除" href="javascript:;" onclick="admin_del(this, '${ctx }/a/sysOfficeResources/rent/delete?id=', '${datasStation.id }')" class="ml-5" style="text-decoration:none">
											<i class="Hui-iconfont">&#xe6e2;</i>
										</a>
									</td>
								</tr>
							</c:forEach>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="tabCon" style="display: none;">
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						名称：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" id="website-title" name="name2" placeholder="控制在25个字、50个字节以内" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						单价（平米/天）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d\.]/g,'')" id="website-Keywords" name="unitPrice" placeholder="" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						面积（平米）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d\.]/g,'')" id="website-description" name="area" placeholder="" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						租金（月）：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d\.]/g,'')" id="website-static" name="areaPrice" placeholder="" value="" class="input-text">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-5 col-sm-5">
						<span class="c-red">*</span>
						使用率：</label>
					<div class="formControls col-xs-7 col-sm-7">
						<input type="text" onkeyup="value=value.replace(/[^\d\.]/g,'')" id="website-uploadfile" name="usageRate" placeholder="百分比，只填数字" value="" class="input-text">
					</div>
				</div>
				
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
						<button onclick="article_save_submit();" class="btn btn-primary radius" type="submit"><i class="Hui-iconfont"></i> 保存</button>
						<button onclick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
					</div>
				</div>
				
				<div class="page-container">
					<table id="dataTable" class="table table-border table-bordered table-bg">
						<thead>
							<tr class="text-c">
								<th width="80">名称</th>
								<th width="80">单价（平米/天）</th>
								<th width="80">面积</th>
								<th width="80">租金（月）</th>
								<th width="80">使用率</th>
								<th width="100">操作</th>
							</tr>
							<c:forEach items="${datasArea }" var="areaStation">
								<tr class="text-c odd">
									<td>${areaStation.name }</td>
									<td>${areaStation.unitPrice }</td>
									<td>${areaStation.area }</td>
									<td>${areaStation.areaPrice }</td>
									<td>${areaStation.usageRate }</td>
									<td>
										<a title="删除" href="javascript:;" onclick="admin_del(this, '${ctx }/a/sysOfficeResources/rent/delete?id=', '${areaStation.id }')" class="ml-5" style="text-decoration:none">
											<i class="Hui-iconfont">&#xe6e2;</i>
										</a>
									</td>
								</tr>
							</c:forEach>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</form:form>
</div>
<script type="text/javascript">
$(function(){
	$.Huitab("#form-article-add .tabBar span","#form-article-add .tabCon","current","click","0")}
);
</script>
</body>
</html>