<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.prisetree.isv.common.util.DictUtils"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<HTML>
<head>
<%@include file="/WEB-INF/views/include/head.jsp"%>
<title>房源列表</title>
</head>
<body>
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>
		信息管理 <span class="c-gray en">&gt;</span>房源列表 
		<a class="btn btn-success radius r" style="line-height: 1.6em; margin-top: 3px; margin-left: 3px" href="javascript:location.replace(location.href);" title="刷新"><i	class="Hui-iconfont">&#xe68f;</i></a>
		<a class="btn btn-danger radius r" style="line-height: 1.6em; margin-top: 3px; margin-left: 3px" href="javascript:;" onclick="datadel('${ctx}/a/sysOfficeResources/delete/batch')" ><i class="Hui-iconfont">&#xe6e2;</i>	批量删除</a>
		<a class="btn btn-primary radius r" style="line-height: 1.6em; margin-top: 3px; margin-left: 3px" href="javascript:;" onclick="admin_add('添加房源信息','${ctx}/a/sysOfficeResources/add','1000','750')" ><i class="Hui-iconfont">&#xe604;</i> 添加</a>
	</nav>
 		<ul id="Huifold1" class="Huifold">
			<li class="item">
				<h4>
					筛选条件<b><i class="Hui-iconfont Hui-iconfont-arrow2-bottom"></i></b>
				</h4>
				<div class="info">
					<div class="text-c">
						<input type="text" class="input-text" style="width: 250px" placeholder="输入房源名" id="name" name="name"> 
						<input type="text" class="input-text" style="width: 250px" placeholder="输入房源所在省" id="province" name="province"> 
						<input type="text" class="input-text" style="width: 250px" placeholder="输入房源所在城市" id="city" name="city">
						<button type="submit" class="btn btn-success" id="query" name="">
							<i class="Hui-iconfont">&#xe665;</i> 查询
						</button>
					</div>
				</div>
			</li>
		</ul> 
	<div class="page-container">
		<table id="dataTable" class="table table-border table-bordered table-bg">
			<thead>
				<tr class="text-c">
					<th width="25"><input type="checkbox" name="" value=""></th>
					<th width="40">房源名</th>
					<th width="40">省</th>
					<th width="90">市</th>
					<th width="100">操作</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<script type="text/javascript" src="${ctxStatic}/js/datatables/1.10.0/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="${ctxStatic}/js/modules/officeResources.js"></script>
	<script type="text/javascript">
	var userType = <%=DictUtils.getJsonDict("user_type")%>;
	$(document).ready(function () {
		$.Huifold("#Huifold1 .item h4", "#Huifold1 .item .info",
				"fast", 1, "click");
	})
</script>
</body>
</html>