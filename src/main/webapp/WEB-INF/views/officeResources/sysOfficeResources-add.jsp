<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <%@include file="/WEB-INF/views/include/head.jsp" %>
    <%@include file="/WEB-INF/views/include/formlib.jsp" %>
    <title>编辑房源</title>
</head>
<body>
<article class="page-container">
	<div class="box">
	<form:form class="form form-horizontal" id="form-add" commandName="entity" action="${ctx}/a/sysOfficeResources/save" method="post">
		<form:hidden path="id"/>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">房源名称：</label>
            <div class="formControls col-xs-4 col-sm-4">
            	<form:input path="name" class="input-text"/>
            </div>

            <label class="form-label col-xs-2 col-sm-2">工位类型：</label>
            <div class="formControls col-xs-4 col-sm-4">
                <form:select path="officeType" class="select">
	                <form:options items="${fns:getDictList('office_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
	            </form:select>
            </div>
        </div>
        <div class="row cl" id="boxDiv">
	        <label class="form-label col-xs-2 col-sm-2">省：</label>
	        <div class="formControls col-xs-2 col-sm-2">
	        	<select class="province select" id="province" name="province" data-value="${entity.province}"></select>
	        </div>
	        <label class="form-label col-xs-2 col-sm-2">市：</label>
	        <div class="formControls col-xs-2 col-sm-2">
	            <select class="city select" id="city" name="city" data-value="${entity.city}"></select>
	        </div>
	        <label class="form-label col-xs-2 col-sm-2">区/县：</label>
	        <div class="formControls col-xs-2 col-sm-2">
	        	<select class="area select" id="area" name="area" data-value="${entity.area}"></select>
	        </div>
	    </div>
	    <div class="row cl">
	    	<label class="form-label col-xs-2 col-sm-2">联系人：</label>
            <div class="formControls col-xs-4 col-sm-4">
            	<form:input path="contact" class="input-text"/>
            </div>
	        <label class="form-label col-xs-2 col-sm-2">联系人电话：</label>
            <div class="formControls col-xs-4 col-sm-4">
            	<form:input path="contactMobile" class="input-text"/>
            </div>
	    </div>
	    <div class="row cl" id="boxDiv">
	        <label class="form-label col-xs-2 col-sm-2">经度：</label>
	        <div class="formControls col-xs-2 col-sm-2">
	        	<form:input path="lon" class="input-text" readonly="readonly" style="background-color: #ececec;"/>
	        </div>
	        <div class="formControls col-xs-2 col-sm-2">
	        	<input class="btn btn-primary radius" type="button" value="&nbsp;&nbsp;获取&nbsp;&nbsp;" onclick="openMap()">
	        </div>
	        <label class="form-label col-xs-2 col-sm-2">纬度：</label>
	        <div class="formControls col-xs-4 col-sm-2">
	        	<form:input path="lat" class="input-text" readonly="readonly" style="background-color: #ececec;"/>
	        </div>
	    </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">详细地址：</label>
            <div class="formControls col-xs-4 col-sm-4">
            	<form:input path="detailAddr" class="input-text"/>
            </div>
            <!-- <label class="form-label col-xs-2 col-sm-2">星级：</label>
            <div class="formControls col-xs-4 col-sm-4">
            	<form:input path="starNum" class="input-text"/>
            </div> -->
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">服务及设施-基础设施：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:input path="baseFacility" class="input-text"/>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">服务及设施-基础服务：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:input path="baseService" class="input-text"/>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">服务及设施-特色服务：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:input path="speService" class="input-text"/>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">地理周边-交通：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:input path="traffic" class="input-text"/>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">地理周边-周边：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:input path="nearby" class="input-text"/>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">空间特点（多个以英文逗号隔开）：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:textarea path="feature" class="input-text" style="height: 50px;"/>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">标签：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:checkboxes items="${tagsList}" path="tagsvo" itemLabel="label" itemValue="label"/>  
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-2 col-sm-2">备注：</label>
            <div class="formControls col-xs-10 col-sm-10">
            	<form:textarea path="profile" class="input-text" style="height: 50px;"/>
            </div>
        </div>
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
            </div>
        </div>
    </form:form>
    </div>
</article>

<script src="${ctxStatic}/js/cxselect/jquery.cxselect.min.js"></script>
<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">
    $(function () {
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });
        
        $('#boxDiv').cxSelect({
      	  url: '${ctxStatic}/json/city.json',
      	  selects: ['province', 'city', 'area'],
            required:'true'
      	});

        $("#form-add").validate({
            rules: {
            	name: {
                    required: true,
                },
                officeType: {
                    required: true,
                },
                lon:{
                	required: true,
                },
                lat:{
                	required: true,
                },
                stationNum:{
                	required: true,
                },
            },
            onkeyup: false,
            focusCleanup: true,
            success: "valid",
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function (data) {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.$('.btn-success').click();
                        parent.layer.close(index);
                    },
                    error: function () {
                        layer.confirm("系统出现错误，请联系管理员！");
                    }
                });
            }
        });
        
        // 初始化数据
        showInput($('#isFlexible'), 'flexibleTarget');
        showInput($('#isStable'), 'stableTarget');
    });
    
    function showInput(select, target)
    {
    	if($(select).val() == '0')
   		{
   			$('#' + target).hide();
   			$('#' + target + ' input').val('');
   		}
    	else
   		{
    		$('#' + target).show();
   		}
    }
    
    function openMap()
    {
    	var city = $('#city').val();
    	if(city != '请选择')
   		{
    		admin_add('经纬度','${ctx}/a/sysOfficeResources/map?city=' + city,'800','500')
   		}
    	else
   		{
    		alert('请选择城市')
   		}
    }
    
    function setLonAndLat(lon, lat)
    {
    	$('#lon').val(lon);
    	$('#lat').val(lat);
    	layer.close(layer.index);
    }
    
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>