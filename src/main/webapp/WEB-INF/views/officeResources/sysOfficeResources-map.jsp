<%@ page contentType="text/html;charset=UTF-8"%>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <title>根据城市名称设置中心点</title>
    <link rel="stylesheet" href="http://cache.amap.com/lbs/static/main1119.css"/>
    <style>
        input[type="text"] {
            height: 25px;
            outline: none;
            border: 0;
            border: 1px solid #CCCCCC;
            padding: 0 4px;
        }
    </style>
    <script src="http://webapi.amap.com/maps?v=1.4.0&key=867e07150819a1b1464ca539de395f1c"></script>
    <script type="text/javascript" src="http://cache.amap.com/lbs/static/addToolbar.js"></script>
</head>
<body>
<div id="container"></div>
<div class="button-group">
    <input id="cityName" class="inputtext" placeholder="请输入关键字" type="text"/>
    <input id="query" class="button" value="检索" type="button"/>
</div>
<script>
    var map = new AMap.Map('container', {
        resizeEnable: true
    });
    map.setCity('${city}');
    
    AMap.service(["AMap.PlaceSearch"], function() {
        var placeSearch = new AMap.PlaceSearch({ //构造地点查询类
            pageSize: 5,
            pageIndex: 1,
            city: "${city}", //城市
            map: map//,
            //panel: "panel"
        });
        AMap.event.addDomListener(document.getElementById('query'), 'click', function() {
            var cityName = document.getElementById('cityName').value;
            if (!cityName) {
                cityName = '北京市';
            }
          	//关键字查询
            placeSearch.search(cityName, function(status, result) {
            });
        });
    });
    
    
    AMap.event.addListener(map, "click", function(e) {
    	window.parent.setLonAndLat(e.lnglat.getLng(), e.lnglat.getLat());
	});
</script>
</body>
</html>