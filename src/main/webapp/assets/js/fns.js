
/*
 参数解释：
 title	标题
 url		请求的url
 id		需要操作的数据id
 w		弹出层宽度（缺省调默认值）
 h		弹出层高度（缺省调默认值）
 */
/*管理员-增加*/
function admin_add(title,url,w,h){
    layer_show(title,url,w,h);
}
/*管理员-删除*/
function admin_del(obj,url,id){
    layer.confirm('确认要删除吗？',function(index){
        //此处请求后台程序，下方是成功后的前台处理……
        $.ajax({
            type: "POST",
            url: url +id,
            success: function (data, textStatus) {
                $(obj).parents("tr").remove();
                layer.msg('已删除!',{icon:1,time:1000});
            },
            error: function (data, textstatus) {
                layer.confirm("系统出现错误，请联系管理员！");
            }
        });
    });
}
/*管理员-编辑*/
function admin_edit(title,url,id,w,h){
    layer_show(title,url,w,h);
}

function datadel(url){
    /*日志-批量删除*/
    var inputs = $('tbody').find('input[type="checkbox"]:checked');
    var idlist = "";
    if (!inputs || !inputs.length) {
        layer.alert('请至少选中一条数据');
        return;
    }
    layer.confirm('确认要删除吗？', function (index) {
        //此处请求后台程序，下方是成功后的前台处理……
        inputs.each(function () {
            idlist = idlist + $(this).val() + ',';
        });

        $.ajax({
            type: "POST",
            url: url+"?ids=" + idlist.substr(0,idlist.length-1),
            success: function (data, textStatus) {
                inputs.each(function () {
                    $(this).parents("tr").remove();
                });
                layer.msg('已删除!', {icon: 1, time: 1000});
            },
            error: function (data, textstatus) {
                layer.confirm("系统出现错误，请联系管理员！");
            }
        });
    });
}

function defaultSettingDT() {
    this.bStateSave = true;//状态保存
    this.pagingType = 'full_numbers';
    this.processing = true;
    this.bDeferRender = true;
    this.sServerMethod = 'POST';
    this.info = true;//是否显示页脚信息，DataTables插件左下角显示记录数
    this.paging = true;//是否显示（应用）分页器
    this.searching = false;//是否启动过滤、搜索功能
    this.bLengthChange = true;//是否允许用户改变表格每页显示的记录数
    this.searching = false;//是否允许Datatables开启本地搜索
    this.ordering = false;//是否允许Datatables开启排序
    this.bServerSide = true;//是否开启服务器模式
    this.bSort = true;//开关，是否让各列具有按列排序功能
    this.bAutoWidth = true;//自动宽度
    this.createdRow = createdRow;
    this.dom='<"top">rt<"bottom"ipl><"clear">';
    this.aLengthMenu=[ 10, 15,20, 30, 50];
}

var createdRow =function ( row, data, index ) {
    $(row).addClass( 'text-c' );
};

Date.prototype.format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1,                 //月份
        "d+": this.getDate(),                    //日
        "h+": this.getHours(),                   //小时
        "m+": this.getMinutes(),                 //分
        "s+": this.getSeconds(),                 //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds()             //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

