
(function () {

    var tableColumns = [
        {"data": "companyName"},
        {"data": "officeResourcesName"},
        {"data": "enterTime"},
        {"data": "leaveTime"},
        {"data": "stationNum"},
        {"data": "unit"},
        {"data": "billingMode"},
        {"data": "unitPrice"},
        {"data": "totalPrice"},
        {"data": "type"},
        {"data": "opt"}
    ];

    var dataTableSetting = new defaultSettingDT();
    dataTableSetting.ajax = {
        url: projectName + "/a/sysOrders",
        type: "POST",
        data: function (d) {
            return $.extend({}, d, {
            	name: $("#companyName").val()
            });
        },
        "dataSrc": function (json) {
            for (var i = 0, l = json.data.length; i < l; i++) {
            	if(!json.data[i].stationNum){
            		json.data[i].stationNum = "-";
            	}
            	json.data[i].billingMode = billingMode[json.data[i].billingMode];
            	json.data[i].type = orderType[json.data[i].type];
            	if(json.data[i].state == '0')
        		{
            		json.data[i].opt = "<a title=\"审核\" href=\"javascript:;\" onclick=\"admin_edit('审核', projectName+'/a/sysOrders/audit?id=" + json.data[i].id + "','" + json.data[i].id + "','600','400')\"" +
                    " class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">审核</i></a> ";
        		}
            	else if(json.data[i].state == '1')
        		{
            		json.data[i].opt = "<i class=\"Hui-iconfont\">审核通过</i>";
        		}
            	else if(json.data[i].state == '2')
        		{
            		json.data[i].opt = "<a title=\"审核结果\" href=\"javascript:;\" onclick=\"admin_edit('审核结果', projectName+'/a/sysOrders/auditinfo?id=" + json.data[i].id + "','" + json.data[i].id + "','600','400')\"" +
                    " class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\" style=\"color:red\">审核未通过,点击查看原因</i></a>";
        		}
            	else if(json.data[i].state == '3')
        		{
            		json.data[i].opt = "<i class=\"Hui-iconfont\">完成</i>";
        		}
            }
            return json.data;
        }
    };
    dataTableSetting.columns = tableColumns;
    dataTableSetting.createdRow = createdRow;

    $(document).ready(function () {
        var dataTable = $("#dataTable").DataTable(dataTableSetting);

        $("#query").on("click", function () {
            dataTable.ajax.reload();
        });

        $('.table-sort tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                $('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

    })

})();
