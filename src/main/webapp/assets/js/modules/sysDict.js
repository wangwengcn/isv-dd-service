(function () {

    var tableColumns = [
        {"data": "checkBox"},
        {"data": "value"},
        {"data": "label"},
        {"data": "type"},
        {"data": "description"},
        {"data": "opt"}
    ];

    var dataTableSetting = new defaultSettingDT();
    dataTableSetting.ajax = {
        url: projectName+"/a/sysDict",
        type: "POST",
        data: function (d) {
            return $.extend({}, d, {
                label: $("#label").val(),
                type: $("#type").val(),
                remark: $("#remark").val()
            });
        },
        "dataSrc": function (json) {
            for (var i = 0, l = json.data.length; i < l; i++) {
                json.data[i].checkBox = "<input type=\"checkbox\" value=\"" + json.data[i].id + "\" >";
                json.data[i].opt = "<a title=\"编辑\" href=\"javascript:;\" onclick=\"admin_edit('编辑',projectName+'/a/sysDict/edit?id=" + json.data[i].id + "','" + json.data[i].id + "','800','500')\"" +
                    " class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe6df;</i></a> " +
                    "<a title=\"删除\" href=\"javascript:;\" onclick=\"admin_del(this,projectName+'/a/sysDict/delete?id=','" + json.data[i].id + "')\" class=\"ml-5\" style=\"text-decoration:none\">" +
                    "<i class=\"Hui-iconfont\">&#xe6e2;</i></a>";
            }
            return json.data;
        }
    };
    dataTableSetting.columns = tableColumns;
    dataTableSetting.createdRow = createdRow;

    $(document).ready(function () {
        var dataTable = $("#dataTable").DataTable(dataTableSetting);

        $("#query").on("click", function () {
            dataTable.ajax.reload();
        });

        $('.table-sort tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                $('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

    })

})();