
(function () {

    var tableColumns = [
        {"data": "checkBox"},
        {"data": "name"},
        {"data": "province"},
        {"data": "city"},
        {"data": "opt"}
    ];

    var dataTableSetting = new defaultSettingDT();
    dataTableSetting.ajax = {
        url: projectName + "/a/sysOfficeResources",
        type: "POST",
        data: function (d) {
            return $.extend({}, d, {
            	name: $("#name").val(),
            	province: $("#province").val(),
            	city: $("#city").val()
            });
        },
        "dataSrc": function (json) {
            for (var i = 0, l = json.data.length; i < l; i++) {
                json.data[i].checkBox = "<input type=\"checkbox\" value=\"" + json.data[i].id + "\" >";
                json.data[i].userType = userType[json.data[i].userType];
                json.data[i].opt = "<a title=\"编辑\" href=\"javascript:;\" onclick=\"admin_edit('编辑', projectName+'/a/sysOfficeResources/edit?id=" + json.data[i].id + "','" + json.data[i].id + "','1000','750')\"" +
                    " class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe6df;</i></a> " +
                    "<a title=\"图片\" href=\"javascript:;\" onclick=\"admin_add('图片', projectName+'/a/sysOfficeResources/img?id=" + json.data[i].id + "','1000','750')\"" +
                    " class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe646;</i></a> " +
                    "<a title=\"房型\" href=\"javascript:;\" onclick=\"admin_add('房型', projectName+'/a/sysOfficeResources/rent?id=" + json.data[i].id + "','1000','750')\"" +
                    " class=\"ml-5\" style=\"text-decoration:none\"><i class=\"Hui-iconfont\">&#xe64b;</i></a> " +
                    "<a title=\"删除\" href=\"javascript:;\" onclick=\"admin_del(this, projectName+'/a/sysOfficeResources/delete?id=','" + json.data[i].id + "')\" class=\"ml-5\" style=\"text-decoration:none\">" +
                    "<i class=\"Hui-iconfont\">&#xe6e2;</i></a>";
            }
            return json.data;
        }
    };
    dataTableSetting.columns = tableColumns;
    dataTableSetting.createdRow = createdRow;

    $(document).ready(function () {
        var dataTable = $("#dataTable").DataTable(dataTableSetting);

        $("#query").on("click", function () {
            dataTable.ajax.reload();
        });

        $('.table-sort tbody').on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            }
            else {
                $('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });

    })

})();
