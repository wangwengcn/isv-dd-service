package com.dingtalk.isv.access.biz.service.message;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.CorpMessageCorpconversationAsyncsendRequest;
import com.dingtalk.api.response.CorpMessageCorpconversationAsyncsendResponse;
import com.dingtalk.isv.access.api.service.message.SendMessageService;
import com.dingtalk.isv.access.biz.base.BaseTestCase;
import com.dingtalk.isv.common.model.ServiceResult;
import com.dingtalk.open.client.api.model.corp.MessageBody;
import com.taobao.api.ApiException;

/**
 * Created by lifeng.zlf on 2016/3/22.
 */
public class SendMessageServiceTest  extends BaseTestCase {
    @Resource
    private SendMessageService sendMessageService;
    @Test
    public void test_sendMessage() {
        Long start = System.currentTimeMillis();
        String suiteKey="suiteytzpzchcpug3xpsm";
        String corpId = "ding4ed6d279061db5e7";
        Long appId = -23L;
        MessageBody.OABody message = new MessageBody.OABody();
        MessageBody.OABody.Head head = new MessageBody.OABody.Head();
        MessageBody.OABody.Body body = new MessageBody.OABody.Body();
        head.setText("HEAD");
        head.setBgcolor("ffffa328");
        head.setText("客户详情");
        //body.setAuthor("dd_test");
        body.setContent("标题 \r\n 换行");
        body.setTitle("xxxxxxxx \r\n  换行");
        //body.setTitle("http://qr.dingtalk.com/page/crminfo?appid=-23&corpid=%24"+corpId+"%24&staffid=\"\"");
        message.setHead(head);
        message.setBody(body);
        message.setMessage_url("http://qr.dingtalk.com/page/crminfo?appid=-23&corpid=%24"+corpId+"%24");
        //020434228123 qianxun   0566581316 fuxi dd_test
        ServiceResult<Void> sr = sendMessageService.sendOAMessageToUser(suiteKey, corpId, appId, "oa", Arrays.asList("dd_test"),null,message);
        System.err.println("cost:"+(System.currentTimeMillis()-start));
        System.err.println(JSON.toJSONString(sr));
    }
    
    
    public static void main(String[] args)
    {
    	String userId = "manager3714";
		String corpId = "ding0a4914b189d2884435c2f4657eb6378f";
		String access_token = "0333ca0ab1f5329fbba6fe67211a12bd";
		Long agentId = 157959177L;
		String messageUrl = "https://isv.prisetree.com/isv-dd-service/?dd_nav_bgcolor=fff6f6f7&corpId=" + corpId;
		String providerName = "米域方寸";
		
		DingTalkClient client = new DefaultDingTalkClient("https://eco.taobao.com/router/rest");
		String img = "lADOADmaWMzazQKA";
		img = StringUtils.isNotEmpty(img) ? ",\"image\": \"@" + img + "\"" : "";
		String tel = "13676400750";
		String title = "预定通知";
		String hColor = "FFBBBBBB";
		String content = null;
		String author = "上海企树网络科技有限公司";
		if(StringUtils.isEmpty(content))
		{
			content = "您在 ${provider_name} 提交的工位预约单已经成功，请在钉钉内添加我们的客服人员，我们将尽快告知您预约结果。";
		}
		content = content.replace("${provider_name}", providerName);
		CorpMessageCorpconversationAsyncsendRequest req = new CorpMessageCorpconversationAsyncsendRequest();
		req.setMsgtype("oa");
		req.setAgentId(agentId);
		req.setUseridList(userId);
		req.setToAllUser(false);
		req.setMsgcontentString("{\"message_url\": \"" + messageUrl + "\",\"head\": {\"bgcolor\": \"" + hColor + "\",\"text\": \"" + title + "\"},\"body\": {\"title\": \"" + title + "\",\"form\": [{\"key\": \"客服人员:\",\"value\": \"" + tel + "\"}],\"content\": \"" + content + "\"" + img + ",\"author\": \"" + author + " \"}}");
		System.out.println(req.getMsgcontent());
		CorpMessageCorpconversationAsyncsendResponse rsp;
		try {
			rsp = client.execute(req, access_token);
			System.out.println(rsp.getBody());
		} catch (ApiException e) {
			e.printStackTrace();
		}
    }

}
